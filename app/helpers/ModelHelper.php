<?php

/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 12/22/14
 * Time: 11:03 AM
 */
class ModelHelper
{
    /**
     * Convert list object Model to array
     * @param $data
     * @param $key
     * @return array
     */
    static function toArray($data, $key = null)
    {
        $result = [];
        if (count($data) > 0)
            foreach ($data as $inc => $obj) {
                $getKey = 'get' . ucfirst($key) . '';
                if (isset($obj->$key) || method_exists(get_class($obj), $getKey)) {
                    $inc = isset($obj->$key) ? $obj->$key : $obj->{$getKey}();
                }
                $result[$inc] = $obj->toArray();
            }
        return $result;
    }
    /**
     * @param array $categories
     * @param int $parent_id
     * @return array
     */
    static function loadCategory(array $categories, $parent_id = 0) {
        $result = [];

        foreach($categories as $k => $v) {
            // Add cate data to result
            if ($v['parent_id'] == $parent_id) {

                $result[$v['id']] = $v;

                // Find child of category
                $result[$v['id']]['child'] = self::loadCategory($categories, $v['id']);
            }
        }

        return $result;
    }

    /**
     * Render html option from categories
     * @param array $categories
     * @param int $level
     * @param string $space
     * @param int $selected
     * @return string
     */
    static function rendHtmlOption(array $categories, $level = 1, $space = '', $selected = 0) {
        $html = '';
        if(!empty($categories)) {
            $prefix = '';
            for($i = 2; $i <= $level; $i ++) {
                $prefix .= $space;
            }

            foreach($categories as $k=> $v) {
                $selected_option = ($selected == $k ? 'selected' : '');
                $html .= "<option value='{$k}' {$selected_option}>{$prefix} {$v['name']}</option>";
                $html .= self::rendHtmlOption($v['child'], $level + 1, $space, $selected);
            }
        }
        return $html;
    }
}