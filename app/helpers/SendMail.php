<?php

/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 12/17/14
 * Time: 9:19 AM
 */

class SendMail
{
    static function sendMailByGoogleSsl($subject = 'Error', $message, $to = '')
    {
        try {
            require_once(__DIR__ . '/../../vendor/autoload.php');

            $config = include(__DIR__ . '/../../app/config/config.php');

            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
                ->setUsername($config->google_sender['email'])
                ->setPassword($config->google_sender['password']);

            $mailer = Swift_Mailer::newInstance($transport);
            $message = Swift_Message::newInstance($subject)
                ->setFrom(array($config->google_sender['email'] => $config->google_sender['label']))
                ->setTo($to)
                ->setBody($message);

            if (!$mailer->send($message)) {
                return false;
            } else {
                return true;
            }

        } catch (Exception $e) {
            var_dump($e);
        }
    }


}
