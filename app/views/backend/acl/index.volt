{% set status = [t._('Disabled'), t._('Enalbed')] %}
<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('List ACL') }}</h1>
        <a href="{{ url('admin/acl/create') }}" class="btn-new">
            <button class="btn btn-primary" type="button">{{ t._('Add new') }}</button>
        </a>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        {{ flashSession.output() }}
        <div class="panel panel-default">
            <!--<div class="panel-heading">
                DataTables Advanced Tables
            </div>-->
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>{{ t._('Order') }}</th>
                            <th>{{ t._('Label') }}</th>
                            <th>{{ t._('Controller name') }}</th>
                            <th>{{ t._('Action name') }}</th>
                            <th>{{ t._('Menu enabled') }}</th>
                            <th>{{ t._('Description') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for index, m in acl %}
                        <tr>
                            <td>{{ index + 1 }}</td>
                            <td>{{ m.getLabel() }}</td>
                            <td>{{ m.getController() }}</td>
                            <td>{{ m.getAction() }}</td>
                            <td>{{ m.getState() ? '<span class="label label-primary">' ~ t._('Enabled') ~ '</span>': '<span class="label label-warning">' ~ t._('Disabled') ~ '</span>' }}</td>
                            <td>{{ m.getDescription() }}</td>
                            <td class="center">
                                <a href="{{ url('admin/acl/edit/' ~ m.getId()) }}">{{ t._('Edit') }}</a> | <a class="delete"
                                                                      href="{{ url('admin/acl/delete/' ~ m.getId())}}">{{
                                t._('Delete') }}</a>
                            </td>
                        </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('#dataTables-example').dataTable(
            {
                "oLanguage": {
                    "sLengthMenu": "_MENU_ " + '<?php echo $t->_("rows/page"); ?>',
                    "sInfo": "<?php echo $t->_('Showing') ?> _START_ to _END_ of _TOTAL_ <?php echo $t->_('result') ?>",
                    "sSearch": "<?php echo $t->_('Search') ?>: ",
                    "sPrevious": "<?php echo $t->_('Previous') ?>",
                    "sNext": "<?php echo $t->_('Next') ?>",
                    "sEmptyTable": "<?php echo $t->_('No data available in table') ?>",
                    "sInfoEmpty": "<?php echo $t->_('Showing 0 to 0 of 0 entries') ?>"
                },
                //"order": [[ 0, "desc" ]],
                /*"lengthMenu": [ 2, 5, 10, 25, 50, 75, 100 ],*/
                 'pageLength': 25,
                "columnDefs": [
                    {"orderable": false, "targets": [4] }
                ]
            }
        );
        var tmp_val = '';
        // Change default keypress search
        $("#dataTables-example_filter input").unbind().keyup(function (e) {
            if (e.keyCode == 13 & tmp_val != this.value) {
                oTable.fnFilter(this.value);
                tmp_val = this.value;
            }
        });
        $('a.delete').click(function (e) {
            if (!confirm("<?php echo $t->_('Are you sure?') ?>"))
                e.preventDefault();
        });
        $(window).keyup(function(event) {
            if(event.keyCode == 78 & event.ctrlKey & event.altKey) { // Ctrl + Alt + N
                window.location.href = $('a.btn-new').prop('href');
            }
        });
    });
    function __delete(event) {
        var e = event ? event : window.event;
        if (!confirm("<?php echo $t->_('Are you sure?') ?>"))
            e.preventDefault();
    }
</script>