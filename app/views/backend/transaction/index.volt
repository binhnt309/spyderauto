<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('List transaction') }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        {{ flashSession.output() }}
        <div class="panel panel-default">
            <!--<div class="panel-heading">
                DataTables Advanced Tables
            </div>-->
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tblTrans">
                        <thead>
                        <tr>
                            <th>{{ t._('#') }}</th>
                            <th>{{ t._('Order Code') }}</th>
                            <th>{{ t._('Paypal IPN') }}</th>
                            <th>{{ t._('Amount') }}</th>
                            <th>{{ t._('Created At') }}</th>
                            <th>{{ t._('State') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>

<style type="text/css">
    #tblTrans_wrapper th:nth-child(3) {
        width: auto !important;
    }
    #tblTrans_wrapper th:last-child {
        width: 18% !important;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('#tblTrans').dataTable(
                {
                    "oLanguage": {
                        "sLengthMenu": "_MENU_ " + '<?php echo $t->_("rows/page"); ?>',
                        "sInfo": "<?php echo $t->_('Showing') ?> _START_ to _END_ of _TOTAL_ <?php echo $t->_('result') ?>",
                        "sSearch": "<?php echo $t->_('Search') ?>: ",
                        "sPrevious": "<?php echo $t->_('Previous') ?>",
                        "sNext": "<?php echo $t->_('Next') ?>",
                        "sEmptyTable": "<?php echo $t->_('No data available in table') ?>",
                        "sInfoEmpty": "<?php echo $t->_('Showing 0 to 0 of 0 entries') ?>"
                    },
                    "order": [[ 1, "desc" ]],
                    /*"lengthMenu": [ 2, 5, 10, 25, 50, 75, 100 ],
                     'pageLength': 2,*/
                    "columnDefs": [
                        {"orderable": false, "targets": [6] }
                    ],
                    "processing": true,
                    "serverSide": true,
                    'ajax': {
                        'url': '/admin/transaction/index?_=' + Math.random(),
                        'data': function (d) {
                            d.key = $('#tblTrans_filter input').val();
                        }
                    }, "fnDrawCallback": function (oSettings) {
                        $('#tblTrans td:last-child a').click(function(e) {
                            if(!confirm('Are you sure do this action?')) {
                                e.preventDefault();
                            }
                        });
                    }
                }
        );
        var tmp_val = '';
        // Change default keypress search
        $("#tblTrans_filter input").unbind().keyup(function (e) {
            if (e.keyCode == 13 & tmp_val != this.value) {
                oTable.fnFilter(this.value);
                tmp_val = this.value;
            }
        });
    });
</script>