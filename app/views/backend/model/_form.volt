<div class="row">
    <div class="col-sm-12">

        {{ flashSession.output() }}

        {{ form('', 'name' : 'model', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}
        <div class="form-group">
            <label>{{ t._('Model name') }}</label><span class="required">*</span>
            {{ text_field('model', 'class' : 'form-control',
                'value' : (req.getPost('model') ? req.getPost('model') : model.getModelName() )) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Alias') }}</label>
            {{ text_field('alias', 'class' : 'form-control',
                'value' : (req.getPost('username') ? req.getPost('alias') : model.getAlias() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Brand') }}</label>
            {{ select('brand', 'using' : ['id', 'brand_name'], brands, 'class' : 'form-control',
                'useEmpty' : true, 'emptyText' : '', 'value' : model.getBrand() ) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Status') }}</label>
            {{ select('status', status, 'value': model.getStatus(),
            'class' : 'form-control' ) }}
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ model.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
        {{ end_form() }}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=model]').prop('action', window.location.href);
    });
    function _formValidate() {
        var result = true,
                msg = '';
        if ($('#model').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Email empty!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>