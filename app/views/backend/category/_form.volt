<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
        {{ form('', 'name' : 'category', 'role' : 'form', 'onsubmit' : 'return _formValidate()', 'enctype' : 'multipart/form-data') }}

        <div class="panel panel-default">
            <div class="panel-heading">SEO data</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="name">{{ t._('Name') }}</label><span class="required">*</span>
                    {{ text_field('name', 'class' : 'form-control',
                    'value' : (req.getPost('name') ? req.getPost('name') : category.getName() )) }}
                </div>

                <div class="form-group">
                    <label for="slug">{{ t._('Slug friendly Url') }}</label><span class="required">*</span>
                    {{ text_field('slug', 'class' : 'form-control',
                    'value' : (req.getPost('slug') ? req.getPost('slug') : category.getSlug() )) }}
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="thumb">{{ t._('Thumb') }}</label>
                            {{ file_field('thumb') }}
                        </div>
                        <div class="col-sm-8">
                            <img src="{{ thumb }}" align="category.getName()" class="thumb"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="parent_id">{{ t._('Parent') }}</label>
                    <select name="parent_id" id="parent_id" class="form-control">
                        <option> - {{ t._('Select category') }}</option>
                        {{ category_options }}
                    </select>
                </div>

                <div class="form-group">
                    <label for="des">{{ t._('Description') }}</label>
                    {{ text_area('des', 'class' : 'form-control', 'rows': '10',
                    'value' : (req.getPost('des') ? req.getPost('des') : category.getDes() )) }}
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>{{ t._('Position')}}</label>
                            {{ text_field('pos', 'class':'form-control', 'placeholder' : 0,
                            'value' :(req.getPost('pos') ? req.getPost('pos') : category.getPos() )) }}
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="status">{{ t._('Status') }}</label>
                            {{ select('status', status, 'value': category.getStatus(),
                            'class' : 'form-control' ) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">SEO data</div>
            <div class="panel-body">
                <div class='form-group'>
                    <label for='keyword'>{{t._('Keyword')}}</label>
                    {{ text_field('keyword', 'class' : 'form-control', 'placeholder':'Keyword for page, maximum 75
                    characters',
                    'maxlength':255,
                    'value':(req.getPost('keyword') ? req.getPost('keyword') : seo.getKeyword() )) }}
                </div>
                <div class='form-group'>
                    <label for='description'>{{t._('Description')}}</label>
                    {{ text_field('description', 'class' : 'form-control', 'placeholder':'Description for page, maximum
                    255 characters',
                    'maxlength':255,
                    'value':(req.getPost('description') ? req.getPost('description') : seo.getDescription() )) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ category.getId() ? t._('Update') : t._('Create') }}
            </button>
        </div>

        {{ end_form() }}
    </div>
</div>

<style type="text/css">
    img.thumb { height: 200px; }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=category]').prop('action', window.location.href);
        $('#name').keyup(function(){
            $('#slug').val(utils.slug($(this).val()));
        });
        tinymce.init({
            selector: 'textarea#des',
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak code",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true,
            external_filemanager_path: "/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "/filemanager/plugin.min.js"},
            relative_urls: false,
            remove_script_host: false
        });
    });
    function _formValidate() {
        var result = true,
            msg = '';
        if ($('#name').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Name empty!') ?>\n";
            result = false;
        }
        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>