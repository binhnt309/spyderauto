<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
        {{ form('', 'name' : 'user', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}
        <div class="form-group">
            <label>{{ t._('Fullname') }}</label>
            {{ text_field('full_name', 'class' : 'form-control', 'value' : (req.getPost('full_name') ? req.getPost('full_name') : user.getFullName() )) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Username') }}<span class="required">*</span></label>
            {{ text_field('username', 'class' : 'form-control', 'value' : (req.getPost('username') ? req.getPost('username') : user.getUsername() )) }}
        </div>

        {% if session.get('member')['username'] == 'admin' %}
        <div class="form-group">
            <label>{{ t._('Email') }}</label><span class="required">*</span>
            {{ email_field('email', 'class' : 'form-control', 'value' : (req.getPost('email') ? req.getPost('email') : user.getEmail())) }}
        </div>
        {% endif %}

        <div class="form-group">
            <label>{{ t._('Password') }}<span class="required">*</span></label>
            {{ password_field('pwd', 'class' : 'form-control') }}
        </div>

        {% if session.get('member')['username'] == 'admin' %}
        <div class="form-group">
            <label>{{ t._('Group') }}</label><span class="required">*</span>
            {{ select('group', userGroups, 'using' : ['id', 'name'], 'class' : 'form-control',
            'value' : user.getGroup(), 'useEmpty': true, 'emptyText' : t._('- Select group') ) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Status') }}</label>
            {{ select('status', ['PENDING' : t._('PENDING'), 'APPROVED' : t._('APPROVED')], 'value': user.getStatus(),
            'class' : 'form-control' ) }}
        </div>
        {% endif %}

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ user.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
        {{ end_form() }}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=user]').prop('action', window.location.href);
        // Disable email field if update
        <?php if($user->getId()) { ?>
            $('#username').prop('disabled', true);
            $('#email').prop('disabled', true);
        <?php } ?>
    });
    function _formValidate() {
        var result = true,
                msg = '';
        if ($('#email').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Email empty!') ?>\n";
            result = false;
        }
        if ($('#uname').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Username empty!') ?>\n";
            result = false;
        }
        <?php
        if (!$user->getId()) {
            ?>
            if ($('#pwd').val().replace(/\s/g, '') == '') {
                msg += "<?php echo $t->_('Password empty!') ?>\n";
                result = false;
            }
        <?php
        }
        ?>
        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>