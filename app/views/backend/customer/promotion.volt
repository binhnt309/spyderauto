<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('List coupon') }}</h1>
        <a href="{{ url('admin/customer/createPromotion') }}">
            <button class="btn btn-primary" type="button">{{ t._('Add new') }}</button>
        </a>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        {{ flashSession.output() }}
        <div class="panel panel-default">
            <!--<div class="panel-heading">
                DataTables Advanced Tables
            </div>-->
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>{{ check_field('check_all', 'class':'form-control check-all') }}</th>
                            <th>{{ t._('#') }}</th>
                            <th>{{ t._('Code') }}</th>
                            <th>{{ t._('Amount') }}</th>
                            <th>{{ t._('Unit') }}/{{ t._('Mobile') }}</th>
                            <th>{{ t._('Start') }}</th>
                            <th>{{ t._('End') }}</th>
                            <th>{{ t._('Total send mail') }}</th>
                            <th>{{ t._('Status') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- data here -->
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>

<!-- Send mail Modal -->
<a href="#mailModal" class="hidden open_mailModal" data-toggle="modal" type="hidden"></a>
<div id="mailModal" tabindex="-1" role="dialog" aria-labelledby="mailModalLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="mailModalLabel" class="modal-title">Send mail</h4>
            </div>
            <div class="modal-body">
                <form method="post" role="form" id="frm-mail">
                    <div class="form-group">
                        <label for="subject">Subject: <span class="text-danger">*</span> </label>
                        <input type="text" id="subject" class="form-control" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <label for="message">Body message: <span class="text-danger">*</span> </label>
                        <textarea id="message" name="message" class="form-control" placeholder="Body message"></textarea>
                    </div>
                    <a href="javascript:;" class="btn btn-primary btn-sendMail" data-id="">Send</a>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var total_send = 0,
        total_mail = 0;

    $(document).ready(function () {
        var oTable = $('#dataTables-example').dataTable(
            {
                "oLanguage": {
                    "sLengthMenu": "_MENU_ " + '<?php echo $t->_("rows/page"); ?>',
                    "sInfo": "<?php echo $t->_('Showing') ?> _START_ to _END_ of _TOTAL_ <?php echo $t->_('result') ?>",
                    "sSearch": "<?php echo $t->_('Search') ?>: ",
                    "sPrevious": "<?php echo $t->_('Previous') ?>",
                    "sNext": "<?php echo $t->_('Next') ?>",
                    "sEmptyTable": "<?php echo $t->_('No data available in table') ?>",
                    "sInfoEmpty": "<?php echo $t->_('Showing 0 to 0 of 0 entries') ?>"
                },
                "order": [[1, "desc"]],
                "lengthMenu": [25, 50, 75, 100],
                /*'pageLength': 2,*/
                "columnDefs": [
                    {"orderable": false, "targets": [0, 9]}
                ],
                "processing": true,
                "serverSide": true,
                'ajax': {
                    'url': '/admin/customer/promotion?_=' + Math.random(),
                    'data': function (d) {
                        d.key = $('#dataTables-example_filter input').val();
                    }
                }
            }
        );
        var tmp_val = '';
        // Change default keypress search
        $("#dataTables-example_filter input").unbind().keyup(function (e) {
            if (e.keyCode == 13 & tmp_val != this.value) {
                oTable.fnFilter(this.value);
                tmp_val = this.value;
            }
        });

        // Send mail
        $('.btn-sendMail').click(function() {

            if($('#subject').val() == '') {
                alert('Subject can not be empty');
                return;
            }
            if($('#message').val() == '') {
                alert('Body message can not be empty');
                return;
            } // -------------------

            if(confirm('Are you sure want send this coupon to list user have been selected?')) {
                $('body').append('<div class="loading"></div>');
                $.ajax({
                    url: '<?php echo $this->url->get('admin/customer/loadMailOfCoupon/') ?>' + $(this).data('id'),
                    method: 'POST',
                    data: $('#frm-mail').serialize(),
                    dataType: 'json',
                    success: function(d) {
                        if(d.length) {
                            total_mail = d.length;
                            var timer = setInterval(function() {
                                if(d.length) {
                                    sendMail(d.pop());
                                } else {
                                    clearInterval(timer);
                                }
                            }, 1000); // Send next email after one minute
                        }
                    }, error: function(err) {
                        $('.loading').remove();
                        alert(err);
                    }
                });
            }
        });
    });

    function openModalDialog(coupon_id) {
        $('#frm-mail')[0].reset();
        $('.btn-sendMail').data('id', coupon_id);
        $('.open_mailModal').click();
    }

    function sendMail(email) {
        $.ajax({
            url : '<?php echo $this->url->get('admin/customer/sendMail/?_=' . mt_getrandmax()) ?>',
            method: 'POST',
            data: $('#frm-mail').serialize() + '&to=' + email,
            dataType: 'json',
            success: function(d) {
                total_send += 1;
                if(total_mail <= total_send) {
                    // Send finished
                    $('.loading').remove();
                    $('#mailModal .close').click();

                    window.location.reload(true);

                }
            }, error: function(err) {
                $('.loading').remove();
                alert(err);
            }
        });
    }

</script>