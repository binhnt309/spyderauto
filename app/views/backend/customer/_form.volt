<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
        {{ form('', 'name' : 'customer', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}

        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label>{{ t._('Customer name') }}</label> <span class="required">*</span>
                    {{ text_field('name', 'class' : 'form-control', 'value' : (req.getPost('name')
                    ? req.getPost('name') : customer.getName() )) }}
                </div>
                <div class="col-sm-6">
                    <label>{{ t._('Email') }}</label> <span class="required">*</span>
                    {{ email_field('email', 'class' : 'form-control', 'value' : (req.getPost('email')
                    ? req.getPost('email') : customer.getEmail() )) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label>{{ t._('Mobile') }}</label>
                    {{ text_field('mobile', 'class' : 'form-control', 'value' : (req.getPost('mobile')
                    ? req.getPost('mobile') : customer.getMobile() )) }}
                </div>
                <div class="col-sm-6">
                    <label>{{ t._('Phone') }}</label>
                    {{ text_field('phone', 'class' : 'form-control', 'value' : (req.getPost('phone')
                    ? req.getPost('phone') : customer.getPhone() )) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-sm-10">
                    <label>{{ t._('Address') }}</label>
                    {{ text_field('address', 'class' : 'form-control', 'value' : (req.getPost('address')
                    ? req.getPost('address') : customer.getAddress() )) }}
                </div>
                <div class="col-sm-2">
                    <label>{{ t._('Country') }}</label>
                    {{ select('country', 'using' : ['id', 'name'], countries, 'class' : 'form-control',
                        'value' : (req.getPost('country') ? req.getPost('country') : customer.getCountry() )) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>{{ t._('Status') }}</label>
            {{ select('status', status, 'value': customer.getStatus(),
            'class' : 'form-control' ) }}
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ customer.getId() ? t._('Update') : t._('Create') }}
            </button>
        </div>
        {{ end_form() }}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=customer]').prop('action', window.location.href);
    });
    function _formValidate() {
        var result = true,
            msg = '';
        if ($('#email').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Email empty!') ?>\n";
            result = false;
        }

        if (!validateEmail($('#email').val().replace(/\s/g, ''))) {
            msg += "<?php echo $t->_('Email invalid!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>