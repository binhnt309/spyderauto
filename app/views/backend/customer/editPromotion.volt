<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('Update coupon') }}</h1>
        <a href="{{ url('admin/customer/promotion') }}">
            <button class="btn btn-primary" type="button">{{ t._('List coupon') }}</button>
        </a>
    </div>
    <!-- /.col-lg-12 -->
</div>
{{ partial('customer/_promotionForm') }}