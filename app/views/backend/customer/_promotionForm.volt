<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
        {{ form('admin/customer/' ~ (coupon.getId() ? 'editPromotion/' ~ coupon.getId() : 'createPromotion'), 'id' : 'frm_coupon', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}

        <div class="form-group">
            <div class="row">
                <div class="col-sm-3">
                    <label>{{ t._('Coupon Code') }}</label> <span class="required">*</span>
                    {{ text_field('code', 'class' : 'form-control', 'value' : (req.getPost('code') ? req.getPost('code')
                    : coupon.getCode()) ) }}
                </div>
                <div class="col-sm-2">
                    <label>{{ t._('Order Min') }}</label>

                    <div class="input-group">
                        {{ text_field('limit_amount', 'class' : 'form-control', 'value' : (req.getPost('limit_amount') ?
                        req.getPost('limit_amount') : coupon.getLimitAmount()), 'placeholder' : '0' ) }}
                        <span class="input-group-addon">$</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <label>{{ t._('Amount') }}</label> <span class="required">*</span>

                    <div class="input-group">
                        {{ text_field('amount', 'class' : 'form-control', 'value' : (req.getPost('amount') ?
                        req.getPost('amount') : coupon.getAmount()) ) }}
                        <span class="input-group-addon select">
                            {{ select('unit', unit, 'class' : 'form-control', 'value' : req.getPost('unit') ?
                            req.getPost('unit') : coupon.getUnit() ) }}
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label>{{ t._('Start') }}</label>
                    {{ text_field('start', 'class' : 'form-control datetime', 'value' : (req.getPost('start')
                    ? req.getPost('start') : coupon.getStart()) ) }}
                </div>
                <div class="form-group">
                    <label>{{ t._('End') }}</label>
                    {{ text_field('end', 'class' : 'form-control datetime', 'value' : (req.getPost('end')
                    ? req.getPost('emd') : coupon.getEnd()) ) }}
                </div>
                <div class="form-group">
                    <label>{{ t._('Active') }}</label>
                    <div class="row">
                        <div class="col-sm-3">
                            {{ select('active', ['0' : 'No', '1' : 'Yes'], 'value': (req.getPost('active') ?
                            req.getPost('active') :
                            (coupon.getActive() ? coupon.getActive() : 1)), 'class' : 'form-control') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label>{{ t._('Customer') }}:</label>
                    {{ select('uid[]', customer, 'using' : ['id', 'username'], 'class' : 'form-control', 'value' :
                    coupon_user_selected, 'multiple' : true, 'size' : 10) }}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ coupon.getId() ? t._('Update') : t._('Create') }}
                    </button>
                    <a href="javascript:;" class="btn btn-default pull-right">{{ t._('Send mail') }}
                    </a>
                </div>
            </div>
        </div>

        {{ end_form() }}
    </div>
</div>

<style type="text/css">
    .input-group-addon.select {
        padding: 0;
    }
    .input-group-addon select {
        border: medium none;
        height: 36px;
        padding: 0;
        width: 40px !important;
    }
</style>

<script type="text/javascript">

    $(document).ready(function () {
        $('form[name=customer]').prop('action', window.location.href);

        $('.datetime').datetimepicker({
            format: 'Y-m-d H:i'
        });

    });

    function _formValidate() {
        var result = true,
            msg = '';
        if ($('#code').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Coupon code can\'t be empty!') ?>\n";
            result = false;
        }

        if ($('#amount').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Amount of coupon can\'t be empty!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg);
        }
        return result;
    }
</script>