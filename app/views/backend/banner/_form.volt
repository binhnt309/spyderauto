<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
    </div>
    {{ form('', 'name' : 'banner', 'role' : 'form', 'onsubmit' : 'return _formValidate()', 'enctype' :
    'multipart/form-data') }}
    <div class="col-sm-8">
        <div class="form-group">
            <label>{{ t._('Title') }}</label><span class="required">*</span>
            {{ text_field('title', 'class' : 'form-control',
                'value' : (req.getPost('title') ? req.getPost('title') : banner.getTitle() )) }}
        </div>

        <div class="form-group">
            <label for="link">{{ t._('Link') }}</label>
            {{ text_field('link', 'class' : 'form-control', 'value' : (req.getPost('link') ? req.getPost('link') :
            banner.getLink() ), 'placeholder' : 'example: http://example.com/article/title-of-article') }}
        </div>

        <div class="col-sm-4" style="padding-left: 0;">
            <div class="form-group">
                <label for="pos">{{ t._('Position') }}</label>
                {{ text_field('pos', 'class' : 'form-control', 'value' : (req.getPost('pos') ? req.getPost('pos') :
                banner.getPos() ), 'placeholder':'0') }}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <label>{{ t._('Status') }}</label>
                {{ select('status', status, 'value': banner.getStatus(),
                'class' : 'form-control' ) }}
            </div>
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ banner.getId() ? t._('Update') : t._('Create') }}</button>
        </div>

    </div>

    <div class="col-sm-4">
        <div class="form-group image">
            <label>{{ t._('Banner') }}</label>
            <input type="hidden" name="cur_image" value="{{ banner.image }}">
            <input type="file" name="image"/>
            <img src="{{ image }}" class="image" alt="{{banner.getImage() }}"/>
        </div>
    </div>

    {{ end_form() }}
</div>

<style type="text/css">
    .image img { max-height: 240px; padding: 10px 0; max-width: 100%; }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=banner]').prop('action', window.location.href);
    });

    function _formValidate() {
        var result = true,
            msg = '';
        if ($('#title').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Title can not be empty!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>