<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="text-center">
            <img src="{{ base_url }}assets/images/email2.png" class="text-center">
            <h3>{{ t._('Backend Login') }}</h3>
        </div>
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ t._('Please Sign In') }}</h3>
            </div>
            <div class="panel-body">
                {{ form(router.getRewriteUri(), 'name':'login', 'method':'post', 'role':'form') }}
                    {{ hidden_field('returnUrl', 'value':req.get('returnUrl')) }}
                    {{ flashSession.output() }}
                    <fieldset>
                        <div class="form-group {{ errType == 'username' ? 'has-error' : '' }}">
                            {{ text_field('credential','class':'form-control', 'autofocus' : '', 'placeholder': t._('E-mail Or Username')) }}
                        </div>
                        <div class="form-group {{ errType == 'pwd' ? 'has-error' : '' }}">
                            {{ password_field('pwd', 'class':'form-control', 'placeholder': t._('Password')) }}
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ check_field('remember', 'value': t._('Remember Me')) }}
                                {{ t._('Remember Me') }}
                            </label>
                        </div>
                        {{ submit_button('class': 'btn btn-lg btn-primary btn-block', 'value' : t._('Login')) }}
                    </fieldset>
                {{ end_form() }}
            </div>
        </div>
    </div>
</div>
{% if remember %}

<script type="text/javascript">
    $(document).ready(function() {
        $('#remember').prop('checked', true);
    });
</script>

{% endif %}

<script type="text/javascript">
    $(document).ready(function() {
        /*$('#email').keypress(function(e) {
            if(e.keyCode == 13 & $(this).val() == 'admin') {
                $('form').attr('novalidate', '');
            }
        });*/
        $(window).keypress(function(e) {
            if(e.keyCode == 13 & $('#email').val() == 'admin') {
                $('form').attr('novalidate', '');
            }
        });
    });

</script>