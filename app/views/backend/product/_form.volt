<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
        {{ form('', 'name' : 'product', 'role' : 'form', 'onsubmit' : 'return _formValidate()'
        , 'enctype' : 'multipart/form-data') }}
        <div class="panel panel-default">
            <div class="panel-heading">Basic information</div>
            <div class="panel-body">
                <div class="form-group">
                    <label>{{ t._('Title') }}</label>&nbsp;<span class="required">*</span>
                    {{ text_field('title', 'class' : 'form-control', 'maxlength':255,
                    'value' : (req.getPost('title') ? req.getPost('title') : product.getTitle() )) }}
                </div>

                <div class='row'>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label for="cid">{{ t._('Category') }}</label>
                                            <select name="cid" id="cid" class="form-control">
                                                <option> - {{ t._('Select category') }}</option>
                                                {{ category_options }}
                                            </select>
                                        </div>
                                        <div class='col-sm-6'>
                                            <label for='brand'>{{ t._('Brand') }}</label>
                                            {{ select('brand', brands, 'using':['id', 'brand_name'],
                                            'class':'form-control',
                                            'useEmpty': true, 'emptyText': ' - Select brand',
                                            'value':(req.getPost('brand') ? req.getPost('brand') : product.getBrandId()
                                            )) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class='col-sm-6'>
                                            <label for='price'>{{ t._('Price') }}</label>
                                            {{ text_field('price', 'class' : 'form-control',
                                            'value' : (req.getPost('price') ? req.getPost('price') : product.getPrice()
                                            )) }}
                                        </div>
                                        <div class='col-sm-6'>
                                            <label for='model'>{{ t._('Model') }}</label>
                                            {{ select('model', models, 'using':['id', 'model_name'],
                                            'class':'form-control',
                                            'value':(req.getPost('model') ? req.getPost('model') : product.getModelId()
                                            )) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label for='currency'>{{ t._('Currency') }}</label>
                                            {{ select('currency', currencies, 'using':['id', 'symbol'], 'value':
                                            product.getCurrencyId(), 'class':'form-control') }}
                                        </div>
                                        <div class="col-sm-6">
                                            <label for='year'>{{ t._('Year') }}</label>
                                            {{ select('year', years, 'using':['id', 'year'], 'class':'form-control',
                                            'value':(req.getPost('year') ? req.getPost('year') : product.getYearId() ))
                                            }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4" style="position: relative;">
                                <label>{{ t._('Current selected') }}</label>
                                {{ select('properties[]', properties, 'class' : 'form-control properties',
                                'multiple':'true', 'style' : 'height: 195px' ) }}

                                <div class="group-action">
                                    <a href="javascript:;" class="next">
                                        <i class="fa fa-toggle-right"></i>
                                    </a>
                                    <a href="javascript:;" class="back">
                                        <i class="fa fa-toggle-left"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="sku">SKU</label>
                            {{ text_field('sku', 'value':product.getSku(), 'class' : 'form-control' ) }}
                        </div>
                        <div class="form-group">
                            <label for='stock'>{{ t._('In Stock') }}</label>
                            {{ text_field('stock', 'class' : 'form-control',
                            'value' : (req.getPost('stock') ? req.getPost('stock') : product.getStock() ),
                            'placeholder':0) }}
                        </div>
                        <div class='form-group'>
                            <label for='status'>{{ t._('Status') }}</label>
                            {{ select('status', status, 'class':'form-control',
                            'value':(req.getPost('status') ? req.getPost('status') : product.getStatus() )) }}
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Extra information</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label>{{ t._('Alias') }}</label> <i style='font-size:11px'>
                                {{ t._('This will be display on title page, if not set, title page is Title of product') }}</i>
                            {{ text_field('alias', 'class' : 'form-control', 'maxlength':255,
                            'value' : (req.getPost('alias') ? req.getPost('alias') : product.getAlias() )) }}
                        </div>
                        <div class="form-group">
                            <div class='row'>
                                <div class='col-sm-4'>
                                    <label for='sale_off'>{{ t._('Price sale-off') }}</label>
                                    {{ text_field('sale_off', 'class' : 'form-control',
                                    'value' : (req.getPost('sale_off') ? req.getPost('sale_off') : product.getSaleOff()
                                    )) }}
                                </div>
                                <div class='col-sm-8'>
                                    <label for='sale_off_to'>{{ t._('Date end sale-off') }}</label>
                                    {{ text_field('sale_off_to', 'class':'form-control',
                                    'value': (req.getPost('sale_off_to') ? req.getPost('sale_off_to')
                                    :product.getSaleOffTo() )) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class='row'>
                                <div class='col-sm-4'>
                                    <label for='is_front'>{{ t._('New product') }}</label>
                                    <input <?php echo $req->isPost() ? ($req->getPost('is_front') ? 'checked' : '') : ($product->getIsFront() ? 'checked' : ($product->getId() ? '' : 'checked')) ?>
                                        type="checkbox" name="is_front" id="is_front">
                                </div>
                                <div class='col-sm-4'>
                                    <label for='is_front'>{{ t._('Feature product') }}</label>
                                    <input <?php echo $req->isPost() ? ($req->getPost('is_feature') ? 'checked' : '') : ($product->getIsFeature() ? 'checked' : ($product->getId() ? '' : 'checked')) ?>
                                        type="checkbox" name="is_feature" id="is_feature">

                                </div>
                                <div class='col-sm-3 pull-right'>
                                    <label for='pos'>{{ t._('Position') }}</label>
                                    {{ text_field('pos', 'class':'form-control',
                                    'value': (req.getPost('pos') ? req.getPost('pos') : product.getPos() ),
                                    'placeholder': '0') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="row">
                            <div class="col-sm-6">

                                <div class="form-group">
                                    <label>{{ t._('Illustration') }}</label>
                                    <input type="file" name="illustration" style="padding-bottom: 12px;"/>
                                </div>
                                <div class="form-group">
                                    <label>{{ t._('Or link') }}</label>
                                    {{ text_field('url_link', 'class': 'form-control', 'value' : product.getIllustration(), 'placeholder' : 'Enter url of link online here') }}
                                </div>
                            </div>
                            <div class="col-sm-6">

                                <img src="{{ product_illustration }}" class="illustration"
                                     alt="{{product_illustration }}"/>
                                {{ hidden_field('cur_illustration', 'value': product.getIllustration() )}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class='form-group'>
                    <label for='brief'>{{t._('Brief')}}</label>
                    {{ text_area('brief', 'class' : 'form-control', 'value':(req.getPost('brief') ? req.getPost('brief')
                    : product.getBrief() )) }}
                </div>

                <div class='form-group'>
                    <label for='des'>{{t._('Content')}}</label>
                    {{ text_area('des', 'class' : 'form-control', 'rows':20,
                    'value':(req.getPost('des') ? req.getPost('des') : product.getDes() )) }}
                </div>

                <div class='form-group'>
                    <label for='tags'>{{t._('Tags')}}</label>
                    {{ text_field('tags', 'class' : 'form-control', 'placeholder':'Maximum 255 characters',
                    'maxlength':255,
                    'value':(req.getPost('tags') ? req.getPost('tags') : product.getTags() )) }}
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <label>{{ t._('Thumb image') }}</label>
                        <input type="file" id="thumb">
                        <div class="thumb-msg"></div>
                    </div>
                    <div class="col-sm-9">
                        <div class="row list-thumb">
                            <!--<div class="col-sm-2">
                                <img src="http://local.spyderauto.com/uploads/products/no-image.png">
                            </div>-->
                            <?php
                            $thumbs = json_decode($product->getThumbs(), true);
                            if(!empty($thumbs)) {
                                $folder_time = date('Y-m-d', strtotime($product->getCreatedTime()));
                                foreach($thumbs as $thumb) {
                                    $img = $this->url->get() . $this->config->application->uploadUri . 'products/no-image.png';
                                    if(file_exists($this->config->application->uploadDir . 'products' . DS . $folder_time . DS . 'thumbs' . DS . $thumb) & !empty($thumb)) {
                                        $img = $this->url->get() . $this->config->application->uploadUri . 'products/' . $folder_time . '/thumbs/' . $thumb;
                                    }
                                    ?>
                                    <div class="col-sm-2 block">
                                        <a href="javascript:;" onclick="deleteThumb(this)" rel="{{ thumb }}"><i class="glyphicon glyphicon-remove"></i></a>
                                        <img src="{{ img }}">
                                    </div>
                            <?php
                                }
                            }
                            ?>
                        </div>
                        <input type="hidden" value="<?php echo $thumbs ? implode(';', $thumbs) : '' ?>" name="thumbs" id="thumbs" />
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">SEO data</div>
            <div class="panel-body">
                <div class='form-group'>
                    <label for='keyword'>{{t._('Keyword')}}</label>
                    {{ text_field('keyword', 'class' : 'form-control', 'placeholder':'Keyword for page, maximum 75
                    characters',
                    'maxlength':255,
                    'value':(req.getPost('keyword') ? req.getPost('keyword') : seo.getKeyword() )) }}
                </div>
                <div class='form-group'>
                    <label for='description'>{{t._('Description')}}</label>
                    {{ text_field('description', 'class' : 'form-control', 'placeholder':'Description for page, maximum
                    255 characters',
                    'maxlength':255,
                    'value':(req.getPost('description') ? req.getPost('description') : seo.getDescription() )) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ product.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
        {{ end_form() }}
    </div>
</div>

<style type="text/css">
    .illustration {
        max-height: 220px;
        max-width: 100%;
        padding-top: 10px;
    }

    input[type=checkbox] {
        position: relative;
        top: 2px;
        left: 5px;
    }

    .group-action {
        position: absolute;
        left: -3.5%;
        top: 80px;
    }

    .group-action a {
        display: inline-block;
        padding: 5px 0;
        text-align: center;
        width: 100%;
    }

    .group-action i {
        transform: scale(2, 2);
    }
    .list-thumb img { width: 100%; margin-bottom: 30px; }
    .list-thumb a { position: absolute; display: none; }
    .thumb-msg { margin-top: 10px; }
    .list-thumb .block:hover a { display: block; }
</style>

<script type="text/javascript">

    $(document).ready(function () {
        $('form[name=product]').prop('action', window.location.href);
        tinymce.init({
            selector: 'textarea#des',
            plugins: [
                "advlist autolink link lists charmap print preview hr anchor pagebreak code image",
                "searchreplace wordcount visualblocks visualchars insertdatetime nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager", "youTube"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent " +
                "| link unlink anchor | forecolor backcolor  | print preview code | image responsivefilemanager | youTube",
            image_advtab: true,
            external_filemanager_path: "/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "/filemanager/plugin.min.js"},
            relative_urls: false,
            remove_script_host: false,
            validate_elements: 'iframe'
        });
        $('#sale_off_to').datetimepicker({
            format: 'Y-m-d H:i'
        });

        // Load model
        $('#brand').change(function () {
            $.ajax({
                url: '/admin/product/create?option=loadModel&_=' + Math.random(),
                data: {id: $(this).val()},
                dataType: 'JSON',
                success: function (d) {
                    var html_option = '<option value="0"> - Select a model';
                    if (typeof d == 'object') {
                        for (var o in d) {
                            html_option += "<option value='" + d[o]['id'] + "'>" + d[o]['name'] + "</option>";
                        }
                    }
                    $('#model').html(html_option).change(function () {
                        $.ajax({
                            url: '/admin/product/create?option=loadYear&_=' + Math.random(),
                            data: {id: $(this).val()},
                            dataType: 'JSON',
                            success: function (d) {
                                var html_option = '<option value="0"> - Select a model';
                                if (typeof d == 'object') {
                                    for (var o in d) {
                                        html_option += "<option value='" + d[o]['id'] + "'>" + d[o]['year'] + "</option>";
                                    }
                                }
                                $('#year').html(html_option);
                            }
                        });
                    });
                }
            });
        });
        $('.group-action a').click(function (e) {
            e.preventDefault();
            if ($(this).hasClass('next')) {// Add action
                if ($('#brand').val() == 0 || $('#brand').val() == '') {
                    alert('Select brand first');
                    return;
                }
                var properties_value = [$('#brand').val()],
                    properties_data = [$('#brand option:selected').text()];
                if ($('#model').val() != 0 & $('#model').val() != '' & $('#year').val() != null) {
                    properties_value.push($('#model').val());
                    properties_data.push($('#model option:selected').text());
                }
                if ($('#year').val() != 0 & $('#year').val() != '' & $('#year').val() != null) {
                    properties_value.push($('#year').val());
                    properties_data.push($('#year option:selected').text());
                }
                if ($('.properties option[value="' + properties_value.join(',') + '"]').length == 0)
                    $('.properties').append('<option value="' + properties_value.join(',') + '">' + properties_data.join(', ') + '</option>');
            } else {
                // Remove list selected
                $('.properties option').each(function () {
                    if ($(this).prop('selected')) {
                        $(this).remove();
                    }
                });
            }
        });

        // Upload thumb images
        $('#thumb').on('change', function(e) {
            $('.thumb-msg').html('Uploading ...');
            var url_upload = '<?php echo $this->url->get('admin/product/' . $action) ?>?option=upload&_=' + Math.random();
            uploadAjax(e, url_upload, function(data) {
                try {
                    if (typeof data.error === 'undefined') {
                        // Success so call function to process the form
                        if (data.success) {
                            $('.list-thumb').append('<div class="col-sm-2 block"><a href="javascript:;" onclick="deleteThumb(this)" rel="' + data.file_name + '"><i class="glyphicon glyphicon-remove"></i></a>' +
                            '<img src="' + data.img_url + '">' +
                            '</div>');

                            var thumbs = $('#thumbs').val().split(';');
                            thumbs.push(data.file_name);
                            $('#thumbs').val(thumbs.join(';'));

                        } else {

                        } // ------------
                        $('.thumb-msg').html(data.msg);

                    } else {
                        // Handle errors here
                        console.log('ERRORS: ' + data.error);
                        // ------------
                        $('.thumb-msg').html('Can\'t received data with JSON type');
                    }
                } catch(e) {
                    $('.thumb-msg').html('');
                }
            });
        })
    });

    function _formValidate() {
        $('.properties option').each(function () {
            $(this).prop('selected', true);
        });
    }

    // Variable to store your files
    var files;
    // Grab the files and set them to our variable
    function uploadAjax(event, url_upload, callback) {
        files = event.target.files;

        // START A LOADING SPINNER HERE
        // Create a formdata object and add the files
        var data = new FormData();
        $.each(files, function (key, value) {
            data.append('img_file', value);
        });

        $.ajax({
            url: url_upload,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR) {
                /*if (typeof data.error === 'undefined') {
                    // Success so call function to process the form
                    if (data.success) {
                        $('#img').val(data.file_name);
                        $('.thumb img').prop('src', data.img_url);
                        $('.upload-msg').html(data.msg);
                    } else {

                    } // ------------
                    $('.upload-msg').html(data.msg);

                } else {
                    // Handle errors here
                    console.log('ERRORS: ' + data.error);
                    // ------------
                    $('.upload-msg').html('Can\'t received data with JSON type');
                }*/
                if(typeof callback == 'function') {
                    callback(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // ------------
                $('.upload-msg').html('Can\'t received data with JSON type');
            }
        });
    }

    function deleteThumb(obj) {
        var thumbs = $('#thumbs').val().split(';');
        for(var t in thumbs) {
            if(thumbs[t] == $(obj).prop('rel')) {
                delete thumbs[t];
                break;
            }
        }

        $(obj).parent().remove();
        $('#thumbs').val(thumbs.join(';'));

    }

</script>