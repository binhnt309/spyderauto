<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('List product') }}</h1>
        <a href="{{ url('admin/product/create') }}">
            <button class="btn btn-primary" type="button">{{ t._('Add new') }}</button>
        </a>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        {{ flashSession.output() }}
        <div class="panel panel-default">
            <!--<div class="panel-heading">
                DataTables Advanced Tables
            </div>-->
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th><input type="checkbox" class='check-all'></th>
                            <th>{{ t._('#') }}</th>
                            <th>{{ t._('Title') }}</th>
                            <th>{{ t._('Price') }}</th>
                            <th>{{ t._('Illustration') }}</th>
                            <th>{{ t._('In stock') }}</th>
                            <th title='{{ t._("Item amount has been sale") }}'>{{ t._('Sale') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- data will be displayed here -->
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>

<style type="text/css">
    .table-responsive img { width: 120px; }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('#dataTables-example').dataTable(
                {
                    "oLanguage": {
                        "sLengthMenu": "_MENU_ " + '<?php echo $t->_("rows/page"); ?>',
                        "sInfo": "<?php echo $t->_('Showing') ?> _START_ to _END_ of _TOTAL_ <?php echo $t->_('result') ?>",
                        "sSearch": "<?php echo $t->_('Search') ?>: ",
                        "sPrevious": "<?php echo $t->_('Previous') ?>",
                        "sNext": "<?php echo $t->_('Next') ?>",
                        "sEmptyTable": "<?php echo $t->_('No data available in table') ?>",
                        "sInfoEmpty": "<?php echo $t->_('Showing 0 to 0 of 0 entries') ?>"
                    },
                    "order": [[ 1, "desc" ]],
                    "lengthMenu": [25, 50, 75, 100 ],
                     /*'pageLength': 2,*/
                    "columnDefs": [
                        {"orderable": false, "targets": [0, 4, 7] }
                    ],
                    "processing": true,
                    "serverSide": true,
                    'ajax': {
                        'url': '/admin/product/index?_=' + Math.random(),
                        'data': function (d) {
                            d.key = $('#dataTables-example_filter input').val();
                        }
                    }
                }
        );
        var tmp_val = '';
        // Change default keypress search
        $("#dataTables-example_filter input").unbind().keyup(function (e) {
            if (e.keyCode == 13 & tmp_val != this.value) {
                oTable.fnFilter(this.value);
                tmp_val = this.value;
            }
        });
        $('a.delete').click(function (e) {
            if (!confirm("<?php echo $t->_('Are you sure?') ?>"))
                e.preventDefault();
        })
    });
    function __delete(event) {
        var e = event ? event : window.event;
        if (!confirm("<?php echo $t->_('Are you sure?') ?>"))
            e.preventDefault();
    }
</script>