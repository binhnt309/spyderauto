<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('System config') }}</h1>
        <a href="#" class="save-config">
            <button class="btn btn-primary" type="button">{{ t._('Save config') }}</button>
        </a>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
    </div>

    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                {{ form( 'admin/system/index', 'name' : 'config','method' : 'post', 'enctype' : 'multipart/form-data', 'onsubmit' : 'return confirm("Do you want save config?")') }}
                <div class="panel-body">
                    {% for conf in config %}
                    <div class="form-group">

                        <label for="{{ conf.getKeyword() }}" title="{{ conf.getDes() }}">{{ conf.getAlias() }}</label>
                        {% if(conf.getFieldType() == 'input') %}
                        <input class="form-control" type="text" name="{{ conf.getKeyword() }}"
                               value="{{ conf.getValue() }}" placeholder="{{ conf.getDes() }}"/>
                        {% elseif (conf.getFieldType() == 'textarea') %}
                        <textarea class="form-control" name="{{ conf.getKeyword() }}">{{ conf.getValue() }}</textarea>
                        {% elseif (conf.getFieldType() == 'file') %}
                        <div class="row">
                            <div class="col-sm-5">
                                <input type="file" name="{{ conf.getKeyword() }}"/>
                            </div>
                            <div class="col-sm-7">
                                <img style="max-width: 100%; max-height: 150px;" src="/uploads/logo/{{ (conf.getValue() ? conf.getValue() : 'default.png') }}" alt="{{ conf.getAlias() }}"/>
                            </div>
                        </div>
                        {% endif %}

                    </div>

                    {% endfor %}
                    <div class="form-group">
                        <button class="btn btn-primary btn-save" type="submit">{{ t._('Save config') }}</button>
                    </div>
                </div>
                {{ end_form() }}
            </div>
        </div>

    </div>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('a.save-config').click(function(e) {
            e.preventDefault();
            $('button.btn-save').click();
        });
        tinymce.init({
            selector: 'textarea',
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak code",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true,
            external_filemanager_path: "/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "/filemanager/plugin.min.js"},
            relative_urls: false,
            remove_script_host: false
        });
    });
</script>