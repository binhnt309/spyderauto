<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
        {{ form('', 'name' : 'currency', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}

        <div class="form-group">
            <label>{{ t._('Name') }}</label><span class="required">*</span>
            {{ text_field('name', 'class' : 'form-control',
                'value' : (req.getPost('name') ? req.getPost('name') : currency.getName() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Abbreviation') }}</label>
            {{ text_field('abbreviation', 'class' : 'form-control',
                'value' : (req.getPost('abbreviation') ? req.getPost('abbreviation') : currency.getAbbreviation() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Symbol') }}</label>
            {{ text_field('symbol', 'class' : 'form-control',
                'value' : (req.getPost('symbol') ? req.getPost('symbol') : currency.getSymbol() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Country') }}</label>
            {{ select('country', 'class' : 'form-control', 'using' : ['id','name'], countries,
                'value' : (req.getPost('country') ? req.getPost('country') : currency.getCountryId() )) }}
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ currency.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
        {{ end_form() }}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=currency]').prop('action', window.location.href);
    });
    function _formValidate() {
        var result = true,
                msg = '';
        if ($('#name').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Name empty!') ?>\n";
            result = false;
        }
        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>