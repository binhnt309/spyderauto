<div class="row">
    {{ form('', 'name' : 'art_category', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}
    <div class="col-sm-12">
        {{ flashSession.output() }}
        <div class="form-group">
            <label>{{ t._('Category name') }}</label><span class="required">*</span>
            {{ text_field('name', 'class' : 'form-control', 'value' : (req.getPost('name') ? req.getPost('name') :
            art_category.getName() )) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Category Alias') }}</label>
            {{ text_field('alias', 'class' : 'form-control', 'value' : (req.getPost('alias') ? req.getPost('alias') :
            art_category.getAlias() )) }}
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>{{ t._('Parent Group') }}</label>
            <select name="parent_id" id="parent_id" class="form-control">
                <option> - {{ t._('Select category') }}</option>
                {{ category_options }}
            </select>
        </div>
    </div>

    <div class="col-sm-6"></div>
    <div class="clearfix"></div>

    <div class="col-sm-3">
        <div class="form-group">
            <label>{{ t._('In Menu') }}</label>
            <input type="checkbox" <?php echo $art_category->getInMenu() ? 'checked' : '' ?> name="in_menu"/>
        </div>
    </div>
    <div class="col-sm-9">

        <div class="form-group">
            <label>{{ t._('In Footer') }}</label>
            <input type="checkbox" <?php echo $art_category->getInFooter() ? 'checked' : '' ?> name="in_footer"/>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>{{ t._('Position') }}</label>
            {{ text_field('pos', 'class' : 'form-control',
            'value' : (req.getPost('pos') ? req.getPost('pos') : art_category.getPos()) ) }}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label>{{ t._('Status') }}</label>
            {{ select('status', status, 'value': req.getPost('status') ? req.getPost('status') :
            art_category.getStatus(), 'class' : 'form-control' ) }}
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ art_category.getId() ? t._('Update') : t._('Create') }}
            </button>
        </div>
    </div>
</div>
{{ end_form() }}
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=art_category]').prop('action', window.location.href);

    });
    function _formValidate() {
        var result = true,
            msg = '';
        if ($('#name').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Name not empty!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg);
        }
        return result;
    }
</script>