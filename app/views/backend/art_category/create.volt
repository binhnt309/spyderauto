<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('Create category of article') }}</h1>
        <a href="{{ url('admin/art_category/index') }}">
            <button class="btn btn-primary" type="button">{{ t._('Back to list') }}</button>
        </a>
    </div>
    <!-- /.col-lg-12 -->
</div>
{{ partial('art_category/_form') }}