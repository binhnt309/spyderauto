<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
        {{ form('', 'name' : 'brand', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}
        <div class="form-group">
            <label>{{ t._('Brand name') }}</label><span class="required">*</span>
            {{ text_field('brand', 'class' : 'form-control',
                'value' : (req.getPost('brand') ? req.getPost('brand') : brand.getBrandName() )) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Brief') }}</label>
            {{ text_field('brief', 'class' : 'form-control',
                'value' : (req.getPost('brief') ? req.getPost('brief') : brand.getBrief() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Status') }}</label>
            {{ select('status', status, 'value': brand.getStatus(),
            'class' : 'form-control' ) }}
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ brand.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
        {{ end_form() }}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=brand]').prop('action', window.location.href);
    });

    function _formValidate() {
        var result = true,
                msg = '';
        if ($('#brand').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Brand empty!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>