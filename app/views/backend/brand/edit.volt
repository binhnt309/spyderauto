<div class="row">
    <div class="col-lg-12 header-top">
        <h1 class="page-header">{{ t._('Update brand') }}</h1>
        <a href="{{ url('admin/brand/index') }}">
            <button class="btn btn-primary" type="button">{{ t._('Back to list') }}</button>
        </a>
    </div>
    <!-- /.col-lg-12 -->
</div>
{{ partial('brand/_form') }}