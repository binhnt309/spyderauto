<div class="row">
    {{ form('', 'name' : 'year', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}
    <div class="col-sm-12">
        {{ flashSession.output() }}
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>{{ t._('Year From') }}</label><span class="required">*</span>
            {{ text_field('year', 'class' : 'form-control',
            'value' : (req.getPost('year') ? req.getPost('year') : year.getYear() )) }}
        </div>

    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>{{ t._('Year To') }}</label>
            {{ text_field('end_year', 'class' : 'form-control',
            'value' : (req.getPost('end_year') ? req.getPost('end_year') : year.getEndYear() )) }}
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>{{ t._('Brand') }}</label>
            {{ select('brand', 'using':['id', 'brand_name'], brands, 'class' : 'form-control',
            'value' : (req.getPost('brand') ? req.getPost('brand') : year.getBrandId() ), 'useEmpty':true,
            'emptyText':'---') }}
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>{{ t._('Model') }}</label>
            {{ select('model', 'using':['id', 'model_name'], models, 'class' : 'form-control',
            'value' : (req.getPost('model') ? req.getPost('model') : year.getModelId() ), 'useEmpty':true,
            'emptyText':'---') }}
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <label>{{ t._('Status') }}</label>
            {{ select('status', status, 'value': year.getId() ? year.getStatus() : 1, 'class' : 'form-control' ) }}
        </div>
        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ year.getId() ? t._('Update') : t._('Create') }}
            </button>
        </div>
    </div>
    {{ end_form() }}
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $('form[name=year]').prop('action', window.location.href);

        // Load model by brand
        $('#brand').change(function () {
            $.ajax({
                url: '/admin/year/<?php echo $year->getId() ? 'edit':'create' ?>/?_=' + Math.random(),
                data: {option: 'getModel', brand: $(this).val()},
                dataType: 'JSON',
                success: function (d) {
                    try {
                        if (typeof d == 'object') {
                            $('#model').html('<option>--</option>');
                            for (var o in d) {
                                $('#model').append('<option value="' + o + '">' + d[o] + '</option>')
                            }
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }
            })
        });
    });

    function _formValidate() {
        var result = true,
            msg = '';
        if ($('#year').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Email empty!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>