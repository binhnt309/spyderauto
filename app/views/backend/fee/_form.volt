<div class="row">
    {{ form('', 'name' : 'fee', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}
    <div class="col-sm-9">

        {{ flashSession.output() }}

        <div class="form-group">
            <label for="country_id">{{ t._('Country') }}</label><span class="required">*</span>
            {{ select('country_id', countries, 'using': ['id', 'name'], 'class' : 'form-control',
            'value' : (req.getPost('country_id') ? req.getPost('country_id') : fee.getCountryId() )) }}
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="col-sm-9">
        <div class="form-group">
            <label for="city_id">{{ t._('State/Province') }}</label>
            <select class="form-control" name="city_id" id="city_id">
                <option value="0"> - {{ t._('Select state/province') }}</option>
                {% if cities | length %}
                {% for c in cities %}
                {% set selected = city == c.getId() ? 'selected' : '' %}
                <option {{ selected }} class="{{ c.getParentId() == country ? '' : 'hidden' }}" value="{{ c.getId() }}" data-parent="{{ c.getParentId() }}">{{ c.getName() }}</option>
                {% endFor %}
                {% endif %}
            </select>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="time_delivery">{{ t._('Time delivery') }} (unit: day/s)</label>
            {{ text_field('time_delivery', 'class' : 'form-control',
            'value' : (req.getPost('time_delivery') ? req.getPost('time_delivery') : fee.getTimeDelivery() )) }}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label>{{ t._('Ship fee') }}</label>
            {{ text_field('amount', 'class' : 'form-control',
            'value' : (req.getPost('amount') ? req.getPost('amount') : fee.getAmount() )) }}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label>{{ t._('Currency') }}</label>
            {{ select('currency', 'using': ['id', 'name'], currencies, 'value': fee.getCurrencyId(),
            'class' : 'form-control' ) }}
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ fee.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
    </div>
    {{ end_form() }}
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=fee]').prop('action', window.location.href);

        /*$('#time_delivery').datetimepicker({
            format: 'Y-m-d H:i'
        });*/

        $('#country_id').change(function() {
            var obj = $(this);
            $('#city_id option').each(function() {
                if($(this).data('parent') == $(obj).val()) {
                    $(this).removeClass('hidden');
                } else {
                    $(this).addClass('hidden');
                }
            });
        });
    });
    function _formValidate() {
        var result = true,
            msg = '';
        if ($('#country_id').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('You must select country first!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg);
        }
        return result;
    }
</script>