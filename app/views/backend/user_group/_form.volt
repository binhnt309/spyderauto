<div class="row">
    <div class="col-sm-12">
        {{ flashSession.output() }}
        {{ form('', 'name' : 'group', 'role' : 'form') }}
        <div class="form-group">
            <label>{{ t._('Group name') }}<span class="required">*</span></label>
            {{ text_field('name', 'class' : 'form-control', 'value' : (req.getPost('name') ? req.getPost('name') : group.getName() )) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Access control list') }}</label>
            {{ select('acl[]', acl_list, 'class' : 'form-control', 'multiple' : 'multiple', 'size': 10, 'value' : acl_selected, 'useEmpty':true, 'emptyText':'' ) }}
            {{ select('acl_of_group[]', acl_of_group, 'using' : ['acl_id', 'acl_id'], 'class' : 'hidden', 'multiple' : 'multiple', 'size': 10, 'value' : acl_of_group_selected) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Description') }}</label>
            <textarea class="form-control" name="description">{{ group.getDescription() }}</textarea>
        </div>

        <div class="form-group">
            <label>{{ t._('Status') }}</label>
            {{ select('status', ['0' : t._('Disabled'), '1' : t._('Enabled')], 'value': group.getStatus(), 'class' : 'form-control' ) }}
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ group.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
        {{ end_form() }}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=group]').prop('action', window.location.href);
    });
</script>