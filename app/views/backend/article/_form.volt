<div class="row">
    {{ form('', 'name' : 'article', 'role' : 'form', 'enctype' : 'multipart/form-data', 'onsubmit' : 'return
    _formValidate()') }}
    <div class="col-sm-8">
        {{ flashSession.output() }}
        <div class="form-group">
            <label>{{ t._('Title') }}</label> <span class="required">*</span>
            {{ text_field('title', 'class' : 'form-control', 'value' : (req.getPost('title') ? req.getPost('title') :
            article.getTitle() )) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Category') }}</label>
            <select class="form-control" id="cid" name="cid">
                <option> - {{ t._('Select category') }}</option>
                {{ category_options }}
            </select>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-4">
                    <label>On menu top:</label>
                    <input type="checkbox" name="top" id="top" <?php echo $article->getTop()? 'checked':'' ?>>
                </div>
                <div class="col-sm-8">
                    <label>On footer:</label>
                    <input type="checkbox" name="bottom" id="bottom" <?php echo $article->getBottom()? 'checked':'' ?>>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <label>{{ t._('Abbreviation') }}</label>
        <input type="file" name="abbreviation"/>
        <input type="hidden" value="{{ article.getThumb() }}" name="cur_thumb"/>
        <img src="{{ abbreviation }}"/>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <label>{{ t._('Brief') }}</label>
            {{ text_area('brief', 'class' : 'form-control', 'value' : (req.getPost('brief') ? req.getPost('brief') :
            article.getBrief() )) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Content') }}</label>
            {{ text_area('content', 'class' : 'form-control', 'style' : 'height: 400px', 'value' :
            (req.getPost('content') ? req.getPost('content')
            : article.getContent() )) }}
        </div>
    </div>

    <div class="col-sm-10">
        <div class="form-group">
            <label>{{ t._('Tags') }}</label>
            {{ text_field('tag', 'class' : 'form-control', 'value' : (req.getPost('tag') ? req.getPost('tag') :
            article.getTag() )) }}
        </div>
    </div>
    <div class="col-sm-2">

        <label>{{ t._('Position') }}</label>
        {{ text_field('pos', 'class' : 'form-control', 'value' : (req.getPost('pos') ? req.getPost('pos') :
        article.getPos() )) }}

    </div>
    <div class="clearfix"></div>

    <div class="col-sm-2">
        <div class="form-group">
            <label>{{ t._('Status') }}</label>
            {{ select('status', status, 'value': req.getPost('status') ? req.getPost('status') : article.getStatus(),
            'class' : 'form-control' ) }}
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ article.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
    </div>
    {{ end_form() }}
</div>

<style type="text/css">
    form img {
        max-height: 140px;
        margin-top: 2px;
        max-width: 205px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=article]').prop('action', window.location.href);

        tinymce.init({
            selector: 'textarea#content',
            plugins: [
                "advlist autolink link lists charmap print preview hr anchor pagebreak code image",
                "searchreplace wordcount visualblocks visualchars insertdatetime nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager", "youTube"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent " +
                "| link unlink anchor | forecolor backcolor  | print preview code | image responsivefilemanager | youTube",
            image_advtab: true,
            external_filemanager_path: "/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "/filemanager/plugin.min.js"},
            relative_urls: false,
            remove_script_host: false,
            validate_elements: 'iframe'
        });

    });
    function _formValidate() {
        var result = true,
            msg = '';
        if ($('#title').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Title not be empty!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>