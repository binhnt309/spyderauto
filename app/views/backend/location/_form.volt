<div class="row">
    <div class="col-sm-12">

        {{ flashSession.output() }}

        {{ form('', 'name' : 'location', 'role' : 'form', 'onsubmit' : 'return _formValidate()') }}
        <div class="form-group">
            <label>{{ t._('Location name') }}</label><span class="required">*</span>
            {{ text_field('name', 'class' : 'form-control',
                'value' : (req.getPost('name') ? req.getPost('name') : location.getName() )) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Abbreviation') }}</label>
            {{ text_field('alias', 'class' : 'form-control',
                'value' : (req.getPost('alias') ? req.getPost('alias') : location.getAlias() )) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Parent Location') }}</label>
            {{ select('parent_id', 'using' : ['id', 'name'], locations, 'class' : 'form-control',
                'useEmpty' : true, 'emptyText' : '', 'value' : location.getParentId() ) }}
        </div>

        <div class="form-group">
            <label>{{ t._('Type') }}</label>
            {{ select('type', type, 'class' : 'form-control', 'value': (req.getPost('type')?req.getPost('type') : location.getType()) ) }}
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">{{ location.getId() ? t._('Update') : t._('Create') }}</button>
        </div>
        {{ end_form() }}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('form[name=location]').prop('action', window.location.href);
    });
    function _formValidate() {
        var result = true,
                msg = '';
        if ($('#name').val().replace(/\s/g, '') == '') {
            msg += "<?php echo $t->_('Location name empty!') ?>\n";
            result = false;
        }

        if (!result) {
            alert(msg)
        }
        return result;
    }
</script>