<div class="header">
    <div class="box_header_user_menu">
        <ul class="user_menu"><li class="act first"><a href=""><div class="button-t"><span>Shipping &amp; Returns</span></div></a></li><li class=""><a href=""><div class="button-t"><span>Advanced Search</span></div></a></li><li class=""><a href=""><div class="button-t"><span>Create an Account</span></div></a></li><li class="last"><a href=""><div class="button-t"><span>Log in</span></div></a></li></ul>
    </div>
    <div class="header-right">
        <ul class="follow_icon">
            <li><a href="#"><img src="images/icon.png" alt=""/></a></li>
            <li><a href="#"><img src="images/icon1.png" alt=""/></a></li>
            <li><a href="#"><img src="images/icon2.png" alt=""/></a></li>
            <li><a href="#"><img src="images/icon3.png" alt=""/></a></li>
        </ul>
    </div><div class="clear"></div>
    <div class="header-bot">
        <div class="logo">
            <a href="index.html"><img src="images/logo.png" alt=""/></a>
        </div>
        <div class="search">
            <input type="text" class="textbox" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">
            <button class="gray-button"><span>Search</span></button>
        </div>
        <div class="clear"></div>
    </div>
</div>