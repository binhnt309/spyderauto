<div class="row article">
    <div class="col-sm-12">
        <img src="{{ img }}" class="thumb" alt="{{ article.getTitle() }}"/>

        <h2>{{ article.getTitle() }}</h2>
    </div>

    <div class="col-sm-12">
        {{ article.getContent() }}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // Search
        $('input.keyword').keyup(function(e){
            if(e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword='+ $(this).val();
            }
        }); // -------------
    });
</script>