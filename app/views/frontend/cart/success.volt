<div class="row payment-success">
    <div class="col-sm-3">

    </div>
    <div class="col-sm-6">
        {% if success %}
        <h2>Payment success</h2>
        {% else %}
        <h2 class="text-danger">Order error</h2>
        {% endif %}
        {{ flashSession.output() }}

        {% if success %}
        <div class="alert alert-success" role="alert">
            <p>You have create order and payment success. We will check payment and deliver products to you as soon as
                possible.</p>
        </div>
        <p>Now, you can go:</p>
        <ul>
            <li><a href="{{ url('member/order') }}">List order</a></li>
            <li><a href="{{ url('member/transaction') }}">List transaction</a></li>
        </ul>
        {% else %}
        <div class="alert alert-danger" role="alert">
            <p>Create order occur error. Please <a href="{{ url('contact') }}">contact</a> to us to resolve this
                problem.</p>
        </div>
        <p>You want to go:</p>
        <ul>
            <li><a href="{{ url('cart/index') }}">Your cart</a></li>
            <li><a href="{{ url('member/order') }}">List order</a></li>
            <li><a href="{{ url('member/transaction') }}">List transaction</a></li>
        </ul>
        {% endif %}
    </div>
</div>

<style type="text/css">
    .payment-success {
        background: #FFF;
        margin: 0;
        min-height: 400px;
    }

    .alert {
        margin-top: 30px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        // Search
        $('input.keyword').keyup(function(e){
            if(e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword='+ $(this).val();
            }
        }); // -------------
    });
</script>