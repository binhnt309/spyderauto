<div class="row cart-data">
    {{ form('cart/review', 'name' : 'shipping', 'method' : 'post', 'onsubmit' : '', 'role' : 'form', ' data-parsley-validate' : '') }}
    <input type="hidden" id="ship_new_address" name="ship_new_address" value="0"/>

    {% if user['id'] %}

    <div class="col-sm-1"></div>
    <div class="col-sm-5 member-login">
        <h2>{{ t._('Your Address') }}</h2>
        <i class="note">{{ t._('If you want change your address, go to you <a href="' ~ url('member/profile') ~ '">profile</a>')}}</i>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your Name') }}</div>
            <div class="col-sm-8">{{ user['fullname'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your Email') }}</div>
            <div class="col-sm-8">{{ user['email'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your telephone') }}</div>
            <div class="col-sm-8">{{ user['phone'] }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your mobile') }}</div>
            <div class="col-sm-8">{{ user['mobile'] }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your address') }}</div>
            <div class="col-sm-8">{{ user['address'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your address 2') }}</div>
            <div class="col-sm-8">{{ user['address_2'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your city/district') }}</div>
            <div class="col-sm-8">{{ user['district'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your state/province') }}</div>
            <div class="col-sm-8">{{ user['province'] }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('ZipCode') }}</div>
            <div class="col-sm-8">{{ user['zipcode'] }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your country') }}</div>
            <div class="col-sm-8">{{ countries[user['country']]['name'] }}</div>
        </div>
    </div>
    <div class="col-sm-5 border-left">
        <h2>{{ t._('Shipping To') }}</h2>
        <!--<i class="note">{{ t._('If empty, the shipping address will be the same your address') }}</i>-->

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your Name') }}<span class="required">*</span></div>
            <div class="col-sm-8">{{ text_field('fullname', 'class': 'form-control', 'placeholder' : 'Your name', 'required' : true) }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your Email') }}<span class="required">*</span></div>
            <div class="col-sm-8">
                <input type="email" name="email" id="email" placeholder="Enter your email" class="form-control"/>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your telephone') }}</div>
            <div class="col-sm-8">{{ text_field('phone', 'class': 'form-control', 'placeholder': 'Your phone') }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your mobile') }}</div>
            <div class="col-sm-8">{{ text_field('mobile', 'class': 'form-control', 'placeholder': 'Your mobile') }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your Country') }}<span class="required">*</span></div>
            <div class="col-sm-8">
                {{ select('country', countries_obj, 'using' : ['id', 'name'], 'class' : 'form-control', 'required' : true,
                'useEmpty' : true, 'emptyText' : '- Select country', 'value' : country ) }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your address') }}</div>
            <div class="col-sm-8">{{ text_field('address', 'class' : 'form-control', 'placeholder' : 'Your address')
                }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your address 2') }}</div>
            <div class="col-sm-8">{{ text_field('address_2', 'class' : 'form-control', 'placeholder' : 'Your address 2')
                }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your city') }}</div>
            <div class="col-sm-8">{{ text_field('district', 'class' : 'form-control', 'placeholder' : 'Your district')
                }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('ZipCode') }}</div>
            <div class="col-sm-8">
                {{ text_field('zip_code', 'class' : 'form-control', 'placeholder' : 'Your zip code') }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('State/Province') }}</div>
            <div class="col-sm-8">
                {{ text_field('province', 'class' : 'form-control hidden', 'placeholder' : 'Your province') }}
                <select class="form-control" name="city" id="city">
                    <option value="0"> - {{ t._('Select state') }}</option>
                    {% if cities | length %}
                    {% for c in cities %}
                    <option class="{{ c.getParentId() == country ? '' : 'hidden' }}" value="{{ c.getId() }}" data-parent="{{ c.getParentId() }}">{{ c.getName() }}</option>
                    {% endFor %}
                    {% endif %}
                </select>
            </div>
        </div>

    </div>

    <div class="clearfix"></div>
    <div class="col-sm-6 text-center" style="margin-top: 15px; margin-bottom: 15px;">
        <button class="btn btn-warning" type="submit" onclick="removeValidateForm()">{{ t._('Ship to this address') }}</button>
    </div>
    <div class="col-sm-5 text-center" style="margin-top: 15px; margin-bottom: 15px;">
        <button class="btn btn-warning" type="submit" onclick="validate_shipping()">{{ t._('Ship to this address') }}</button>
    </div>

    {% else %}

    <div class="col-sm-1"></div>
    <div class="col-sm-5 border-right">
        <h2>{{ t._('Shipping To') }}</h2>
        <i class="note">{{ t._('If empty, the shipping address will be the same your address') }}</i>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your Name') }} <span class="required">*</span></div>
            <div class="col-sm-8">{{ text_field('fullname', 'class': 'form-control', 'placeholder' : 'Your name', 'required' : true) }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your Email') }} <span class="required">*</span></div>
            <div class="col-sm-8">
                <input type="email" name="email" placeholder="Enter your email" class="form-control" required="true">
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your telephone') }}</div>
            <div class="col-sm-8">{{ text_field('phone', 'class': 'form-control', 'placeholder': 'Your phone') }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your mobile') }}</div>
            <div class="col-sm-8">{{ text_field('mobile', 'class': 'form-control', 'placeholder': 'Your mobile') }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your Country') }}<span class="required">*</span></div>
            <div class="col-sm-8">
                {{ select('country', countries_obj, 'using' : ['id', 'name'], 'class' : 'form-control', 'required' : true,
                'useEmpty' : true, 'emptyText' : '- Select country', 'value' : country ) }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your address') }} <span class="required">*</span></div>
            <div class="col-sm-8">{{ text_field('address', 'class' : 'form-control', 'placeholder' : 'Your address', 'required' : true)
                }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your address 2') }}</div>
            <div class="col-sm-8">{{ text_field('address_2', 'class' : 'form-control', 'placeholder' : 'Your address 2')
                }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Your city') }}</div>
            <div class="col-sm-8">{{ text_field('district', 'class' : 'form-control', 'placeholder' : 'Your district')
                }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('ZipCode') }}</div>
            <div class="col-sm-8">
                {{ text_field('zip_code', 'class' : 'form-control', 'placeholder' : 'Your zip code') }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('State/Province') }}</div>
            <div class="col-sm-8">
                {{ text_field('province', 'class' : 'form-control hidden', 'placeholder' : 'Your province') }}
                <select class="form-control" name="city" id="city">
                    <option value="0"> - {{ t._('Select state') }}</option>
                    {% if cities | length %}
                    {% for c in cities %}
                    <option class="{{ c.getParentId() == country ? '' : 'hidden' }}" value="{{ c.getId() }}" data-parent="{{ c.getParentId() }}">{{ c.getName() }}</option>
                    {% endFor %}
                    {% endif %}
                </select>
            </div>
        </div>
    </div>

    <div class="col-sm-5">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <h2>{{ t._("Login") }}</h2>
            </div>

            <div class="col-sm-2"></div>
            <div class="col-sm-8 msg-content"></div>
            <div class="clearfix"></div>

            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="form-group">
                    <label for="username">{{ t._('Username Or Email') }}</label> <span class="required">*</span>
                    {{ text_field('certificate', 'class' : 'form-control', 'placeholder' : 'Enter username or email') }}
                </div>
                <div class="form-group">
                    <label for="pwd">{{ t._('Password') }}</label> <span class="required">*</span>
                    {{ password_field('pwd', 'class' : 'form-control', 'placeholder' : 'Enter password') }}
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-sm-2"></div>
            <div class="col-sm-4">
                <a class="btn btn-primary btn-login">{{ t._('Login') }}</a>
            </div>
            <div class="col-sm-4 text-right">
                <a class="btn" style="padding: 8px 0" href="{{ url('member/register') }}">Register</a>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="col-sm-1"></div>
    <div class="col-sm-5 text-center" style="margin-top: 15px; margin-bottom: 15px;">
        <button class="btn btn-warning" type="submit" onclick="validate_shipping()">{{ t._('Ship to this address') }}</button>
    </div>
    <div class="col-sm-6"></div>
    {% endif %}

    {{ end_form()}}
</div>

<style type="text/css">
    .cart-data .row {
        padding-top: 10px;
        padding-bottom: 10px;
    }

    i.note {
        font-size: 12px;
    }

    .border-left {
        border-left: 1px solid #AAA;
    }

    .border-right {
        border-right: 1px solid #AAA;
    }
    form .col-sm-4:first-child {
        position: relative;
        top: 10px;
    }
    form .member-login .col-sm-4:first-child {
        top: 0;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-login').click(function() {
            $.ajax({
                url : '<?php echo $this->url->get('auth/login') ?>',
                type: 'POST',
                data: { certificate : $('#certificate').val(), 'pwd' : $('#pwd').val() },
                dataType: 'json',
                success: function(d) {
                    if(d.success) {
                        window.location.reload();
                    } else {
                        $('.msg-content').html(d.msg);
                    }
                }
            })
        });

        // process select box country change
        $('#country').change(function(e) {
            var country_selected = $(this).val();
            if($('#city option[data-parent=' + country_selected + ']').length == 0) {
                $('#city').addClass('hidden');
                $('#province').removeClass('hidden');
            } else {
                $('#city').removeClass('hidden');
                $('#province').addClass('hidden');
                $('#city option').each(function() {
                    if($(this).data('parent') == $(obj).val()) {
                        $(this).removeClass('hidden');
                    } else {
                        $(this).addClass('hidden');
                    }
                })
            }
        });

        // Search
        $('input.keyword').keyup(function(e){
            if(e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword='+ $(this).val();
            }
        }); // -------------
    });

    function validate_shipping() {
        $('#ship_new_address').val(1);
    }

    function removeValidateForm() {
        $('#ship_new_address').val(0);
        $('form input, select').prop('required', false);
    }
</script>