{% set enableAction = items|length ? '' : 'disabled' %}

<div class="row cart-data">
    <div class="col-sm-8 view">
        <h2>List items in cart</h2>

        <div class="msg-notify">
            {{ flashSession.output() }}
        </div>

        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Price/item</th>
                <th>Quantity</th>
                <th>Total Amount</th>
                <td>#</td>
            </tr>
            <?php
            $total_price = 0;
            if (count($items)) {
                foreach ($items as $k => $v) {
                    ?>
                    <tr class="row-{{ v['id'] }}">
                        <td>{{ k + 1 }}</td>
                        <td>{{ v['product_title'] }}</td>
                        <td>{{ v['price'] }}</td>
                        <td>
                            {{ text_field('quantity_' ~ v['id'], 'value' : v['amount'], 'class' : 'form-control
                            quantity') }}
                        </td>
                        <td>{{ v['price'] * v['amount'] }} $</td>
                        <td><a href="javascript:;" class="delete" data-id="{{ v['id'] }}"
                               data-msg="{{ t._('Are you sure want delete this item in your cart?') }}"></a>
                        </td>
                    </tr>
                    <?php
                    $total_price += $v['price'] * $v['amount'];
                }
            } else {
                ?>
                <tr>
                    <td colspan="6"><i>{{ t._('Your cart is empty') }}</i></td>
                </tr>
            <?php
            }
            ?>
        </table>

        <div class="text-right">
            <button class="btn btn-default btn-update {{ enableAction }}">{{ t._('Update cart') }}</button>
        </div>
    </div>

    <div class="col-sm-4 sum">
        <h2>Summary</h2>

        <hr style="border: 1px solid #DDD; clear: both;"/>

        <div class="form-group">
            <label class="col-sm-6">Total items:</label>

            <div class="col-sm-6 total-item">{{ items |length }}</div>
        </div>

        <div class="clearfix"></div>

        <div class="form-group">
            <label class="col-sm-6">Total price:</label>

            <div class="col-sm-6 total-price">{{ total_price }} $</div>
        </div>

        <div class="clearfix"></div>

        <!--<div class="form-group">
            <label class="col-sm-6">Total ship:</label>

            <div class="col-sm-6">{{ t._('Calculating soon ...') }}</div>
        </div>-->

        <hr style="border: 1px solid #DDD; clear: both;"/>

        <div class="row">
            <div class="col-sm-12">
                <div class="coupon-code">
                    <h4>Promo code</h4>

                    <p>You have received promo code. Enter here:</p>
                    <div class="input-group">
                        {{ text_field('promotion_code', 'class' : 'form-control', 'placeholder' : 'Promo code here ...') }}
                        <span class="input-group-addon">{{ t._('Find') }}</span>
                    </div>
                    <div class="coupon-amount">
                    </div>
                </div>
            </div>
        </div>

        <div class="text-right">
            <a class="btn btn-primary btn-order  {{ enableAction }}" href="{{ url('cart/shipping') }}">{{t._('Continue')}}</a>
        </div>

    </div>
</div>

<button type="button" class="btn btn-primary btn-lg hidden open-confirm-modal" data-toggle="modal"
        data-target="#confirmModal"></button>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ t._('Warning') }}</h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-warning btn-yes" data-id="">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<style type="text/css">
    .form-group {
        overflow: auto;
    }

    .cart-data {
        padding: 20px 0;
    }

    .view table input {
        width: 50px;
    }

    .delete {
        background: url('/assets/images/delete.png') no-repeat;
        padding: 0 8px;
    }

    .coupon-amount { margin-top: 10px; }
    .coupon-amount.loading {
        background: url('/assets/images/loading.gif') no-repeat center;
        height: 30px;
        width: 100%;
    }

    .coupon-code {
        border: 1px solid #ececec;
        background: #fafafa;
        padding: 20px;
        margin-bottom: 20px;
    }
    .input-group-addon { cursor: pointer; }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        var quantity_changed = false,
            btn_option = 'delete';

        $('.quantity').keyup(function (e) {
            quantity_changed = true;
            var obj = $(this).parent().parent();

            $(obj).find('td:nth-child(5)').html(
                parseFloat($(obj).find('td:nth-child(3)').html()) * $(this).val() + ' $'
            );

            cart_utils.update_total();
        });

        // Delete
        $('a.delete').click(function () {
            btn_option = 'delete';

            $('.modal-body p').html($(this).data('msg'));

            $('.open-confirm-modal').click();
            $('.btn-yes').data('id', $(this).data('id'));
        });

        $('.btn-yes').click(function () {
            switch (btn_option) {
                case 'update':
                    quantity_changed = false;
                    // Call update items cart
                    // ...

                    // Recall click order action after update cart success
                    window.location = $('.btn-order').prop('href');
                    break;

                case 'delete':
                    /**
                     * Success
                     */
                    var obj = $(this);
                    $.ajax({
                        url: '/cart/removeItem/' + $(this).data('id'),
                        dataType: 'json',
                        success: function (d) {

                            $('.msg-notify').html(d.msg);

                            if (d.success) {
                                // 1. remove this row
                                $('.row-' + $(obj).data('id')).remove();
                                // 2. Update total items
                                $('.total-item').html(parseInt($('.total-item').html()) - 1);
                                // 3. Update total price
                                cart_utils.update_total();
                            }
                        }, error: function (err) {

                        }
                    })

                    break;
                default :
                    break;
            }

            // Close dialog
            $('button.close').click();

        });

        //-------------
        $('#promotion_code').keyup(function (e) {
            if (e.keyCode == 13) {
                loadCoupon($(this));
            }
        }).blur(function(e) {
                loadCoupon($(this));
            });
        $('.input-group-addon').click(function(){
            loadCoupon($('#promotion_code'));
        });
        //---------------

        // Update action
        $('.btn-update').click(function () {

            quantity_changed = false;
            // Do update action ...
            var data = {};
            $('.quantity').each(function () {
                data[$(this).prop('id')] = $(this).val()
            });
            //console.log(data);
            $.ajax({
                url: '/cart/updateCart/?_=' + Math.random(),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (d) {
                    var msg = [];
                    if (d.data_fail) {
                        for (var i in d.data_fail) {
                            console.log(d.data_fail[i]);

                            $('#quantity_' + d.data_fail[i]['id']).val(d.data_fail[i]['quantity']);

                            msg.push(' - ' + d.data_fail[i]['msg']);
                        }
                        $('.msg-notify').html(
                            '<div class="alert alert-warning">' +
                            '<a data-dismiss="alert" class="close" href="#">×</a>' +
                            msg.join('<br/>') +
                            '</div>'
                        );
                    } else
                        $('.msg-notify').html(d.msg);

                }, error: function (err) {

                }
            });
        });

        // Check before go to order page
        $('.btn-order').click(function (e) {
            if (quantity_changed) {
                btn_option = 'update';
                $('.modal-body p').html('<?php echo $t->_('Do you want update cart before order?') ?>');
                e.preventDefault();

                $('.open-confirm-modal').click();
            }
        });

        // Search
        $('input.keyword').keyup(function (e) {
            if (e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword=' + $(this).val();
            }
        }); // -------------
    });

    var cart_utils = {
        'update_total': function () {
            var total_price = 0;
            $('.quantity').each(function () {
                total_price += parseFloat($(this).parent().parent().find('td:nth-child(3)').html()) * $(this).val();
            });
            $('.total-price').html(total_price + ' $');
        }
    }

    function checkCoupon(obj) {

        $.ajax({
            url: '<?php echo $this->url->get('cart/index?option=setCoupon&data=') ?>' + $(obj).prop('checked'),
            data: {code: $('#promotion_code').val()},
            dataType: 'json',
            success: function (d) {

            }, error: function (err) {

            }
        });
    }

    function loadCoupon(obj){
        $('.coupon-amount').addClass('loading').html('');
        $.ajax({
            url: '<?php echo $this->url->get('cart/index?option=getCoupon') ?>' + '&code=' + $(obj).val() + '&_=' + Math.random(),
            dataType: 'json',
            success: function (d) {
                if (d.success) {
                    $('.coupon-amount').html('You will received <b class="text-success" style="font-size: 16px;">' + d.amount + d.unit
                        + '</b> for this order. Do you want use this code? <input type="checkbox" id="check_code" onclick="checkCoupon(this)"/>');

                } else {
                    $('.coupon-amount').html('<div class="alert alert-danger">' +
                        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                        d.msg +
                        '</div>');
                }
                $('.coupon-amount').removeClass('loading');
            }, error: function (err) {
                $('.coupon-amount').html('<div class="alert alert-danger">' +
                    '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                    err +
                    '</div>');
            }
        });
    }

</script>