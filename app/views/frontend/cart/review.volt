<div class="row cart-data">
    <h2>Review</h2>

    {{ form(url('cart/review'), 'method' : 'post', 'id' : 'frm_order') }}

    <input name="cmd" value="_cart" type="hidden">
    <input name="upload" value="1" type="hidden">
    <input name="no_note" value="0" type="hidden">
    <input name="bn" value="PP-BuyNowBF" type="hidden">
    <input name="tax" value="0" type="hidden">
    <input name="rm" value="2" type="hidden">

    <input name="business" value="{{ config['paypal_account'] }}" type="hidden">
    <input name="handling_cart" value="0" type="hidden">
    <input name="currency_code" value="USD" type="hidden">
    <input name="lc" value="US" type="hidden">
    <input name="cbt" value="Return to My Site" type="hidden">
    <input name="cancel_return" value="{{ baseUri }}cart/paymentCancel" type="hidden">
    <input name="return" value="{{ baseUri }}cart/success" type="hidden">
    <input name="custom" value="" type="hidden">

    <div class="hidden">
        {% for key, item in carts %}
        <div id="item_{{ key + 1 }}" class="itemwrap">
            <input id="item_name_{{ key + 1 }}" name="item_name_{{ key + 1 }}" value="{{ item.getProductTitle() }}" type="hidden">
            <input name="quantity_{{ key + 1 }}" value="{{ item.getAmount() }}" type="hidden">
            <input id="amount_{{ key + 1 }}" name="amount_{{ key + 1 }}" value="{{ item.getPrice() }}" type="hidden">
            <input name="shipping_{{ key + 1 }}" value="{{ fee / carts|length }}" type="hidden">
            <input type="hidden" name="baseamt_{{ key + 1 }}" value="{{ item.getPrice() }}"/>
            <input type="hidden" name="basedes_{{ key + 1 }}" value="{{ item.getProductTitle() }}"/>
        </div>
        {% endFor %}
    </div>

    <div class="col-sm-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th style="width: 100px;">{{ t._('Illustration') }}</th>
                <th>{{ t._('Title') }}</th>
                <th style="width: 110px">{{ t._('Price') }}</th>
                <th>{{ t._('Quality') }}</th>
            </tr>
            </thead>
            <tbody>
            {% set total_quality = 0 %}
            {% set total_price = 0 %}
            {% if carts|length > 0 %}
            {% for key, item in carts %}

            <tr>
                <td>{{ key + 1 }}</td>
                <td><img src="{{ stringHelper.product_img(url(), item.getIllustration(), item.getCreatedTime()) }}"
                         alt="{{ item.getProductTitle() }}"/></td>
                <td>{{ item.getProductTitle() }}</td>
                <td>{{ item.getPrice() }} <sup>USD</sup></td>
                <td>{{ item.getAmount() }} <sup>Unit</sup></td>
            </tr>

            {% set total_quality += item.getAmount() %}
            {% set total_price += item.getPrice() * item.getAmount() %}
            {% endFor %}
            {% endif %}


            <?php
            $order_data = $this->session->get('order');
            $order_data['amount'] = $total_price;
            $order_data['total_items'] = $total_quality;

            // Set order data to session
            $this->session->set('order', $order_data);

            // Calculate coupon value
            $coupon_value = 0;
            if ($coupon->getAmount() > 0) {
                if ($coupon->getUnit() == CouponExt::UNIT_MONEY) {
                    $coupon_value = $coupon->getAmount();
                } else { // Percent
                    $coupon_value = floor($coupon->getAmount() * $total_price / 100);
                }
            }
            ?>

            <tr class="sum">
                <th colspan="3">Total</th>
                <td><b>{{ total_price }}</b> <sup>USD</sup></td>
                <td><b>{{ total_quality}}</b> <sup>Unit</sup></td>
            </tr>
            <tr class="sum">
                <th colspan="3">Ship fee</th>
                <td colspan="2"><b>{{ fee }}</b> <sup>USD</sup></td>
            </tr>
            {% if coupon_value> 0 %}
            <tr class="sum">
                <th colspan="3">Discount</th>
                <td colspan="2"><strong class="text-success" style="font-size: 20px;">{{ coupon_value }}</strong> <sup>USD</sup></td>
            </tr>
            {% endif %}
            <tr class="sum">
                <th colspan="3">Payment</th>
                <td colspan="2"><b>{{ total_price + fee - coupon_value }}</b> <sup>USD</sup></td>
            </tr>
            <tr class="sum">
                <th colspan="3">Time transfer</th>
                <td colspan="2"><b>{{ time_trans }}</b> days</td>
            </tr>

            </tbody>
        </table>
    </div>
    <div class="col-sm-6 review-shipping">
        <h4>Shipping address</h4>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Full name') }}:</div>
            <div class="col-sm-8">{{ data['fullname'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Email') }}:</div>
            <div class="col-sm-8">{{ data['email'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Phone/Mobile') }}:</div>
            <div class="col-sm-8">{{ data['phone'] ~ ' / ' ~ data['mobile'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Address') }}:</div>
            <div class="col-sm-8">{{ data['address'] }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Address 2') }}:</div>
            <div class="col-sm-8">{{ data['address_2'] }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('City') }}:</div>
            <div class="col-sm-8">{{ data['district'] }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('ZipCode') }}:</div>
            <div class="col-sm-8">{{ data['zip_code'] }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Province/State') }}:</div>
            <div class="col-sm-8">{{ data['province'] }}</div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-right">{{ t._('Country') }}:</div>
            <div class="col-sm-8">{{ data['country'] }}</div>
        </div>

        {% if coupon_value > 0  %}
        <div class="row hidden">
            <div class="col-sm-12">
                <div class="discount-data">
                    <h4>Discount value</h4>
                    <p class="text-success"><b style="font-size: 20px;">{{ coupon_value }}</b> <sup>USD</sup></p>
                </div>
            </div>
        </div>
        {% endif %}

        <div class="row" style="padding-top: 40px;">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
                <!--<button type="submit" class="btn btn-primary">{{ t._('Payment') }}</button>-->
                <a class="btn btn-primary btn-submit" href="javascript:;">{{ t._('Payment') }}</a>
            </div>
        </div>
    </div>

    {{ end_form() }}
</div>

<style type="text/css">
    .cart-data h2 {
        padding-left: 15px;
    }

    table img {
        width: 100%;
    }

    tr.sum td, tr.sum th {
        background: #f9f9f9;
    }

    tr.sum b {
        font-size: 20px;
        color: #ff6600;
    }

    .review-shipping .row {
        padding: 5px;
    }

    .discount-data {
        border: 1px solid #ececec;
        background: #fafafa;
        padding: 20px;
    }
</style>

<script type="text/javascript">

    $(document).ready(function () {
        // Search
        $('input.keyword').keyup(function (e) {
            if (e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword=' + $(this).val();
            }
        }); // -------------
        $('a.btn-submit').click(function (e) {
            validateData();
            e.preventDefault();
        });
    });

    function validateData() {
        var paypal_url = 'https://www.paypal.com/cgi-bin/webscr';
        $('body').append('<div class="loading"></div>');

        $.ajax({
            url: '<?php echo $this->url->get('cart/inActiveCoupon') ?>' + '?_=' + Math.random(),
            type: 'POST',
            dataType: 'json',
            success: function (d) {
                if (d.valid) {
                    paypal_discount.readForm();
                    $('#frm_order').prop('action', paypal_url).submit();
                } else {
                    window.location.href = "<?php echo $this->url->get('cart/index') ?>";
                    console.log(d);
                }
            }, error: function (err) {
                window.location.href = "<?php echo $this->url->get('cart/index') ?>";
                console.log(err);
            }
        });
    }

    var paypal_discount = {
        'readForm': function () {
            var coupon_val = parseFloat(
            <?php echo $coupon_value ?> / $('input[name^=baseamt_').length
            );
            var amt, des;
            if (parseFloat(coupon_val) > 0)
                $('input[name^=baseamt_').each(function (index) {
                    amt = parseFloat($(this).val());       // base amount
                    des = $('#item_name_' + (index + 1)).val();          // base description
                    amt = amt - coupon_val;
                    des = des + ", COUPON = " + coupon_val + ' USD';
                    $('#amount_' + (index + 1)).val(amt);
                    $('#item_name_' + (index + 1)).val(des);
                });
        }
    }
</script>