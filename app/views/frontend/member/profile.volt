<div class="row profile-wrapper">
    <div class="col-sm-2 menu">
        <ul>
            <li>
                <img title="{{ session.get('auth')['member']['username'] }}" src="{{ avatar }}" class="avatar"
                     alt="{{ session.get('auth')['member']['avatar'] ? session.get('auth')['member']['avatar'] :'no-avatar' }}"/>
            </li>
            <li>
                <a href="{{ url('member/profile') }}" class="icon-profile">Profile</a>
            </li>
            <li>
                <a href="{{ url('member/order') }}" class="icon-order">Oder</a>
            </li>
            <li>
                <a href="{{ url('member/transaction') }}" class="icon-trans">Transaction</a>
            </li>
        </ul>
    </div>
    <div class="col-sm-10">
        <form name="profile" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-10 profile_data">
                    {{ flashSession.output() }}
                    <h2>Your Profile</h2>

                    <div class="row">
                        <div class="col-sm-9">
                            <!--<div class="form-group">
                                <label for="fullname">{{ t._('Full name') }}:</label>
                                {{ text_field('fullname', 'placeholder':'Enter your full name',
                                'class':'form-control',
                                'value':req.getPost('fullname') ? req.getPost('fullname') :
                                session.get('auth')['member']['fullname'])
                                }}
                            </div>-->

                            <div class="form-group">
                                <label for="fullname">{{ t._('Fist name') }}:</label>
                                {{ text_field('fist_name', 'placeholder':'Enter your first name',
                                    'class':'form-control', 'value':req.getPost('first_name') ? req.getPost('first_name') :
                                    session.get('auth')['member']['first_name'])
                                }}
                            </div>

                            <div class="form-group">
                                <label for="fullname">{{ t._('Last name') }}:</label>
                                {{ text_field('last_name', 'placeholder':'Enter your last name',
                                'class':'form-control',
                                'value':req.getPost('last_name') ? req.getPost('last_name') :
                                session.get('auth')['member']['last_name'])
                                }}
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>{{ t._('Username') }}:</label>

                                        <div>{{ session.get('auth')['member']['username'] }}</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>{{ t._('Email') }}:</label>

                                        <div>{{ session.get('auth')['member']['email'] }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pwd">{{ t._('Password') }}:</label>
                                {{ password_field('pwd', 'placeholder':'Enter your password',
                                'class':'form-control',
                                'value' : '') }}
                            </div>

                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="avatar">Avatar</label>
                                <input type="file" id="avatar" name="avatar">
                                <img src="{{ avatar }}" alt="{{ session.get('auth')['member']['avatar'] ? session.get('auth')['member']['avatar'] :'no-avatar' }}"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>{{ t._('Telephone') }}:</label>

                        <div class="row">
                            <div class="col-sm-9">
                                {{ text_field('phone', 'placeholder':'Enter your phone number',
                                'class':'form-control',
                                'value':req.getPost('phone') ? req.getPost('phone') :
                                session.get('auth')['member']['phone']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>{{ t._('Mobile') }}:</label>

                        <div class="row">
                            <div class="col-sm-9">
                                {{ text_field('mobile', 'placeholder':'Enter your mobile number',
                                'class':'form-control',
                                'value':req.getPost('mobile') ? req.getPost('mobile') :
                                session.get('auth')['member']['mobile']) }}
                            </div>
                        </div>
                    </div>

                    <!--<h3>{{ t._('Your address, this will be used to shipping') }}</h3>-->
                    <div class="form-group">
                        <label>Country</label>
                        {{ select('country', countries, 'using' : ['id', 'name'], 'class':'form-control',
                        'useEmpty': true, 'emptyText': t._('- Select country'),
                        'value' : req.getPost('country') ? req.getPost('country') :
                        session.get('auth')['member']['country']) }}
                    </div>

                    <div class="form-group">
                        <label for="address">{{ t._('Address 1') }}:</label>
                        {{ text_field('address', 'placeholder':'Enter your address', 'class':'form-control',
                        'value':req.getPost('address') ? req.getPost('address') :
                        session.get('auth')['member']['address']) }}
                    </div>

                    <div class="form-group">
                        <label for="address_2">{{ t._('Address 2') }}:</label> <i style="color: #999">Option</i>
                        {{ text_field('address_2', 'placeholder':'Enter your address 2', 'class':'form-control',
                        'value':req.getPost('address_2') ? req.getPost('address_2') :
                        session.get('auth')['member']['address_2']) }}
                    </div>

                    <div class="form-group">
                        <label>{{ t._('City') }}</label>
                        {{ text_field('district', 'placeholder':'Enter your district',
                        'class':'form-control',
                        'value':req.getPost('district') ? req.getPost('district') :
                        session.get('auth')['member']['district']) }}
                    </div>

                    <div class="form-group">
                        <label>{{ t._('State') }}</label>
                        <select class="form-control" name="city" id="city">
                            <option value="0"> - {{ t._('Select state') }}</option>
                            {% if cities | length %}
                            {% for c in cities %}
                            {% set selected = session.get('auth')['member']['province'] == c.getId() ? 'selected' : '' %}
                            <option class="{{ c.getParentId() == country ? '' : 'hidden' }}" {{ selected }}
                                    value="{{ c.getId() }}" data-parent="{{ c.getParentId() }}">{{ c.getName() }}</option>
                            {% endFor %}
                            {% endif %}
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="zipcode">{{ t._('Zipcode') }}:</label>
                        {{ text_field('zipcode', 'placeholder':'Enter your zipcode', 'class':'form-control',
                        'value':req.getPost('zipcode') ? req.getPost('zipcode') : session.get('auth')['member']['zipcode']) }}
                    </div>

                    <div class="form-group hidden">
                        <label>{{ t._('District') }} / {{ t._('Province') }} / {{ t._('Country') }}:</label>

                        <div class="row">
                            <div class="col-sm-4">
                                {{ text_field('district', 'placeholder':'Enter your district',
                                'class':'form-control',
                                'value':req.getPost('district') ? req.getPost('district') :
                                session.get('auth')['member']['district'])
                                }}
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="city" id="city">
                                    <option value="0"> - {{ t._('Select state') }}</option>
                                    {% if cities | length %}
                                    {% for c in cities %}
                                    {% set selected = session.get('auth')['member']['province'] == c.getId() ? 'selected' : '' %}
                                    <option class="{{ c.getParentId() == country ? '' : 'hidden' }}" {{ selected }}
                                            value="{{ c.getId() }}" data-parent="{{ c.getParentId() }}">{{ c.getName() }}</option>
                                    {% endFor %}
                                    {% endif %}
                                </select>
                                {{ text_field('province', 'placeholder':'Enter province if not available',
                                'class':'form-control', 'value':req.getPost('province') ? req.getPost('province') :
                                (session.get('auth')['member']['province'] ? '' : session.get('auth')['member']['province_name']))
                                }}
                            </div>
                            <div class="col-sm-4">
                                {{ select('country', countries, 'using' : ['id', 'name'], 'class':'form-control',
                                'useEmpty': true, 'emptyText': t._('- Select country'),
                                'value' : req.getPost('country') ? req.getPost('country') :
                                session.get('auth')['member']['country']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">{{ t._('Update') }}</button>
                        <button type="reset" class="btn btn-default">{{ t._('Reset') }}</button>
                    </div>

                </div>

            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        var current_city_choose = $('#city').val();
        $('#country').change(function(e) {
            var obj = $(this);
            $('#city option').each(function() {
                if($(this).data('parent') == $(obj).val()) {
                    $(this).removeClass('hidden');
                } else {
                    $(this).addClass('hidden');
                }
            });
        });
        $('#city').change(function() {
            $('#province').val('');
            current_city_choose = $(this).val();
        });
        $('#province').keyup(function() {
            if($(this).val().replace(/\s/g, '').length > 0) {
                $('#city').val(0);
            } else {
                $('#city').val(current_city_choose);
            }
        });

        // Search
        $('input.keyword').keyup(function(e){
            if(e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword='+ $(this).val();
            }
        }); // -------------
    });
</script>