<div class="row profile-wrapper">
    <div class="col-sm-2 menu">
        <ul>
            <li>
                <img title="{{ session.get('auth')['member']['username'] }}" src="{{ avatar }}" class="avatar"
                     alt="{{ session.get('auth')['member']['avatar'] ? session.get('auth')['member']['avatar'] :'no-avatar' }}"/>
            </li>
            <li>
                <a href="{{ url('member/profile') }}" class="icon-profile">Profile</a>
            </li>
            <li>
                <a href="{{ url('member/order') }}" class="icon-order">Oder</a>
            </li>
            <li>
                <a href="{{ url('member/transaction') }}" class="icon-trans">Transaction</a>
            </li>
        </ul>
    </div>

    <div class="col-sm-10">
        <div class="row">
            <div class="col-sm-12 profile_data">
                <a href="{{ url('member/order') }}" class="pull-right">{{ t._('Back to list order') }}</a>
                <h3>{{ t._('Order information') }}</h3>
                <label class="col-sm-2 text-right">
                    {{ t._('Order code') }}:
                </label>
                <div class="col-sm-4">
                    {{ order.getCode() }}
                </div>
                <div class="clearfix"></div>
                <label class="col-sm-2 text-right">{{ t._('Name') }}:</label>
                <div class="col-sm-4">
                    {{ order.getCustomerName() }}
                </div>
                <label class="col-sm-2 text-right">{{ t._('Email') }}:</label>
                <div class="col-sm-4">
                    {{ order.getCustomerEmail() }}
                    </div>
                    <div class="clearfix"></div>
                    <label class="col-sm-2 text-right">{{ t._('Mobile') }}:</label>
                    <div class="col-sm-4">
                        {{ order.getCustomerMobile() }}
                    </div>
                    <label class="col-sm-2 text-right">{{ t._('Phone') }}:</label>
                    <div class="col-sm-4">
                        {{ order.getCustomerPhone() }}
                    </div>
                <div class="clearfix"></div>
                <label class="col-sm-2 text-right">{{ t._('Created time') }}:</label>
                <div class="col-sm-4">
                    {{ order.getCreatedAt() }}
                </div>

                <!-- Shipping address -->
                <div class="clearfix"></div>
                <h3>{{ t._('Shipping address') }}</h3>
                <label class="col-sm-2 text-right">{{ t._('Address') }}:</label>
                <div class="col-sm-10">
                    {{ order.getCustomerAddress() }}
                </div>
                <div class="clearfix"></div>
                <label class="col-sm-2 text-right">{{ t._('Address2') }}:</label>
                <div class="col-sm-10">
                    {{ order.getCustomerAddress2() }}
                </div>
                <div class="clearfix"></div>
                <label class="col-sm-2 text-right">{{ t._('District') }}:</label>
                <div class="col-sm-2">
                    {{ order.getDistrictLabel() }}
                </div>
                <div class="clearfix"></div>
                <label class="col-sm-2 text-right">{{ t._('Province') }}:</label>
                <div class="col-sm-2">
                    {{ order.getProvinceLabel() }}
                </div>
                <div class="clearfix"></div>
                <label class="col-sm-2 text-right">{{ t._('Country') }}:</label>
                <div class="col-sm-2">
                    {{ order.getCountryLabel() }}
                </div>

                <div class="clearfix"></div>
                <h3>{{ t._('List item in order') }}</h3>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ t._('Title') }}</th>
                                <th>{{ t._('Price') }} (USD)</th>
                                <th>{{ t._('Amount') }}</th>
                                <th>{{ t._('Illustration') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for index, m in orderItems %}
                            <tr>
                                <td>{{ index + 1 }}</td>
                                <td><a href="{{ url('product/detail/') ~ slug.generate(m.getProductTitle()) ~ '-' ~ m.getProductId() }}" target="_blank">{{ m.getProductTitle() }}</a></td>
                                <td>{{ m.getPrice()}}</td>
                                <td>{{ m.getAmount() }}</td>
                                <td><img src="{{ url()~ 'uploads/products/' ~ m.getIllustration() }}"/></td>
                            </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .table-responsive img { width: 150px; }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        // Search
        $('input.keyword').keyup(function (e) {
            if (e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword=' + $(this).val();
            }
        }); // -------------
    });
</script>