<div class="auth-form">
    <form name="register" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-12">
                {{ flashSession.output() }}
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-8 register_data">
                <h2>Register information</h2>

                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 text-right" for="first_name">{{ t._('First name') }}:</label>
                                <div class="col-sm-9">
                                    {{ text_field('first_name', 'placeholder':'Enter your first name', 'class':'form-control',
                                    'value':req.getPost('first_name') ? req.getPost('first_name') : customer.getFistName()) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 text-right" for="fullname">{{ t._('Last name') }}:</label>
                                <div class="col-sm-9">
                                    {{ text_field('last_name', 'placeholder':'Enter your last name', 'class':'form-control',
                                    'value':req.getPost('last_name') ? req.getPost('last_name') : customer.getLastName()) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 text-right" for="username">{{ t._('Username') }}: <span class="text-danger">*</span> </label>
                                <div class="col-sm-9">
                                    {{ text_field('username', 'placeholder':'Enter your login name', 'class':'form-control',
                                    'value':req.getPost('username') ? req.getPost('username') : customer.getUsername()) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 text-right" for="pwd">{{ t._('Password') }}: <span class="text-danger">*</span> </label>
                                <div class="col-sm-9">
                                    {{ password_field('pwd', 'placeholder':'Enter your password', 'class':'form-control',
                                    'value' : '') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 text-right" for="email">{{ t._('Email') }}: <span class="text-danger">*</span> </label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" placeholder="Ex: example@hostmail.com" class="form-control"
                                           value="{{ req.getPost('email') ? req.getPost('email') : customer.getEmail() }}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="avatar">Avatar</label>
                            <input type="file" id="avatar" name="avatar">
                            <img src="{{ url('uploads/avatar/no_avatar.jpg')}}" alt="No Avatar"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <label class="col-sm-2 text-right">{{ t._('Telephone') }} :</label>
                        <div class="col-sm-6">
                            {{ text_field('phone', 'placeholder':'Enter your phone number', 'class':'form-control',
                            'value':req.getPost('phone') ? req.getPost('phone') : customer.getPhone()) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">

                    <div class="row">
                        <label class="col-sm-2 text-right">{{ t._('Mobile') }} :</label>
                        <div class="col-sm-6">
                            {{ text_field('mobile', 'placeholder':'Enter your mobile number', 'class':'form-control',
                            'value':req.getPost('mobile') ? req.getPost('mobile') : customer.getMobile()) }}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 text-right">Country</label>
                        <div class="col-sm-6">
                            {{ select('country', countries, 'using' : ['id', 'name'], 'class':'form-control',
                            'useEmpty': true, 'emptyText': t._('- Select country'),
                            'value' : req.getPost('country') ? req.getPost('country') : (customer.getCountry() ?
                            customer.getCountry() : country) ) }}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 text-right" for="address">{{ t._('Address 1') }}:</label>
                        <div class="col-sm-6">
                            {{ text_field('address', 'placeholder':'Enter your address', 'class':'form-control',
                            'value':req.getPost('address') ? req.getPost('address') : customer.getAddress()) }}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 text-right" for="address_2">{{ t._('Address 2') }}:</label>
                        <div class="col-sm-6">
                            {{ text_field('address_2', 'placeholder':'Enter your address 2', 'class':'form-control',
                            'value':req.getPost('address_2') ? req.getPost('address_2') : customer.getAddress2()) }}
                        </div>
                        <i class="col-sm-2" style="color: #999">Option</i>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 text-right">{{ t._('City') }}</label>

                        <div class="col-sm-6">
                            {{ text_field('district', 'placeholder':'Enter your district', 'class':'form-control',
                            'value':req.getPost('district') ? req.getPost('district') : customer.getDistrict()) }}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 text-right">{{ t._('State') }}</label>

                        <div class="col-sm-6">
                            <select class="form-control" name="city" id="city">
                                <option value="0"> - {{ t._('Select state') }}</option>
                                {% if cities | length %}
                                {% for c in cities %}
                                <option class="{{ c.getParentId() == country ? '' : 'hidden' }}" value="{{ c.getId() }}"
                                        data-parent="{{ c.getParentId() }}">{{ c.getName() }}
                                </option>
                                {% endFor %}
                                {% endif %}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 text-right" for="zipcode">{{ t._('Zipcode') }}:</label>
                        <div class="col-sm-4">
                            {{ text_field('zipcode', 'placeholder':'Enter your zipcode', 'class':'form-control',
                            'value':req.getPost('zipcode') ? req.getPost('zipcode') : customer.getZipcode()) }}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-2">

                        </div>
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">{{ t._('Register') }}</button>
                            <button type="reset" class="btn btn-default">{{ t._('Reset') }}</button>
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-sm-2"></div>
        </div>
    </form>
</div>

<style type="text/css">
    h2, .h2 {
        padding: 10px 0;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#country').change(function (e) {
            var obj = $(this);
            $('#city option').each(function () {
                if ($(this).data('parent') == $(obj).val()) {
                    $(this).removeClass('hidden');
                } else {
                    $(this).addClass('hidden');
                }
            });
        });
        $('#city').change(function () {
            $('#province').val('');
        });
        $('#province').keyup(function () {
            if ($(this).val().replace(/\s/g, '').length > 0) {
                $('#city').val(0);
            } else {
                $('#city').val(current_city_choose);
            }
        });

        // Search
        $('input.keyword').keyup(function (e) {
            if (e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword=' + $(this).val();
            }
        }); // -------------
    });
</script>