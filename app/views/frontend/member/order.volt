<div class="row profile-wrapper">
    <div class="col-sm-2 menu">
        <ul>
            <li>
                <img title="{{ session.get('auth')['member']['username'] }}" src="{{ avatar }}" class="avatar"
                     alt="{{ session.get('auth')['member']['avatar'] ? session.get('auth')['member']['avatar'] :'no-avatar' }}"/>
            </li>
            <li>
                <a href="{{ url('member/profile') }}" class="icon-profile">Profile</a>
            </li>
            <li>
                <a href="{{ url('member/order') }}" class="icon-order">Oder</a>
            </li>
            <li>
                <a href="{{ url('member/transaction') }}" class="icon-trans">Transaction</a>
            </li>
        </ul>
    </div>

    <div class="col-sm-10">
        <div class="row">
            <div class="col-sm-12 profile_data">
                <h3>List order</h3>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ t._('Code') }}</th>
                                <th>{{ t._('Date order') }}</th>
                                <th>{{ t._('Total Quality') }}</th>
                                <th>{{ t._('Total Money') }}</th>
                                <th>{{ t._('Total Fee') }}</th>
                                <th>{{ t._('Status') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <!--{% for index, m in orders %}
                            <tr>
                                <td>{{ index + 1 }}</td>
                                <td>{{ m.created_time }}</td>
                                <td>{{ m.total_items }}</td>
                                <td>{{ m.amount}}</td>
                                <td>{{ m.fee_amount}}</td>
                                <td>{{ status[m.status] }}</td>
                                <td>
                                    <a href="{{ url('member/viewOrder/' ~ m.id)}}">{{ t._('Delete') }}</a>
                                </td>
                            </tr>
                            {% endfor %}-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('#dataTables-example').dataTable(
            {
                "oLanguage": {
                    "sLengthMenu": "_MENU_ " + '<?php echo $t->_("rows/page"); ?>',
                    "sInfo": "<?php echo $t->_('Showing') ?> _START_ to _END_ of _TOTAL_ <?php echo $t->_('result') ?>",
                    "sSearch": "<?php echo $t->_('Search') ?>: ",
                    "sPrevious": "<?php echo $t->_('Previous') ?>",
                    "sNext": "<?php echo $t->_('Next') ?>",
                    "sEmptyTable": "<?php echo $t->_('No data available in table') ?>",
                    "sInfoEmpty": "<?php echo $t->_('Showing 0 to 0 of 0 entries') ?>"
                },
                "order": [[ 0, "desc" ]],
                /*"lengthMenu": [ 2, 5, 10, 25, 50, 75, 100 ],
                 'pageLength': 2,*/
                "columnDefs": [
                    {"orderable": false, "targets": [] }
                ],
                "processing": true,
                "serverSide": true,
                'ajax': {
                    'url': '/member/order?_=' + Math.random(),
                    'data': function (d) {
                        d.from = $('#dataTables-example_filter input.from').val();
                        d.to = $('#dataTables-example_filter input.to').val();
                    }
                }
            }
        );

        $('#dataTables-example_filter').html(
            '<input type="text" class="form-control input-sm datepicker from" placeholder="Created from">' +
             '<input type="text" class="form-control input-sm datepicker to" placeholder="Created to">'
        ).addClass('text-right');

        var tmp_val = '';
        // Change default keypress search
        $("#dataTables-example_filter input").unbind().keyup(function (e) {
            if (e.keyCode == 13 & tmp_val != this.value) {
                oTable.fnFilter(this.value);
                tmp_val = this.value;
            }
        });

        $('input.datepicker').datetimepicker({
            format: 'Y-m-d H:i'
        });

        // Search
        $('input.keyword').keyup(function(e){
            if(e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword='+ $(this).val();
            }
        }); // -------------
    });
</script>