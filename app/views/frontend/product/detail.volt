<input type="hidden" id="cid" value="{{ category.getId() }}">
<div class="row sp-categories">
    <div class="col-sm-12">
        <label>{{ t._('Search by Vehicle') }}</label>
    </div>
    <div class="col-sm-2">

        {{ select('brand', brands, 'using' : ['id', 'brand_name'], 'class' : 'form-control', 'useEmpty' : true,
        'emptyText' : t._('- Select brand')) }}

        {{ select('model', [], 'class' : 'form-control', 'useEmpty' : true, 'emptyText' : t._('')) }}
        {{ select('year', [], 'class' : 'form-control', 'useEmpty' : true, 'emptyText' : t._('')) }}

        <button class="btn btn-default form-control sp-search">{{ t._('Search') }}</button>

    </div>
    <div class="col-sm-10">
        <div class="row slick-data" style="width: 95%; margin-left: 1.5%;">
            {% if(pro_cats | length ) %}
            {% for cat in pro_cats %}
            <?php
            $sub_dir = date('Y-m-d', strtotime($cat['created_time']));

            $img = strpos($cat['thumb'], 'http:') > -1 ? $cat['thumb'] : (file_exists($this->config->application->baseDir . 'public' . DS . 'uploads' .
                DS . 'products' . DS . $sub_dir . DS . $cat['thumb']) & !empty($cat['thumb']) ?
                '/uploads/products/' . $sub_dir . '/' . $cat['thumb'] : '/uploads/products/no-image.png');
            ?>
            <div class="col-sm-3 text-center sp-cat-block">
                <div>
                    <a href="/product/{{ cat['slug'] }}">
                        <img src="{{ img }}" alt="{{ cat['name'] }}">
                        <span>{{ cat['name'] }}</span>
                    </a>
                </div>
            </div>
            {% endfor %}
            {% endif %}
        </div>
    </div>
</div>

<div class="row sp-products detail">
    <div class="col-sm-12 list">
        <div class="row">
            <div class="col-sm-6">
                <div class="item-info"></div>
                <img src="{{ illustration }}" alt="{{ product.getTitle() }}" id="illustration"/>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-5 add-to-cart">
                <div class="row">
                    <div class="col-sm-12" style="height:30px;"></div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <b>{{ t._('Product name') }}</b> :
                    </div>
                    <div class="col-sm-9">
                        <h4>{{ product.getTitle() }}</h4>
                        {{ hidden_field('pro_id', 'value': product.getId()) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <b>SKU</b> :
                    </div>
                    <div class="col-sm-9">
                        {{ product.getSku() }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <b>{{ sale_off ? 'Promotion' : 'Price' }}</b> :
                    </div>
                    <div class="col-sm-9 price">
                        {{ (sale_off ? product.getSaleOff() : product.getPrice()) ~ ' ' ~ currency }}
                    </div>
                </div>
                {% if sale_off %}
                <div class="row">
                    <div class="col-sm-3">
                        <b>End date of promotion</b> :
                    </div>
                    <div class="col-sm-9">
                        {{ product.getSaleOffTo() }}
                    </div>
                </div>
                {% endif %}

                <div class="row">
                    <div class="col-sm-3">
                        <b>In stock</b> :
                    </div>
                    <div class="col-sm-9">
                        {{ product.getStock() }} item(s)
                        <input type="hidden" id="in_stock" value="{{ product.getStock() }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <b>Quality</b> :
                    </div>
                    <div class="col-sm-9">
                        {{ text_field('amount', 'class':'form-control', 'value' : 1, 'style':'width: 20%; display:
                        inline-block') }}
                        <!--<button class="btn btn-default">Add Cart</button>-->
                        <span class="add-cart"></span>
                    </div>

                    <div class="col-sm-12 message">

                    </div>

                </div>
                <div class="row list-thumb">
                    <div class="col-sm-12">
                        <b>{{ t._('Thumbnails') }}:</b>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{ illustration }}" class="active">
                    </div>
                    <?php
                    $thumbs = json_decode($product->getThumbs(), true);
                    if(!empty($thumbs)) {
                        foreach($thumbs as $thumb) {
                            $img = $this->url->get() . $this->config->application->uploadUri . 'products/no-image.png';
                            if(file_exists($this->config->application->uploadDir . 'products' . DS . date('Y-m-d', strtotime($product->getCreatedTime())) . DS . 'thumbs' . DS . $thumb)
                                & !empty($thumb)) {
                                $img = $this->url->get() . $this->config->application->uploadUri . 'products/' . date('Y-m-d', strtotime($product->getCreatedTime())) . '/thumbs/' . $thumb;
                                ?>
                                <div class="col-sm-4">
                                    <img src="{{ img }}">
                                </div>
                    <?php
                            }
                        }
                    }
                    ?>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <b>{{ t._('Brief') }}</b> :
                    </div>
                    <div class="col-sm-9">
                        {{ product.getBrief() }}
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 des">
                {{ product.getDes() }}
            </div>
        </div>
        <div class="row other-product">

            <div class="col-sm-12 product-fit-title">

            </div>
            <div class="col-sm-12">
                {% if (other_products | length) %}
                <ul>
                    <?php foreach ($other_products as $pro) { ?>
                        <li>
                            <a href="/product/detail/{{ slug.generate(pro['title']) ~ '-' ~ pro['id'] }}">
                                {{ stringHelper.strip_word(pro['title'], 10) }}
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                {% else %}
                <i>No result</i>
                {% endif %}
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.slick-data').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '0',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '0',
                        slidesToShow: 1
                    }
                }
            ]
        });

        $('span.add-cart').click(function () {

            // Check current amount
            if ($('#amount').val() > $('#in_stock').val()) {
                $('div.message').html('<div class="alert alert-danger">' +
                    '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                    'Max quality is ' + $('#in_stock').val() + ' item(s)' +
                    '</div>');
                $('#amount').val($('#in_stock').val());
                return;
            }

            $('body').append('<div class="loading"></div>');

            $.ajax({
                url: '<?php echo $this->config->application->baseUri . 'cart/addItemToCart' ?>' + '?_=' + Math.random(),
                method: 'POST',
                data: {
                    pid: $('#pro_id').val(),
                    quality: $('#amount').val()
                },
                dataType: 'json',
                success: function (d) {

                    if (d.success & d.type == 'new') {
                        $('sup.total-item').html(parseInt($('sup.total-item').html()) + 1);
                    }

                    $('div.message').html(d.msg);
                    $('div.loading').remove();
                },
                error: function (e) {
                    $('div.message').html('<div class="alert alert-danger">' +
                        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                        e +
                        '</div>');
                    $('div.loading').remove();
                }
            })
        });

        // Search -----------
        $('input.keyword').keyup(function(e){
            if(e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword='+ $(this).val();
            }
        }); // -------------

        $('.list-thumb img').click(function() {
            // Change source
            $('#illustration').prop('src', $(this).prop('src'));
            // Re-active current thumb view
            $('.list-thumb').find('.active').removeClass('active');
            $(this).parent().addClass('active');
        });
    });

</script>

<style type="text/css">
    .list-thumb img { width: 98%; padding: 1%; border: 1px solid #317eac; border-radius: 5px;  }
    .list-thumb img.active, .list-thumb img:hover { border-color: #FF0000; }
    #illustration { max-width: 100%; }
</style>