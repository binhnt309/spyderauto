<input type="hidden" id="cid" value="{{ category.getId() }}">
<div class="row sp-categories">
    <div class="col-sm-12">
        <label>{{ t._('Search by Vehicle') }}</label>
    </div>
    <div class="col-sm-2">

        {{ select('brand', brands, 'using' : ['id', 'brand_name'], 'class' : 'form-control', 'useEmpty' : true,
        'emptyText' : t._('- Select brand')) }}

        {{ select('model', [], 'class' : 'form-control', 'useEmpty' : true, 'emptyText' : t._('')) }}
        {{ select('year', [], 'class' : 'form-control', 'useEmpty' : true, 'emptyText' : t._('')) }}

        <button class="btn btn-default form-control sp-search">{{ t._('Search') }}</button>

    </div>
    <div class="col-sm-10">
        <div class="row slick-data" style="width: 95%; margin-left: 1.5%;">
            {% if(pro_cats | length ) %}
            {% for cat in pro_cats %}
            <?php
            $sub_dir = date('Y-m-d', strtotime($cat['created_time']));

            $img = strpos($cat['thumb'], 'http:') > -1 ? $cat['thumb'] : (file_exists($this->config->application->baseDir . 'public' . DS . 'uploads' .
                DS . 'products' . DS . $sub_dir . DS . $cat['thumb']) & !empty($cat['thumb']) ?
                '/uploads/products/' . $sub_dir . '/' . $cat['thumb'] : '/uploads/products/no-image.png');
            ?>
            <div class="col-sm-3 text-center sp-cat-block">
                <div>
                    <a href="/product/{{ cat['slug'] }}">
                        <img src="{{ img }}" alt="{{ cat['name'] }}">
                        <span>{{ cat['name'] }}</span>
                    </a>
                </div>
            </div>
            {% endfor %}
            {% endif %}
        </div>
    </div>
</div>

<div class="row sp-products">
    <div class="col-sm-3 cate-description">
        <h2>{{ category.getName() }}</h2>

        <div>
            {{ category.getDes() }}
        </div>
    </div>
    <div class="col-sm-9 list">
        {% if (products | length) %}
        {% set inc = 0 %}
        {% set repeat = 0 %}
        {% for pro in products %}
        {% if inc == 3 %}
        {% set inc = 0 %}
        {% endif %}
        {% if inc == 0 %}
        <div class="row">
            {% endif %}
            <div class="col-sm-4 sp-block text-center">
                <?php
                $sub_dir = date('Y-m-d', strtotime($pro->getCreatedTime()));
                $_img = strpos($pro->getIllustration(), 'http:') > -1 ?
                    $pro->getIllustration() : (file_exists($this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'products' .
                    DS . date('Y-m-d', strtotime($pro->getCreatedTime())) . DS . $pro->getIllustration()) & $pro->getIllustration() != '' ?
                    $this->config->application->baseUri . 'uploads/products/' . date('Y-m-d', strtotime($pro->getCreatedTime())) . '/' . $pro->getIllustration() :
                    $this->config->application->baseUri . 'uploads/products/no-image.png');

                ?>
                <a href="/product/detail/{{ slug.generate(pro.getTitle()) ~ '-' ~ pro.getId() }}">
                    <img src="{{ _img }}" align="{{ pro.getTitle() }}"/>
                    {{ stringHelper.strip_word(pro.getTitle(), 10) }}
                </a>

                <?php
                $promotion = false;
                $sale_off = $pro->getSaleOff();

                if (!empty($sale_off) & strtotime($pro->getSaleOffTo()) > time()) {
                    $promotion = true;
                }
                ?>

                <p class="item-price {{ promotion ? 'promotion' : '' }}">
                    {% if !promotion %}
                    <span>{{ t._('Price') }}:</span>
                    <span>{{ pro.getPrice() ~ ' ' ~ currencies[pro.getCurrencyId()]['abbreviation'] }}</span>
                    {% else %}
                    <span>{{ t._('Promotion') }}: </span>
                    <span>{{ pro.getSaleOff() ~ ' ' ~ currencies[pro.getCurrencyId()]['abbreviation'] }}</span>
                    {% endif %}
                </p>
                {% if promotion %}
                <p class="end-sale-off">
                    <span>{{ t._('End sale-off date') }} :</span>
                    <span>{{ pro.getCreatedTime() }}</span>
                </p>
                {% endif %}
            </div>
            <?php
            if ($inc == 2 || ($inc + 1 + ($repeat * 3)) == count($products)) {
            $repeat++;
            ?>
        </div>
        <?php } ?>

        <?php $inc++ ?>

        {% endfor %}
        {% endif %}
    </div>
</div>

<style type="text/css">
    .sp-products {
        margin-top: 15px;
        background: #FFF;
        color: #333;
    }

    .sp-products h2 {
        text-transform: uppercase;
    }

    .cate-description {
        padding: 0;
    }

    .cate-description h2 {
        background: none repeat scroll 0 0 #000;
        color: #fff;
        left: 0;
        padding: 10px 5px;
        position: absolute;
        top: -20px;
        width: 100%;
    }

    .cate-description > div {
        margin-top: 40px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('.slick-data').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '0',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '0',
                        slidesToShow: 1
                    }
                }
            ]
        });

        // Search
        $('input.keyword').keyup(function (e) {
            if (e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword=' + $(this).val();
            }
        }); // -------------
    });
</script>