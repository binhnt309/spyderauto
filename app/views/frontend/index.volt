<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->

    {{ get_title() }}
    <meta name="keyword" content="{{ meta_keyword is defined ? meta_keyword : '' }}"/>
    <meta name="description" content="{{ meta_description is defined ? meta_description : '' }}"/>

    {{ assets.outputCss('frontendCss') }}
    {{ assets.outputJs('libraryJs') }}
</head>
<body>
<div class="header-bg">
    <div class="wrap">
        <div class="h-bg">
            <div class="total">
                {{ partial('partials/header') }}

                {{ partial('partials/menu') }}

                <div class="banner-top">
                    <div class="header-bottom">

                        {{ content() }}

                        <div class="clear"></div>

                        {{ partial('partials/footer') }}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs('frontendJs') }}
</body>

</html>