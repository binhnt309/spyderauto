<div class="row contact-form">
    <div class="col-sm-7" style="margin-top: 20px;">
        {{ config['contact'] }}
    </div>
    <div class="col-sm-5">
        <h3>{{ t._('Contact us') }}</h3>
        {{ msg }}
        {{ form() }}
        <div class="form-group">
            <label>{{ t._('Your name') }}</label> <span class="required">*</span>
            {{ text_field('name', 'class' : 'form-control', 'required' : true, 'placeholder' : 'Enter you name') }}
        </div>
        <div class="form-group">
            <label>{{ t._('Your email') }}</label> <span class="required">*</span>
            <input type="email" class="form-control" required="true" placeholder="ex. example@hostmail.com" name="email">
        </div>
        <div class="form-group">
            <label>{{ t._('Title') }}</label> <span class="required">*</span>
            {{ text_field('title', 'class' : 'form-control', 'required' : true) }}
        </div>
        <div class="form-group">
            <label>{{ t._('Description') }}</label>
            {{ text_area('des', 'class' : 'form-control', 'rows' : 5) }}
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">{{ t._('Send Us') }}</button>
        </div>

        {{ end_form() }}
    </div>
</div>

<style type="text/css">
    .contact-form {
        background: #fff;
        margin: 0;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        // Search
        $('input.keyword').keyup(function(e){
            if(e.keyCode == 13) {
                window.location.href = '<?php echo $this->url->get() ?>?keyword='+ $(this).val();
            }
        }); // -------------
    });
</script>