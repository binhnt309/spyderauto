<div id="slider1_container"
     style="position: relative; top: 0px; left: 0px; width: 1045px; height: 300px; overflow: hidden; ">
    <div u="slides"
         style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1045px; height: 300px; overflow: hidden;">

        {% if banners | length %}
        {% for banner in banners %}
        <div>

            <img u="image" src="/uploads/banner/{{ banner.getImage() }}"/>

            {% if( banner.getTitle() )%}
            <div u=caption t="*" class="captionOrange"
                 style="position:absolute; left:20px; top: 30px; width:300px; height:30px;">
                {{ banner.getTitle() }}
            </div>
            {% endif %}
        </div>
        {% endfor %}
        {% endif %}
    </div>

    <div u="navigator" class="jssorb01" style="bottom: 16px; right: 10px;">
        <!-- bullet navigator item prototype -->
        <div u="prototype"></div>
    </div>

    <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="top: 123px; left: 8px;">
        </span>
    <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="top: 123px; right: 8px;">
        </span>
</div>

<div class="auth-form">
    <form name="login" method="post">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4 login-data">
                <h2>{{ t._('Login') }}</h2>

                {{ flashSession.output() }}

                <div class="form-group">
                    <label for="certificate">{{ t._('Username') }} Or Email:</label><span class="required"></span>
                    {{ text_field('certificate', 'placeholder':'Enter username or email', 'class' : 'form-control',
                        'value' : req.getPost('certificate') ? req.getPost('certificate') : '') }}
                </div>
                <div class="form-group">
                    <label for="pwd">{{ t._('Password') }}:</label><span class="required"></span>
                    {{ password_field('pwd', 'class' : 'form-control', 'placeholder' : 'Enter you password' )}}
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">{{ t._('Login') }}</button>
                    <a href="/member/forgetPass" class="pull-right hidden">{{ t._('Forgot password')}}</a>
                </div>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </form>
</div>