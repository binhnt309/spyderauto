<?php

$router = new \Phalcon\Mvc\Router();

/**
 * Frontend
 */
$router->add(
    '/:controller',
    array(
        'namespace' => 'Application\Controllers\Frontend',
        'controller' => 1,
        'action' => 'index'
    )
);
$router->add(
    '/:controller/:action/:params',
    array(
        'namespace' => 'Application\Controllers\Frontend',
        'controller' => 1,
        'action' => 2,
        'params' => 3,
    )
);

$router->add(
    '/product/([a-zA-Z0-9-]+)',
    array(
        'namespace' => 'Application\Controllers\Frontend',
        'controller' => 'product',
        'action' => 'index',
        'slug' => 1
    )
);

$router->add(
    '/product/detail/([a-zA-Z0-9-]+)',
    array(
        'namespace' => 'Application\Controllers\Frontend',
        'controller' => 'product',
        'action' => 'detail',
        'slug' => 1
    )
);

$router->add(
    '/article/([a-zA-Z0-9-]+)',
    array(
        'namespace' => 'Application\Controllers\Frontend',
        'controller' => 'article',
        'action' => 'detail',
        'slug' => 1
    )
);

$router->add(
    '/article/detail/([a-zA-Z0-9-]+)',
    array(
        'namespace' => 'Application\Controllers\Frontend',
        'controller' => 'article',
        'action' => 'detail',
        'slug' => 1
    )
);
$router->add(
    '/home',
    array(
        'namespace' => 'Application\Controllers\Frontend',
        'controller' => 'index',
        'action' => 'index'
    )
);

/**
 * Admin
 */
$router->add(
    '/admin',
    array(
        'namespace' => 'Application\Controllers\Backend',
        'action' => 'index'
    )
);
$router->add(
    '/admin/:controller',
    array(
        'namespace' => 'Application\Controllers\Backend',
        'controller' => 1
    )
);

$router->add(
    '/admin/:controller/:action/([0-9]+)/:params',
    array(
        'namespace' => 'Application\Controllers\Backend',
        'controller' => 1,
        'action' => 2,
        'id' => 3,
        'params' => 4
    )
);
$router->add(
    '/admin/:controller/:action/:params',
    array(
        'namespace' => 'Application\Controllers\Backend',
        'controller' => 1,
        'action' => 2,
        'params' => 3,
    )
);

// Options
$router->removeExtraSlashes(true);

// Return router rules
return $router;