<?php

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new \Phalcon\DI\FactoryDefault();

/**
 * A component that allows manage static resources such as css stylesheets or javascript libraries in a web application
 */
$di->set(
    'assets',
    function () use ($config) {
        return new \Phalcon\Assets\Manager([
            /*'output' => realpath($config->application->baseDir),
            'compileAlways' => false,
            'stat' => true*/
            'compileAlways' => false,
            'stat' => true
        ]);
    },
    true
);

/*$di->set('flash', function(){
    $flash = new \Phalcon\Flash\Direct(array(
        'error' => 'alert alert-error',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info',
    ));
    return $flash;
});*/

/**
 * Re-declare flashSession
 */
$di->set('flashSession', function() {
   $flashSession = new \Phalcon\Flash\Session(array(
       'error' => 'alert alert-danger',
       'success' => 'alert alert-success',
       'warning' => 'alert alert-warning'
   ));
    return $flashSession;
});

$di->set('closeHtmlButton', function() {
    return '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set(
    'url',
    function () use ($config) {
        $url = new \Phalcon\Mvc\Url();
        $url->setBaseUri($config->application->baseUri);
        return $url;
    },
    true
);
$di->set('config', $config);

/**
 * Setting up the view component
 */
$di->set(
    'view',
    function () use ($config) {

        $view = new \Phalcon\Mvc\View();

        $view->setViewsDir($config->application->viewsDir);

        $view->registerEngines(
            array(
                '.volt' => function ($view, $di) use ($config) {

                    $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);

                    $volt->setOptions(
                        array(
                            'compiledPath' => $config->application->cacheDir,
                            'compiledSeparator' => '_'
                        )
                    );

                    return $volt;
                },
                '.phtml' => '\Phalcon\Mvc\View\Engine\Php'
            )
        );

        return $view;
    },
    true
);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set(
    'db',
    function () use ($config) {
        return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            'host' => $config->database->host,
            'username' => $config->database->username,
            'password' => $config->database->password,
            'dbname' => $config->database->dbname,
            'charset' => $config->database->charset
        ));
    }
);

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set(
    'modelsMetadata',
    function () {
        return new \Phalcon\Mvc\Model\Metadata\Memory();
    }
);

/**
 * Start the session the first time some component request the session service
 */
$di->set(
    'session',
    function () {
        $session = new \Phalcon\Session\Adapter\Files();
        $session->start();
        return $session;
    }
);

$di->set('cookies', function() {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(false);
    return $cookies;
});

/**
 * Component responsible for instantiating controllers and executing the required actions on them in an MVC application
 */
$di->set(
    'dispatcher',
    function () {
        //Create an EventsManager
        $eventsManager = new \Phalcon\Events\Manager();

        //Attach a listener
        $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {

            //Handle 404 exceptions
            if ($exception instanceof \Phalcon\Mvc\Dispatcher\Exception) {
                $dispatcher->forward(array(
                    'controller' => 'index',
                    'action' => 'page404'
                ));
                return false;
            }

            //Alternative way, controller or action doesn't exist
            if ($event->getType() == 'beforeException') {
                switch ($exception->getCode()) {
                    case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward(array(
                            'controller' => 'index',
                            'action' => 'page404'
                        ));
                        return false;
                }
            }
        });

        $dispatcher = new \Phalcon\Mvc\Dispatcher();
        //Bind the EventsManager to the dispatcher
        $dispatcher->setEventsManager($eventsManager);

        $dispatcher->setDefaultNamespace('Application\Controllers\Frontend');
        return $dispatcher;
    }
);

/**
 * Load routes
 */
$di->set(
    'router',
    function () {
        return require __DIR__ . '/routes.php';
    },
    true
);

/**
 * 404
 */
/*$di->set('router',function() use($Config){
    $Router = new \Phalcon\Mvc\Router();
    $Router->notFound(array(
        "controller" => "index",
        "action" => "route404"
    ));
    return $Router;
});*/

$di->set('slug', function() {
    return new \Phalcon\Utils\Slug();
});

$di->set('stringHelper', function() {
    return new \Phalcon\Utils\StringHelper();
});

$di->set('helper', function() {
    return new ModelHelper();
});