<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 4/9/15
 * Time: 8:22 AM
 */
namespace Application\Controllers\Frontend;
class MemberController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();

        $auth = $this->session->get('auth');

        $action = $this->router->getActionName();

        if (!$auth & $action != 'register') {

            $this->view->disable();
            $controller = $this->router->getControllerName();

            if ($controller != 'auth' && $action != 'login') {
                $this->flashSession->output();

                $this->view->disable();
                $url = $this->router->getRewriteUri();
                if (substr($url, 0, 1) == '/') {
                    $url = substr($url, 1);
                }

                $this->response->redirect('auth/login?returnUrl=' . base64_encode($url));
            }

            return;
        }
    }

    public function indexAction()
    {
        // Do something
    }

    /**
     * Register action
     */
    public function registerAction()
    {
        $auth = $this->session->get('auth');
        if (isset($auth['is_logged']) & $auth['is_logged'] == true) {
            $this->view->disable();
            $this->response->redirect('member/profile');
            return;
        }

        $this->tag->setTitle('Register');

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-auth.css', true);

        $req = $this->request;
        $customer = new \CustomerExt();
        $countries = \LocationsExt::find([
            'conditions' => 'type=' . \LocationsExt::TYPE_COUNTRY,
            'order' => 'name'
        ]);
        $cities = \LocationsExt::find([
            'conditions' => 'type=' . \LocationsExt::TYPE_PROVINCE
        ]);
        $cities_arr = \ModelHelper::toArray($cities, 'id');

        if ($this->request->isPost()) {

            $customer->setFullname($req->getPost('fullname'));
            $customer->setFirstName($req->getPost('first_name'));
            $customer->setLastName($req->getPost('last_name'));
            $customer->setUsername($req->getPost('username'));
            $customer->setPassword($req->getPost('pwd') ? $this->security->hash($req->getPost('pwd')) : '');
            $customer->setEmail($req->getPost('email'));
            $customer->setPhone($req->getPost('phone'));
            $customer->setMobile($req->getPost('mobile'));
            $customer->setAddress($req->getPost('address'));
            $customer->setAddress2($req->getPost('address_2'));
            $customer->setDistrict($req->getPost('district'));
            if ($req->getPost('city')) {
                $customer->setProvince($req->getPost('city'));
                $customer->setProvinceName(
                    isset($cities_arr[$req->getPost('city')]) ? $cities_arr[$req->getPost('city')]['name'] : ''
                );
            } else {
                $customer->setProvinceName($req->getPost('province'));
            }
            $customer->setZipcode($req->getPost('zipcode'));
            $customer->setCountry($req->getPost('country'));

            $data = ['success' => true, 'msg' => '', 'file_name' => ''];
            if ($_FILES['avatar']['size'] > 0) {
                $data = $this->__upload('avatar',
                    $this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'avatar' . DS . date('Y-m-d', time()) . DS);
                $customer->setAvatar($data['file_name']);
            }

            if ($data['success']) {
                if ($customer->save()) {
                    $this->view->disable();
                    $this->session->set('auth', [
                        'member' => $customer->toArray(),
                        'is_logged' => true
                    ]);

                    // Check card
                    $sql = 'UPDATE `' . \CartItemsExt::getTable() . '` SET `session_id`='
                        . $customer->getId() . ' WHERE `session_id`="' . session_id() . '"';
                    $this->db->query($sql);
                    // -------

                    $this->response->redirect('member/profile'); //register_success

                } else {
                    $msg = ['Please review some error below:'];
                    foreach ($customer->getMessages() as $m) {
                        array_push($msg, ' - ' . $m->getMessage());
                    }
                    $this->flashSession->error(implode('<br/>', $msg));
                }
            } else {
                $this->flashSession->error($data['msg']);
            }
        }

        $this->view->setVars([
            'req' => $req,
            'customer' => $customer,
            'countries' => $countries,
            'country' => $req->getPost('country') ? $req->getPost('country') : 232,
            'cities' => $cities
        ]);
    }

    public function profileAction()
    {

        $req = $this->request;
        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-profile.css', true);
        $cities = \LocationsExt::find([
            'conditions' => 'type=' . \LocationsExt::TYPE_PROVINCE
        ]);
        $cities_arr = \ModelHelper::toArray($cities, 'id');
        $member = $this->session->get('auth')['member'];

        if ($this->request->isPost()) {
            $customer = \CustomerExt::findFirst($member['id']);
            $customer->setFullname($req->getPost('fullname'));
            $customer->setFirstName($req->getPost('first_name'));
            $customer->setLastName($req->getPost('last_name'));
            $customer->setPassword($req->getPost('pwd') ? $this->security->hash($req->getPost('pwd')) : $member['password']);
            $customer->setPhone($req->getPost('phone'));
            $customer->setMobile($req->getPost('mobile'));
            $customer->setAddress($req->getPost('address'));
            $customer->setAddress2($req->getPost('address_2'));
            $customer->setDistrict($req->getPost('district'));

            if ($req->getPost('province')) {
                $customer->setProvince($req->getPost('province'));
            } else {
                $customer->setProvince($req->getPost('city'));
                $customer->setProvinceName(
                    isset($cities_arr[$req->getPost('city')]) ? $cities_arr[$req->getPost('city')]['name'] : ''
                );
            }
            $customer->setZipcode($req->getPost('zipcode'));
            $customer->setCountry($req->getPost('country'));

            $data = ['success' => true, 'msg' => '', 'file_name' => ''];
            if ($_FILES['avatar']['size'] > 0) {
                $data = $this->__upload('avatar',
                    $this->config->application->baseDir . 'public' . DS . 'uploads' .
                    DS . 'avatar' . DS . date('Y-m-d', strtotime($customer->getCreatedTime())) . DS);
                $customer->setAvatar($data['file_name']);
            }

            if ($data['success']) {
                if ($customer->save()) {
                    $this->session->set('auth', [
                        'member' => $customer->toArray(),
                        'is_logged' => true
                    ]);
                    $this->flashSession->success('Update profile success');
                    $this->session->set('auth', [
                        'is_logged' => true,
                        'member' => $customer->toArray()
                    ]);
                    $member = $customer->toArray();
                } else {
                    $msg = ['Please review some error below:'];
                    foreach ($customer->getMessages() as $m) {
                        array_push($msg, ' - ' . $m->getMessage());
                    }
                    $this->flashSession->error(implode('<br/>', $msg));
                }
            } else {
                $this->flashSession->error($data['msg']);
            }
        }

        $countries = \LocationsExt::find([
            'conditions' => 'type=' . \LocationsExt::TYPE_COUNTRY,
            'order' => 'name'
        ]);

        $avatar = file_exists($this->config->application->baseDir
            . 'public' . DS . 'uploads' . DS . 'avatar' . DS .
            date('Y-m-d', strtotime($member['created_time'])) . DS . $member['avatar'])
        & !empty($member['avatar']) ?
            $this->config->application->baseUri . 'uploads/avatar/' . date('Y-m-d', strtotime($member['created_time'])) . '/' . $member['avatar']
            :
            $this->config->application->baseUri . 'uploads/avatar/no_avatar.jpg';

        $this->view->setVars([
            'req' => $this->request,
            'countries' => $countries,
            'avatar' => $avatar,
            'country' => $member['country'] ? $member['country'] : 232,
            'cities' => $cities
        ]);
    }

    /**
     * @param $file
     * @param $dir
     * @return array
     */
    private function __upload($file, $dir)
    {
        $result = [
            'success' => true,
            'msg' => '',
            'file_name' => ''
        ];
        require(dirname(__FILE__) . '/../../helpers/FileUpload.php');
        $uploader = new \FileUpload($file);
        $valid_extensions = array('gif', 'png', 'jpeg', 'jpg');

        if (!is_dir($dir)) {
            $old_mask = umask(0);
            mkdir($dir, 0777);
            umask($old_mask);
        }

        // Handle the upload
        $upload = $uploader->handleUpload($dir, $valid_extensions);

        if (!$upload) {
            $result['success'] = false;
            $result['msg'] = $uploader->getErrorMsg();
        } else {
            $result['file_name'] = $uploader->getFileName();
        }

        return $result;
    }

    /**
     * @param int $id
     */
    public function orderAction($id = 0)
    {
        // Check load order detail
        if ($id) {
            $this->__loadOrderDetail($id);
            return;
        } // ---------------------

        if ($this->request->isAjax()) {
            $this->view->disable();

            $this->__loadOrder();
            return;
        }

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-profile.css', true)
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true)
            ->addCss('assets/css/jquery.datetimepicker.css', true);

        $this->assets->collection('frontendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js')
            ->addJs('assets/js/plugins/jquery-ui/jquery.datetimepicker.js');

        // Load list order of current logger
        $member = $this->session->get('auth')['member'];
        $orders = \OrdersExt::find([
            'conditions' => "customer_id = '{$member['id']}'",
            'order' => 'created_at DESC'
        ]);
        $status = \OrdersExt::statusArr();

        $this->view->setVars([
            'orders' => $orders,
            'avatar' => $this->__loadAvatar(),
            'status' => $status
        ]);
    }

    /**
     * @param $order_id
     */
    protected function __loadOrderDetail($order_id)
    {
        $this->tag->setTitle('Order detail');

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-profile.css', true);

        $order = \OrdersExt::findFirst($order_id);
        $orderItems = \OrderItemsExt::find([
            'conditions' => "order_id = $order_id"
        ]);

        $this->view->pick('member/order_detail');

        $this->view->setVars([
            'avatar' => $this->__loadAvatar(),
            'order' => $order,
            'orderItems' => $orderItems,
            'system_config' => $this->config->application
        ]);
    }

    protected function __loadOrder()
    {
        $arr_fields = array(
            'id',
            'created_at',
            'total_items',
            'amount',
            'fee_amount',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $date_from = $this->request->get('from');
        $date_to = $this->request->get('to');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($date_from)) {
            $condition .= " AND created_at > '{$date_from}' AND created_at < '{$date_to}'";
        }

        $total = \OrdersExt::count(array('conditions' => $condition));

        $orders = \OrdersExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $result = array();
        $status = \OrdersExt::statusArr();
        foreach ($orders as $key => $m) {
            if ($m instanceof \OrdersExt) {

                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getCode();
                $result[$key][] = $m->getCreatedAt();
                $result[$key][] = $m->getTotalItems();
                $result[$key][] = $m->getAmount();
                $result[$key][] = $m->getFeeAmount();
                $result[$key][] = isset($status[$m->getStatus()]) ? $status[$m->getStatus()] : $status[\OrdersExt::STATUS_PAID];
                $result[$key][] = "<a href='" . $this->config->application->baseUri
                    . "member/order/{$m->getId()}'>" . $this->_getTranslation()->_('View') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    public function transactionAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadTransaction();
            return;
        }

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-profile.css', true)
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true)
            ->addCss('assets/css/jquery.datetimepicker.css', true);

        $this->assets->collection('frontendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js')
            ->addJs('assets/js/plugins/jquery-ui/jquery.datetimepicker.js');

        $this->view->setVars([
            'avatar' => $this->__loadAvatar(),
        ]);

    }

    protected function __loadTransaction()
    {
        $arr_fields = array(
            'id',
            'order_code',
            'tx_transaction',
            'amount',
            'created_at',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $date_from = $this->request->get('from');
        $date_to = $this->request->get('to') ? $this->request->get('to') : date('Y-m-d') . '23:59:59';

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $member = $this->session->get('auth')['member'];
        $condition = "customer_id='{$member['id']}'";
        if (!empty($date_from) || !empty($date_to)) {
            $condition .= " AND created_at > '{$date_from}' AND created_at < '{$date_to}'";
        }

        $total = \TransactionsExt::count(array('conditions' => $condition));

        $orders = \TransactionsExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $result = array();
        $status = \TransactionsExt::statusArr();
        foreach ($orders as $key => $m) {
            if ($m instanceof \TransactionsExt) {
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getOrderCode();
                $result[$key][] = $m->getTxTransaction();
                $result[$key][] = $m->getAmount();
                $result[$key][] = $m->getCreatedAt();
                $result[$key][] = isset($status[$m->getStatus()]) ? $status[$m->getStatus()] : $status[\TransactionsExt::STATUS_PAID];
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Register page after register new customer
     */
    public function registerSuccessAction()
    {

        $this->tag->setTitle('Register Success');

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-auth.css', true);

    }

    /**
     * @return string
     */
    private function __loadAvatar()
    {
        $member = $this->session->get('auth')['member'];
        $avatar = file_exists($this->config->application->baseDir
            . 'public' . DS . 'uploads' . DS . 'avatar' . DS .
            date('Y-m-d', strtotime($member['created_at'])) . DS . $member['avatar'])
        & !empty($member['avatar']) ?
            $this->config->application->baseUri . 'uploads/avatar/' . date('Y-m-d', strtotime($member['created_time'])) . '/' . $member['avatar']
            :
            $this->config->application->baseUri . 'uploads/avatar/no_avatar.jpg';
        return $avatar;
    }


    public function forgetPassAction()
    {

    }

    public function resetPassAction()
    {

    }

}