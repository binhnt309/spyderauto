<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 1/23/15
 * Time: 2:48 PM
 */
namespace Application\Controllers\Frontend;

class AuthController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        // Set link for css, js
        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-auth.css', true);
    }

    public function loginAction()
    {
        if($this->request->isAjax()) {
            $this->view->disable();
        }

        $this->assets->collection('libraryJs')
            ->addJs('assets/js/jssor.js', true)
            ->addJs('assets/js/jssor.slider.js', true)
            ->addJs('assets/js/sp-banner.js', true);

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-banner.css', true);

        // Load banner -----------
        $banners = \BannersExt::find([
            'conditions' => "status=" . \BannersExt::STATUS_APPROVED,
            'order' => 'pos'
        ]);

        // Check login
        if ($this->session->get('auth')['is_logged']) {
            $this->view->disable();
            $this->response->redirect('member/profile');
        } else {
            \Phalcon\Tag::setTitle('Login');
            $req = $this->request;

            // Check post data ------------------------
            if ($req->isPost()) {

                // Check remember login
                $customer = \CustomerExt::findFirst(
                    "username='{$req->getPost('certificate')}' OR email='{$req->getPost('certificate')}'"
                );
                if ($customer instanceof \CustomerExt) {
                    if ($this->security->checkHash($this->request->getPost('pwd'), $customer->getPassword())) {
                        // Login success
                        $this->view->disable();

                        $this->session->set('auth', [
                            'is_logged' => true,
                            'member' => $customer->toArray()
                        ]);

                        // Check card
                        $sql = 'UPDATE `' . \CartItemsExt::getTable() . '` SET `session_id`='
                            . $customer->getId() . ' WHERE `session_id`="' . session_id() . '"';
                        $this->db->query($sql);
                        //-------------

                        if ($this->request->isAjax()) {
                            echo json_encode([
                                'success' => true
                            ]);

                            return;
                        }

                        if ($req->get('returnUrl')) {
                            $this->response->redirect(base64_decode($this->request->get('returnUrl')));
                        } else {
                            // Default redirect to profile page
                            $this->response->redirect('member/profile');
                        }
                    } else {
                        $this->flashSession->error($this->_getTranslation()->_('Password in-correct!'));

                        if ($this->request->isAjax()) {
                            echo json_encode([
                                'success' => false,
                                'msg' => '<div class="alert alert-danger">
                                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                ' . $this->_getTranslation()->_('Password in-correct!') . '
                                            </div>'
                            ]);
                            return;
                        }
                    }
                } else {
                    $this->flashSession->error($this->_getTranslation()->_('Username or email does not exited!'));

                    if ($this->request->isAjax()) {
                        echo json_encode([
                            'success' => false,
                            'msg' => '<div class="alert alert-danger">
                                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                ' . $this->_getTranslation()->_('Username or email does not exited!') . '
                                            </div>'
                        ]);
                        return;
                    }
                }
            } // ----------------------------------

            $this->view->setVars(array(
                'req' => $req,
                'banners' => $banners
            ));
        }

    }

    public function logoutAction()
    {
        $this->view->disable();
        $this->session->remove('auth');
        //$this->cookies->reset();
        $this->response->redirect('home');
    }

}