<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 4/9/15
 * Time: 8:22 AM
 */

namespace Application\Controllers\Frontend;

class ArticleController extends ControllerBase
{
    /**
     * @param string $slug
     */
    public function indexAction($slug = '')
    {

    }

    /**
     * @param string $slug
     */
    public function detailAction($slug = '')
    {
        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-article.css', true);

        $slug = preg_split('/-/', $slug);
        $id = count($slug) ? $slug[count($slug) - 1] : 0;

        $article = \ArticlesExt::findFirst([
            'conditions' => 'id=' . $id . ' AND status=' . \ArticlesExt::STATUS_APPROVED]
        );
        $img = '/uploads/articles/default.png';

        if ($article instanceof \ArticlesExt) {
            if ($article->getThumb()!= null & file_exists($this->config->application->baseDir . 'public' . DS . 'uploads' .
                    DS . 'articles' . DS . date('Y-m-d', strtotime($article->getCreatedAt())) . DS . $article->getThumb())
            ) {
                $img = $this->config->application->baseUri . 'uploads/articles/' .
                    date('Y-m-d', strtotime($article->getCreatedAt())) . '/' . $article->getThumb();
            }
        } else {
            $article = new \ArticlesExt();
        }

        // Load seo
        $seo = \SeoExt::findFirst([
            'conditions' => "type=".\SeoExt::TYPE_PRODUCT." AND object_id={$id}"
        ]);
        if($seo instanceof \SeoExt) {
            $this->tag->setTitle($seo->getTitle() ? $seo->getTitle() : $article->getTitle());
        }

        $this->view->setVars([
            'article' => $article,
            'img' => $img,
            'meta_keyword' => $seo->getKeyword(),
            'meta_description' => $seo->getDescription()
        ]);
    }
}