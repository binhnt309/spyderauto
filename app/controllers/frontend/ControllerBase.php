<?php

namespace Application\Controllers\Frontend;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

    public function initialize()
    {

        // Assets manager: Js and Css
        $this->assets->collection('frontendCss')
            ->addCss('assets/frontend/style.css', true);

        $this->assets->collection('libraryJs')
            ->addJs('assets/js/jquery-1.10.2.js', true);

        $this->assets->collection('frontendJs')
            ->addJs('assets/js/script.js', true)
            ->addJs('assets/js/supperfish.js', true);

        // Load config
        $system = \SystemExt::find([
            'columns' => 'keyword, value'
        ]);

        $system_config = [];

        if(count($system)) {
            foreach($system as $s) {
                $system_config[$s->keyword] = $s->value;
            }
        }

        // Default title
        $this->tag->setTitle(isset($system_config['title']) ? $system_config['title'] : $this->_getTranslation()->_('Home'));

        // Product categories
        $pro_cats_obj = \ProductCategoriesExt::find([
            'conditions' => "status=" . \ProductCategoriesExt::STATUS_APPROVED,
            'order' => 'pos, created_time'
        ]);// -----------------

        // Load categories for arts
        $art_cats = [];
        $art_cats_obj = \ArtCategoriesExt::find([
            'conditions' => 'status=' . \ArtCategoriesExt::STATUS_ENABLED
        ]);
        if($art_cats_obj) {
            foreach ($art_cats_obj as $inc => $c) {
                if(!$c instanceof \ArtCategoriesExt){
                    continue;
                }
                $arts = \ArticlesExt::find([
                    'conditions' => 'cid=' . $c->getId() . " AND status = " . \ArticlesExt::STATUS_APPROVED,
                    'columns' => 'id, title, top, bottom',
                    'order' => 'pos ASC, created_at DESC'
                ]);

                $art_cats[$inc] = $c->toArray();
                $art_cats[$inc]['child'] = $arts->toArray();

            }
        } // --------------------

        $returnUrl = $this->router->getRewriteUri();
        if (substr($returnUrl, 0, 1) == '/') {
            $returnUrl = substr($returnUrl, 1);
        }

        $this->view->setVars([
            'config' => $system_config,
            't' => $this->_getTranslation(),
            'art_cats' => $art_cats,
            'pro_cats' => $pro_cats_obj->toArray(),
            'currencies' => \ModelHelper::toArray(\CurrencyExt::find(), 'id'),
            'meta_keyword' => $system_config['keyword'],
            'meta_description' => $system_config['description'],
            'returnUrl' => base64_encode($returnUrl)
        ]);
    }

    public function _getTranslation()
    {
        $messages = array();
        // Get language
        $language = $this->request->get('_l');
        if (!$language) {
            $language = $this->session->get('_l');
            if (!$language) {
                $language = $this->request->getBestLanguage();
            }
        }
        $this->session->set('_l', $language);

        //Check if we have a translation file for that lang
        if (file_exists(__DIR__ . "/../../../app/messages/" . $language . ".php")) {
            require __DIR__ . "/../../../app/messages/" . $language . ".php";
        } else {
            // fallback to some default
            if (file_exists(__DIR__ . "/../../../app/messages/en.php"))
                require __DIR__ . "/../../../app/messages/en.php";
        }

        //Return a translation object
        return new \Phalcon\Translate\Adapter\NativeArray(array(
            "content" => $messages
        ));

    }

    public function afterExecuteRoute()
    {
        $this->view->setViewsDir($this->view->getVIewsDir() . 'frontend/');
    }

}