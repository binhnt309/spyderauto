<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 4/9/15
 * Time: 8:21 AM
 */

namespace Application\Controllers\Frontend;

use Phalcon\Exception;

class CartController extends ControllerBase
{
    /**
     * View cart data
     */
    public function indexAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();

            $result = [];

            switch ($this->request->get('option')) {
                case 'getCoupon':
                    $result = $this->__loadCoupon($this->session->get('auth')['member']['id'], $this->request->get('code'));
                    break;
                case 'setCoupon':
                    $auth = $this->session->get('auth');
                    $data = ['coupon_selected' => ($this->request->get('data') == 'true' ? true : false), 'coupon_code' => $this->request->get('code')];

                    $session_data = array_merge($auth, $data);

                    $this->session->set('auth', $session_data);
                    $result = [$this->request->get('data')];
                    break;

                default:
                    break;
            }

            echo json_encode($result);
            return;
        }

        $this->tag->setTitle('Cart items');

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-cart.css', true);

        // Load list items
        $auth = $this->session->get('auth');
        if (isset($auth['member'])) {
            $user_id = $auth['member']['id'];
        } else {
            $user_id = session_id();
        }

        $cart_items = \CartItemsExt::find([
            'conditions' => "session_id='$user_id'"
        ]);

        if (!count($cart_items)) {
            $this->flashSession->warning($this->closeHtmlButton . 'Your cart is empty. Please order greater than or equal one item before continue.');
        }

        $this->view->setVars([
            'items' => $cart_items->toArray(),
            'req' => $this->request,
            'meta_keyword' => 'Keyword for cart page'
        ]);
    }

    /**
     * Display cart information form
     */
    public function shippingAction()
    {
        $this->tag->setTitle('User information payment');

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-cart.css', true);


        $countries = \LocationsExt::find([
            'conditions' => 'type=' . \LocationsExt::TYPE_COUNTRY,
            'order' => 'name'
        ]);

        $cities = \LocationsExt::find([
            'conditions' => 'type=' . \LocationsExt::TYPE_PROVINCE,
            'order' => 'parent_id, name'
        ]);

        // Load customer info
        $auth = $this->session->get('auth');
        if ($auth) {
            $user = $auth['member'];
            $session_id = $user['id'];
        } else {
            $session_id = session_id();
            $user = new \CustomerExt();
            $user = $user->toArray();
        }

        // Load items
        $cart_items = \CartItemsExt::count([
            'conditions' => "session_id='$session_id'"
        ]);
        if (!$cart_items) {
            $this->view->disable();
            $this->response->redirect('cart/index');
            return;
        } // ------------

        $this->view->setVars([
            'user' => $user,
            'countries' => \ModelHelper::toArray($countries, 'id'),
            'countries_obj' => $countries,
            'cities' => $cities,
            'country' => 232 // United States
        ]);
    }

    public function reviewAction()
    {

        $this->tag->setTitle('Cart Review');

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-cart.css', true);

        $coupon = new \CouponExt();
        $session = $this->session->get('auth');

        if (isset($session['coupon_selected']) & $session['coupon_selected']) {
            $code = $session['coupon_code'];
            $coupon = \CouponExt::findFirst("code='$code'");

            if ($coupon instanceof \CouponExt) {
                if (strtotime($coupon->getStart()) >= time() || strtotime($coupon->getEnd()) <= time()) {
                    $coupon = new \CouponExt();
                    $this->flashSession->warning($this->closeHtmlButton . "Coupon has been expired.");
                } else {
                    // Check coupon has been used
                    $user_coupon = \CouponUserExt::findFirst('uid=' . $session['member']['id'] . ' AND coupon_id=' . $coupon->getId());
                    if ($user_coupon instanceof \CouponUserExt) {
                        if (!$user_coupon->getValidate()) {
                            $coupon = new \CouponExt();
                            $this->flashSession->warning($this->closeHtmlButton . "This coupon has been used for other order.");
                        }
                    } else {
                        $coupon = new \CouponExt();
                        $this->flashSession->warning($this->closeHtmlButton . "Can't find this coupon code for you at this time.");
                    }
                }
            }
        } // ------------

        if ($this->request->isPost()) {

            $data = $this->request->getPost();
            $countries = \LocationsExt::find([
                'conditions' => 'type=' . \LocationsExt::TYPE_COUNTRY
            ]);
            $countries_arr = \ModelHelper::toArray($countries, 'id');
            $cities = \LocationsExt::find([
                'conditions' => 'type=' . \LocationsExt::TYPE_PROVINCE
            ]);
            $cities_arr = \ModelHelper::toArray($cities, 'id');

            if ($data['ship_new_address']) {
                if($data['country'])
                    $condition = "country_id=" . $data['country'];
                if ($data['city']) {
                    $condition .= ' AND city_id="' . $data['city'] . '"';
                }
                $data['country'] = $countries_arr[$data['country']]['name'];
                $data['province'] = $cities_arr[$data['city']]['name'];
            } else {
                $member = $this->session->get('auth')['member'];
                $condition = "country_id=" . $member['country'];
                if ($member['province']) {
                    $condition .= ' AND city_id="' . $member['province'] . '"';
                }

                // Set data for order
                $data['fullname'] = $member['fullname'];
                $data['email'] = $member['email'];
                $data['phone'] = $member['phone'];
                $data['mobile'] = $member['mobile'];
                $data['address'] = $member['address'];
                $data['address_2'] = $member['address_2'];
                $data['province'] = isset($cities_arr[$member['province']]) ? $cities_arr[$member['province']]['name'] : $member['province_name'];
                $data['country'] = isset($countries_arr[$member['country']]) ? $countries_arr[$member['country']]['name'] : '';
                $data['zip_code'] = $member['zipcode'];
            }

            // Load list item in cart
            $auth = $this->session->get('auth');
            if (isset($auth['member'])) {
                $user_id = $auth['member']['id'];
            } else {
                $user_id = session_id();
            }

            $cart_items = \CartItemsExt::find(['conditions' => "session_id='$user_id'"]);

            // Get Shipping data
            $ship_fee_obj = \ShippingFeeExt::findFirst([
                'conditions' => $condition
            ]);

            // Load fee ship default
            $fee = $this->config->ship_fee;
            $time_trans = $this->config->time_transfer;
            if ($ship_fee_obj instanceof \ShippingFeeExt) {
                $fee = $ship_fee_obj->getAmount();
                $time_trans = $ship_fee_obj->getTimeDelivery();
            }

            // Set order data to session
            $this->session->set('order', ['data' => $data, 'step' => '3', 'fee' => $fee]);

            // Process data
            $this->view->setVars([
                'fee' => $fee,
                'time_trans' => $time_trans,
                'data' => $data,
                'carts' => $cart_items
            ]);

        } else {
            $this->response->redirect('cart');
        }

        $this->view->setVars([
            'baseUri' => $this->config->application->baseUri,
            'coupon' => $coupon
        ]);
    }

    public function successAction()
    {

        /*$data = [
            ["mc_gross"]=>  "0.23",
            ["protection_eligibility"]=> "Eligible",
            ["address_status"]=> "unconfirmed",
            ["item_number1"]=> "",
            ["item_number2"]=> "",
            ["payer_id"]=> "BMAWMGE7FR56W",
            ["tax"]=> "0.00",
            ["address_street"]=> "Ha noi",
            ["payment_date"]=> "20:52:48 May 04, 2015 PDT",
            ["payment_status"]=> "Completed",
            ["charset"]=> "windows-1252",
            ["address_zip"]=> "",
            ["mc_shipping"]=> "0.03",
            ["mc_handling"]=> "0.00",
            ["first_name"]=> "binh", ["mc_fee"]=> "0.23",
            ["address_country_code"]=> "VN",
            ["address_name"]=> "binh nguyen",
            ["notify_version"]=> "3.8",
            ["custom"]=> "",
            ["payer_status"]=> "verified",
            ["business"]=> "bieu_huynh@yahoo.com",
            ["address_country"]=> "Vietnam",
            ["num_cart_items"]=> "2", ["mc_handling1"]=> "0.00",
            ["mc_handling2"]=> "0.00",
            ["address_city"]=> "Ha noi",
            ["payer_email"]=> "ntbinh30986@gmail.com",
            ["verify_sign"]=> "AFcWxV21C7fd0v3bYYYRCpSSRl31AYg5XbbyHQjXBnY4uX59PXL5WjGi",
            ["mc_shipping1"]=> "0.01",
            ["mc_shipping2"]=> "0.02",
            ["tax1"]=> "0.00",
            ["tax2"]=> "0.00",
            ["txn_id"]=> "4PP67201K1396732V",
            ["payment_type"]=> "instant",
            ["last_name"]=> "nguyen",
            ["item_name1"]=> "Acura Integra 94-01 4Dr Euro Style Tail Lights - Black",
            ["address_state"]=> "",
            ["receiver_email"]=> "bieu_huynh@yahoo.com",
            ["item_name2"]=> "Acura Integra 94-01 4Dr Euro Style Tail Lights - Black",
            ["payment_fee"]=> "0.23",
            ["quantity1"]=> "1",
            ["quantity2"]=> "1",
            ["receiver_id"]=> "TNZFY53XFE3B2",
            ["txn_type"]=> "cart",
            ["mc_gross_1"]=> "0.11",
            ["mc_currency"]=> "USD",
            ["mc_gross_2"]=> "0.12",
            ["residence_country"]=> "VN",
            ["transaction_subject"]=> "",
            ["payment_gross"]=> "0.23",
            ["auth"]=> "AjwrThCSMn28kA5WgLtMV9zWe2fMQHjDyCdXH3nkAdhGOIB1uZWeE017jK2g1dxESo0kgWCB0qfIzqzqDVCuzxg"
        ];*/

        $customer_id = session_id();
        if ($this->session->get('auth')) {
            $member = $this->session->get('auth');
            $customer_id = $member['member']['id'];
        }

        $cart = \CartItemsExt::find("session_id='$customer_id'");
        if (!$cart) {
            $this->view->disable();
            $this->response->redirect('cart/index');
            return;
        } // ------

        $success = true;

        if ($this->request->isPost()) {

            $manager = new \Phalcon\Mvc\Model\Transaction\Manager();
            $trans = $manager->get();

            try {
                $order = $this->__createOrder($customer_id, $trans);
                $this->__createTransaction($order, $customer_id, $trans);

                $trans->commit();

            } catch (Exception $e) {
                $this->flashSession->error('Create order error.' . $e);
                $success = false;
            }

            // Create order success
            $this->__deleteCart($customer_id);

        } else {
            $this->response->redirect('cart/index');
        }

        $this->tag->setTitle('Payment success');

        $this->view->setVars([
            'success' => $success
        ]);

    }

    /**
     * Cancel payment from paypal
     */
    public function paymentCancelAction()
    {
        // Re-update coupon for this user if has
        $code = $this->session->get('auth')['coupon_code'];
        $uid = $this->session->get('auth')['member']['id'];
        if ($code) {
            $coupon = \CouponExt::findFirst("code='$code");
            if ($coupon instanceof \CouponExt) {
                $user_has_coupon = \CouponUserExt::findFirst("uid=$uid AND coupon_id={$coupon->getId()}");
                if ($user_has_coupon instanceof \CouponUserExt) {
                    $user_has_coupon->setValidate(1);
                    $user_has_coupon->save();
                }
            }
        } // ------------
        $this->response->redirect('cart/index');
    }

    // ---------------------------------
    // -------- Ajax action ------------
    // ---------------------------------

    /**
     * Calculate total items in your cart
     */
    public function totalItemsAction()
    {
        $this->view->disable();

        $auth = $this->session->get('auth');
        if ($auth['member']) {
            $session_id = $auth['member']['id'];
        } else {
            $session_id = session_id();
        }
        // Load cart by session id
        $total = \CartItemsExt::count([
            'conditions' => "session_id='$session_id'"
        ]);

        echo json_encode([
            'total' => $total
        ]);
    }

    /**
     * @param int $item_id
     */
    public function addItemToCartAction($item_id = 0)
    {

        $this->view->disable();

        $result = ['success' => true, 'msg' => '', 'type' => 'new']; // type = new or update
        $customer = $this->session->get('auth')['member'];

        // Check customer
        if ($customer) {
            $session_id = $customer['id'];
        } else {
            $session_id = session_id();
        } // -------------

        $amount = $this->request->getPost('quality');
        if ((int)$amount == 0) {
            echo json_encode([
                'success' => false,
                'msg' => '<div class="alert alert-warning">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            Quality must greeter than 0
                        </div>'
            ]);
            return;
        }
        $product_id = $this->request->getPost('pid');

        $product = \ProductsExt::findFirst($product_id);
        if (!$product instanceof \ProductsExt) {
            echo json_encode([
                'success' => false,
                'msg' => '<div class="alert alert-warning">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            Product not found
                        </div>'
            ]);
            return;
        } // ----------------

        $in_stock = $product->getStock();

        $item_cart = \CartItemsExt::findFirst([
            'conditions' => "session_id='{$session_id}' AND product_id=$product_id"
        ]);

        $type = 'update';
        if (!$item_cart instanceof \CartItemsExt) {
            $type = 'new';
            $item_cart = new \CartItemsExt();

            $item_cart->setProductId($product_id);
            $item_cart->setProductTitle($product->getTitle());
            $illustration = strpos($product->getIllustration(), 'http://') > -1 ? $product->getIllustration() :
                date('Y-m-d', strtotime($product->getCreatedTime())) . '/' . $product->getIllustration();
            $item_cart->setIllustration($illustration);
            $item_cart->setPrice($product->getSaleOff() & strtotime($product->getSaleOffTo()) > time() ?
                $product->getSaleOff() : $product->getPrice());
            $item_cart->setSessionId($session_id);
        }

        $item_cart->setAmount($item_cart->getAmount() + $amount);

        // Check current item in-stock
        if ($in_stock < $item_cart->getAmount()) {
            $result['success'] = false;
            $result['msg'] = '<div class="alert alert-warning">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            Item in-stock not enough. Please check again
                        </div>';
        } else if ($item_cart->save()) {
            $result['msg'] = '<div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            Add item to cart success
                        </div>';
        } else {
            $result['success'] = false;
            $msg = [];
            foreach ($item_cart->getMessages() as $m) {
                $msg[] = $m->getMessage();
            }
            $result['msg'] = '<div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            ' . implode('<br/>', $msg) . '
                        </div>';
        }

        // Return result
        $result['type'] = $type;
        echo json_encode($result);

    } // ----------

    /**
     * Update all items in cart
     */
    public function updateCartAction()
    {
        $this->view->disable();

        $result = [
            'success' => true,
            'data_fail' => [],
            'data_success' => [],
            'msg' => '<div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            Update items in cart success
                        </div>'
        ];
        /*$ids = $this->request->getPost('ids');
        $amount = $this->request->getPost('amount');*/

        $ids = [];
        $amount = [];
        if ($this->request->getPost()) {
            foreach ($this->request->getPost() as $k => $v) {
                $k = preg_split('/_/', $k);
                $ids[] = $k[1];
                $amount[] = $v;
            }
        }

        $auth = $this->session->get('auth');
        if ($auth) {
            $member_id = $auth['member']['id'];
        } else {
            $member_id = session_id();
        }

        if (is_array($ids) & is_array($amount)) {
            foreach ($ids as $k => $v) {

                $item = \CartItemsExt::findFirst([
                    'conditions' => "id={$v} AND session_id='$member_id'"
                ]);

                if ($item instanceof \CartItemsExt) {

                    $product = \ProductsExt::findFirst($item->getProductId());

                    if ($product instanceof \ProductsExt) {
                        if ($product->getStock() < $amount[$k]) {
                            $result['data_fail'][$k]['id'] = $v;
                            $result['data_fail'][$k]['msg'] = "Item \"{$product->getTitle()}\" has {$product->getStock()} in stock";
                            $result['data_fail'][$k]['quantity'] = $item->getAmount();
                        } else {
                            // Update quantity item of cart
                            $item->setAmount(isset($amount[$k]) ? $amount[$k] : $item->getAmount());
                            if ($item->save()) {
                                $result['data_success'][$k]['id'] = $v;
                            } else {
                                $result['data_fail'][$k]['id'] = $v;
                                foreach ($item->getMessages() as $m) {
                                    $result['data_fail'][$k]['msg'][] = $m->getMessage();
                                }
                                $result['data_fail'][$k]['msg'] = implode(', ', $result['data_fail'][$k]['msg']);
                            }
                        }
                    } else {
                        $result['data_fail'][$k]['id'] = $v;
                        $result['data_fail'][$k]['msg'] = "Product of items \"{$item->getProductTitle()}\" not found";
                        $result['data_fail'][$k]['quantity'] = $item->getAmount();
                    } // End if
                } else {
                    $result['data_fail'][$k]['id'] = $v;
                    $result['data_fail'][$k]['msg'] = 'Cart item not found';
                    $result['data_fail'][$k]['quantity'] = 0;
                } // End if
            } // End for
        }

        echo json_encode($result);
    }

    /**
     * @param int $item_id
     */
    public function removeItemAction($item_id = 0)
    {
        $this->view->disable();

        $result['success'] = false;

        $item_cart = \CartItemsExt::findFirst($item_id);
        if ($item_cart instanceof \CartItemsExt) {
            if ($item_cart->delete()) {
                $result['success'] = true;
                $result['msg'] = '<div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            Remove item from cart success
                        </div>';
            } else {
                $msg = [];
                foreach ($item_cart->getMessages() as $m) {
                    $msg[] = $m->getMessage();
                }
                $result['msg'] = implode('<br/>', $msg);
            }
        } else {
            $result['success'] = false;
            $result['msg'] = '<div class="alert alert-warning">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            Can\'t find this item
                        </div>';
        }

        echo json_encode($result);
    }

    /**
     * Re-check all item in-stock before pay order
     */
    public function checkStockAction()
    {
        $this->view->disable();

        $member_id = session_id();
        $auth = $this->session->get('auth');
        if ($auth) {
            $member_id = $auth['member']['id'];
        }

        $valid = true;
        $cart_items = \CartItemsExt::find("session_id='$member_id'");
        if (!count($cart_items)) {
            $valid = false;
            $this->flashSession->warning($this->closeHtmlButton . 'Your cart is empty. Please order greater than or equal one item before continue.');
        } else {
            foreach ($cart_items as $item) {
                if (!$item instanceof \CartItemsExt) {
                    $valid = false;
                    $this->flashSession->warning($this->closeHtmlButton . "Item \"{$item->getProductTitle()}\" not found in your cart. Please check it again before continue");
                    break;
                }
                $product = \ProductsExt::findFirst($item->getProductId());
                if (!$product instanceof \ProductsExt) {
                    $valid = false;
                    $this->flashSession->warning($this->closeHtmlButton . "Product \"{$item->getProductTitle()}\" not found. Please check it again before continue");
                    break;
                } else {
                    if ($product->getStock() < $item->getAmount()) {
                        $valid = false;
                        $this->flashSession->warning($this->closeHtmlButton . "Item \"{$product->getProductTitle()}\" just has {$product->getStock()} in stock");
                        break;
                    }
                }
            }
        }

        echo json_encode([
            'valid' => $valid
        ]);
    }

    /**
     * De-active coupon for this user
     */
    public function inActiveCouponAction()
    {

        $this->view->disable();

        if ($this->request->isPost()) {
            $code = $this->session->get('auth')['coupon_code'];
            $uid = $this->session->get('auth')['member']['id'];
            if ($code) {
                $coupon = \CouponExt::findFirst("code='$code'");
                if ($coupon instanceof \CouponExt) {
                    $user_has_coupon = \CouponUserExt::findFirst("uid=$uid AND coupon_id={$coupon->getId()}");
                    if ($user_has_coupon instanceof \CouponUserExt) {

                        $user_has_coupon->setValidate(0);

                        if ($user_has_coupon->save()) {
                            $this->checkStockAction();
                        } else {
                            echo json_encode([
                                'valid' => false
                            ]);
                        }
                    }
                } else {
                    $this->checkStockAction();
                }
            } else {
                $this->checkStockAction();
            }
        } else {
            echo json_encode([
                'Bad request'
            ]);
        }
    }

    // -------------------------------------
    // ----------Private function-----------
    // -------------------------------------

    /**
     * @param $uid
     * @param $code
     * @return array
     */
    private function __loadCoupon($uid, $code)
    {
        $result = [];
        $coupon = \CouponExt::findFirst("code='$code'");

        if ($coupon instanceof \CouponExt) {

            if (strtotime($coupon->getStart()) >= time() || strtotime($coupon->getEnd()) <= time()) {
                $result['msg'] = "Promotion code has been expired.";
                $result['success'] = false;
            } else {
                $user = \CouponUserExt::findFirst("uid=$uid AND coupon_id=" . $coupon->getId());

                if (!$user) {
                    $result['msg'] = "You can't use this coupon code.";
                    $result['success'] = false;
                }

                if ($user instanceof \CouponUserExt) {
                    if (!$user->getValidate()) {
                        $result['msg'] = "Your promotion code has been used";
                        $result['success'] = false;
                    } else {
                        $unit = \CouponExt::unitArr();
                        $result['success'] = true;
                        $result['amount'] = $coupon->getAmount();
                        $result['unit'] = $unit[$coupon->getUnit()];
                    }
                }

                // Check min order price
                if ($coupon->getLimitAmount() > 0 & $result['success']) {
                    // Load current price on his order
                    $sql = "SELECT SUM(price*amount) AS `sum` FROM `cart_items` WHERE session_id=$uid";
                    $data = $this->db->query($sql)->fetch();
                    $amount = $data['sum'];
                    if ($coupon->getLimitAmount() > $amount) {
                        $result['msg'] = "Your order must greater than " . $coupon->getLimitAmount() . '$ to get this coupon.';
                        $result['success'] = false;
                    }
                }
            }
        } else {
            $result['msg'] = "Promotion code is not exit.";
            $result['success'] = false;
        }

        return $result;
    }

    /**
     * @param string $customer_id
     * @param null $transaction
     * @return \OrdersExt
     */
    private function __createOrder($customer_id = '', $transaction = null)
    {
        $auth = $this->session->get('auth');
        $order_data = $this->session->get('order');
        $customer_data = $order_data['data'];

        $order = new \OrdersExt();
        $num = '01';
        // Find order of this customer on day
        $list = \OrdersExt::count("customer_id='$customer_id' AND date_format(created_at, '%Y-%m-%d') = '" . date('Y-m-d') . "'");
        if ($list) {
            $num = $list + 1;
            if ($num < 10) {
                $num = '0' . $num;
            }
        } // ----------

        $order->setCustomerId($customer_id);
        $order->setCode($auth ? $auth['member']['username'] . date('Y-m-d') . '-' . $num : $customer_id . '-' . $num);
        $order->setCustomerName($customer_data['fullname']);
        $order->setCustomerEmail($customer_data['email']);
        $order->setCustomerMobile($customer_data['mobile']);
        $order->setCustomerPhone($customer_data['phone']);
        $order->setCustomerAddress($customer_data['address']);
        $order->setCustomerAddress2($customer_data['address_2']);
        $order->setDistrictLabel($customer_data['district']);
        $order->setProvinceLabel($customer_data['province']);
        $order->setCountryLabel($customer_data['country']);
        $order->setFeeAmount($order_data['fee']);
        $order->setAmount($order_data['amount']);
        $order->setTotalItems($order_data['total_items']);
        $order->setZipcode($order_data['zip_code']);

        $coupon_code = isset($auth['coupon_code']) ? $auth['coupon_code'] : '';
        $unit = \CouponExt::unitArr();
        $coupon = \CouponExt::findFirst("code='$coupon_code'");
        $order->setPromotionCode($coupon_code);
        $order->setDiscount($coupon->getAmount() . " " . $unit[$coupon->getUnit()]);


        $order->setTransaction($transaction);

        if ($order->save()) {

            $this->__createItems($order->getId(), $customer_id, $transaction);

            return $order;
        } else {
            $msg = [];
            foreach ($order->getMessages() as $m) {
                $msg[] = '<br/> - ' . $m->getMessage();
            }

            $transaction->rollback(implode('', $msg));
        }
    }

    /**
     * @param int $order_id
     * @param string $customer_id
     * @param \Phalcon\Mvc\Model\Transaction $transaction
     */
    private function __createItems($order_id = 0, $customer_id = '', \Phalcon\Mvc\Model\Transaction $transaction)
    {
        // Load list cart items
        $cart_items = \CartItemsExt::find("session_id='$customer_id'");
        if (count($cart_items)) {
            foreach ($cart_items as $item) {
                if ($item instanceof \CartItemsExt) {
                    $order_item = new \OrderItemsExt();

                    $order_item->setTransaction($transaction);

                    $order_item->setOrderId($order_id);
                    $order_item->setProductId($item->getProductId());
                    $order_item->setProductTitle($item->getProductTitle());
                    $order_item->setPrice($item->getPrice());
                    //$order_item->setPromotionPrice($item->getP)
                    $order_item->setAmount($item->getAmount());
                    $order_item->setIllustration($item->getIllustration());

                    if (!$order_item->save()) {
                        $msg = [];
                        foreach ($order_item->getMessages() as $m) {
                            $msg[] = '<br/> - ' . $m->getMessage();
                        }
                        // Rollback transaction
                        $transaction->rollback(implode('', $msg));
                        break;
                    } else {
                        // Update amount product
                        $this->__updateProduct($order_item, $transaction);
                    }
                }
            }
        }
    }

    /**
     * @param \OrderItemsExt $order_item
     * @param \Phalcon\Mvc\Model\Transaction $transaction
     */
    private function __updateProduct(\OrderItemsExt $order_item, \Phalcon\Mvc\Model\Transaction $transaction)
    {
        $product = \ProductsExt::findFirst($order_item->getId());
        if ($product instanceof \ProductsExt) {
            $product->setTransaction($transaction);

            $product->setSale($product->getSale() + $order_item->getAmount());
            /*if($product->getStock() - $order_item->getAmount() < 0) {
                // Rollback transaction & stop update stock
                $transaction->rollback('Item with SKU ' . $product->getSku() . ' has not enough quantity');
            } else {
                $product->setStock($product->getStock() - $order_item->getAmount());
            }*/
            $product->setStock($product->getStock() - $order_item->getAmount());

            if (!$product->save()) {
                $msg = [];
                foreach ($product->getMessages() as $m) {
                    $msg[] = ' - ' . $m->getMessage();
                }
                // Rollback transaction
                $transaction->rollback('Update stock of product ' . $product->getSku() . ' error. <br/>' . implode('<br/>', $msg));
            }
        } else {
            // Rollback transaction
            $transaction->rollback('Product with id ' . $order_item->getId() . ' not found.');
        }
    }

    /**
     * @param \OrdersExt $order
     * @param int $customer_id
     * @param null $transaction
     */
    private function __createTransaction(\OrdersExt $order, $customer_id = 0, $transaction = null)
    {
        $trans = new \TransactionsExt();

        $trans->setTxTransaction($this->request->getPost('txn_id'));
        $trans->setOrderCode($order->getCode());
        $trans->setAmount($this->request->getPost('payment_gross'));
        $trans->setCurrency($this->request->getPost('mc_currency'));
        $trans->setCustomerId($customer_id);
        $trans->setOrderId($order->getId());
        $trans->setStatus(\TransactionsExt::STATUS_PENDING);

        $trans->setTransaction($transaction);

        if (!$trans->save()) { // Save transaction error
            $msg = [];
            foreach ($trans->getMessages() as $m) {
                $msg[] = '<br/> - ' . $m->getMessage();
            }
            $transaction->rollback(implode('', $msg));
        }
    }

    /**
     * @param string $customer_id
     * @return bool
     */
    private function __deleteCart($customer_id = '')
    {
        $cart = \CartItemsExt::find("session_id='$customer_id'");
        if (count($cart)) {
            foreach ($cart as $c) {
                $c->delete();
            }
        } // -----
        return true;
    }

}