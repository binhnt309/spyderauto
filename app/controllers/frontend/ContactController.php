<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 4/27/15
 * Time: 9:57 PM
 */
namespace Application\Controllers\Frontend;

class ContactController extends ControllerBase
{
    public function indexAction()
    {
        $this->tag->setTitle('About Us');
        $msg = '';
        if ($this->request->isPost()) {

            $auth = $this->session->get('auth');
            if ($auth) {
                $uid = $auth['member']['id'];
            } else {
                $uid = session_id();
            }

            $contact = \ContactExt::findFirst([
                'conditions' => 'uid=' . $uid . ' AND created_time < "' . date('Y-m-d H:i:s', time() + 1000 * 300) . '"',
                'order' => 'created_time'
            ]);
            if ($contact instanceof \ContactExt) {
                $msg = '<div class="alert alert-warning alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>Sorry!</strong> You have just sent email to me. Please wait at lease 5 minutes to continue.
                        </div>';
            } else {
                $contact = new \ContactExt();
                $contact->setName($this->request->getPost('name'));
                $contact->setEmail($this->request->getPost('email'));
                $contact->setTitle($this->request->getPost('title'));
                $contact->setDes($this->request->getPost('des'));
                $contact->setUid($uid);

                /*echo '<pre>';
                print_r($contact->toArray()); die();*/

                if($contact->save())
                    $msg = '<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <strong>Thank you!</strong> You have sent email to us success.
                            </div>';
                else {
                    $msg = '<div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>Sorry!</strong> Have an error when send email to us.
                        </div>';
                }
            }
        }

        $this->view->setVars([
            'msg' => $msg
        ]);
    }
}