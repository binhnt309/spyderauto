<?php

namespace Application\Controllers\Frontend;

use Phalcon\Exception;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            switch ($this->request->get('option')) {
                case 'loadModel':
                    echo json_encode($this->__loadModel($this->request->get('brand') ? $this->request->get('brand') : 0));
                    break;
                case 'loadYear':
                    echo json_encode($this->__loadYear($this->request->get('model') ? $this->request->get('model') : 0));
                    break;
                case 'loadProduct':
                    echo json_encode($this->__loadProduct(true, false));
                    break;
                default:
                    break;
            }
            return;
        }

        $this->assets->collection('libraryJs')
            ->addJs('assets/js/jssor.js', true)
            ->addJs('assets/js/jssor.slider.js', true)
            ->addJs('assets/js/plugins/slick.js', true)
            ->addJs('assets/js/sp-banner.js', true)
            ->addJs('assets/js/sp-home.js', true);

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/sp-banner.css', true)
            ->addCss('assets/css/plugins/slick.css', true)
            ->addCss('assets/css/plugins/slick-theme.css', true);

        // Load banner -----------
        $banners = \BannersExt::find([
            'conditions' => "status=" . \BannersExt::STATUS_APPROVED,
            'order' => 'pos'
        ]);
        // Load list brand ----------------
        $brands = \ProductBrandsExt::find([
            'conditions' => 'status = ' . \ProductBrandsExt::STATUS_ENABLED
        ]);

        if ($this->request->get('keyword')) {
            $this->tag->setTitle('Search by vehicle: ' . $this->request->get('keyword'));
        }
        // Load list products ------------
        $products = $this->__loadProduct(true, $this->request->get('keyword') || $this->request->get('brand') ? false : true);

        $this->view->setVars([
            'banners' => $banners,
            'brands' => $brands,
            'products' => $products,
            'search' => count($this->request->get()) ? true : false,
            'feature' => $this->__loadFeature()
        ]);
    }

    protected function __loadModel($brand_id)
    {
        $models = \ProductModelsExt::find([
            'conditions' => "brand = " . $brand_id,
            'columns' => 'id, model_name AS title'
        ]);
        return \ModelHelper::toArray($models, 'id');
    }

    protected function __loadYear($model_id)
    {
        $years = \ProductYearsExt::find([
            'conditions' => "model_id = " . $model_id . " AND status=" . \ProductYearsExt::STATUS_ENABLED,
            'columns' => 'id, year, end_year'
        ]);

        $result = [];
        if (count($years)) {
            $start = date('Y');
            $end = 1970;
            foreach ($years as $k => $y) {
                if ($y->year < $start) {
                    $start = $y->year;
                }
                if ($y->end_year > $end) {
                    $end = $y->end_year;
                }
            }

            if($start < 1970) {
                $start = 1970;
            }

            for ($i = $start; $i <= $end; $i++) {
                $result[] = ['id' => $i, 'title' => $i];
            }
        }

        return $result;
    }

    /**
     * @param bool $convert
     * @param bool $is_front
     * @return array|\Phalcon\Mvc\Model\ResultsetInterface
     * @throws Exception
     */
    protected function __loadProduct($convert = true, $is_front = true)
    {
        $page_size = 20; // 5 rows
        $query_string = [];

        $condition = 'status=' . \ProductsExt::STATUS_APPROVED;

        $condition_props = '1=1';
        $brand = $this->request->get('brand');
        if (!empty($brand)) {
            $condition_props .= ' AND brand=' . $this->request->get('brand');
            $query_string[] = 'brand=' . $this->request->get('brand');
        }
        if ($this->request->get('model')) {
            $condition_props .= ' AND model=' . $this->request->get('model');
            $query_string[] = 'model=' . $this->request->get('model');
        }
        if ($this->request->get('year')) {
            $condition_props .= " AND start_year <= '{$this->request->get('year')}' AND end_year >= '{$this->request->get('year')}'";
            $query_string[] = 'year=' . $this->request->get('year');
        }

        $properties = \ProductVehicleExt::find([
            'conditions' => $condition_props,
            'group' => 'product_id'
        ]);

        if(!empty($brand)) {
            if(count($properties)) {
                foreach ($properties as $p) {
                    $product_ids[] = $p->getProductId();
                }
            } else {
                $product_ids = [0];
            }
            $condition .= " AND id IN(" . implode(',', $product_ids) . ")";
        }

        if ($this->request->get('cid')) {
            $condition .= ' AND cid=' . $this->request->get('cid');
            $query_string[] = 'cid=' . $this->request->get('cid');
        }

        if ($this->request->get('keyword')) {
            $condition .= ' AND title LIKE "%' . $this->request->get('keyword') . '%"';
            $query_string[] = 'keyword=' . $this->request->get('keyword');
        }
        if ($is_front) {
            $condition .= ' AND is_front=1';
        }

        $page = (int)$this->request->get('page') < 1 ? 1 : (int)$this->request->get('page');
        if ($this->request->get('page')) {
            $query_string[] = 'page=' . $this->request->get('page');
        }

        $products = \ProductsExt::find([
            'conditions' => $condition,
            'order' => 'pos ASC, created_time DESC',
            'limit' => array('number' => $page_size, 'offset' => ($page - 1) * $page_size)
        ]);

        // Load currencies
        $currencies = \ModelHelper::toArray(\CurrencyExt::find(), 'id');

        $result = [];
        if (count($products) > 0 & $convert) {
            $uploadUri = $this->config->application->uploadUri;
            foreach ($products as $p) {
                if (!$p instanceof \ProductsExt) {
                    continue;
                }
                $img = strpos($p->getIllustration(), 'http:') > -1 ? $p->getIllustration() :
                    (file_exists($this->config->application->uploadDir . 'products' .
                    DS . date('Y-m-d', strtotime($p->getCreatedTime())) . DS . $p->getIllustration()) & $p->getIllustration() != '' ?
                    $uploadUri . 'products/' . date('Y-m-d', strtotime($p->getCreatedTime())) . '/' . $p->getIllustration() :
                    $uploadUri . 'products/no-image.png');

                $result[] = [
                    'link' => $uploadUri . 'product/detail/' . \Phalcon\Utils\Slug::generate($p->getTitle()) . '-' . $p->getId(),
                    'src' => $img,
                    'title' => \Phalcon\Utils\StringHelper::strip_word($p->getTitle(), 10),
                    'price' => (isset($currencies[$p->getCurrencyId()]) ? $currencies[$p->getCurrencyId()]['abbreviation'] : '$') . $p->getPrice(),
                    'sale_off' => $p->getSaleOff() & (strtotime($p->getSaleOffTo()) > time()) ?
                        ($currencies[$p->getCurrencyId()]['abbreviation'].$p->getSaleOff()) : 0
                ];
            }
        }

        // Set total page for view
        $total = \ProductsExt::count($condition);
        $pages = [];
        for ($i = 1; $i <= ceil($total / $page_size); $i++) {
            $pages[$i] = $i;
        }
        if (empty($pages)) {
            $pages['1'] = 1;
        }

        if ($this->request->isAjax()) {

        } else {
            $this->view->setVars([
                'pages' => $pages,
                'query_string' => implode('&', $query_string)
            ]);
        }

        return $convert ? ['data' => $result, 'pages' => $pages] : $products;

    }

    private function __loadFeature() {
        $models = \ProductsExt::find([
            'conditions' => "is_feature = 1",
            'order' => 'modified_time DESC',
            'limit' => 6
        ]);

        return $models;
    }

    public function page404Action()
    {
        $this->view->disable();
        echo '<h1>Page not found</h1>';
    }

}

