<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 4/9/15
 * Time: 8:21 AM
 */

namespace Application\Controllers\Frontend;

class ProductController extends ControllerBase
{

    /**
     * @param string $slug
     */
    public function indexAction($slug = '')
    {

        $this->assets->collection('libraryJs')
            ->addJs('assets/js/sp-home.js', true)
            ->addJs('assets/js/plugins/slick.js', true);

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/plugins/slick.css', true)
            ->addCss('assets/css/plugins/slick-theme.css', true);

        $cat = \ProductCategoriesExt::findFirst([
            'conditions' => "slug = '{$slug}'"
        ]);

        // Load list brand ----------------
        $brands = \ProductBrandsExt::find([
            'conditions' => 'status = ' . \ProductBrandsExt::STATUS_ENABLED
        ]);

        $products = [];
        $seo = new \SeoExt();
        if ($cat instanceof \ProductCategoriesExt) {

            $this->tag->setTitle($cat->getName());

            // Seo
            $seo = \SeoExt::findFirst("type=" . \SeoExt::TYPE_CATEGORY . " AND object_id={$cat->getId()}");

            if($seo instanceof \SeoExt) {
                $this->view->setVars([
                    'meta_keyword' => $seo->getKeyword(),
                    'meta_description' => $seo->getDescription()
                ]);
            } // -----------

            // Find product in cat
            $products = \ProductsExt::find([
                'conditions' => "cid={$cat->getId()}"
            ]);
        }

        $this->view->setVars([
            'category' => ($cat instanceof \ProductCategoriesExt ? $cat : new \ProductCategoriesExt()),
            'brands' => $brands,
            'products' => $products,
            'meta_keyword' => $seo->getKeyword(),
            'meta_description' => $seo->getDescription()
        ]);
    }

    /**
     * @param string $slug
     */
    public function detailAction($slug = '')
    {
        $this->assets->collection('libraryJs')
            ->addJs('assets/js/sp-home.js', true)
            ->addJs('assets/js/plugins/slick.js', true);

        $this->assets->collection('frontendCss')
            ->addCss('assets/css/plugins/slick.css', true)
            ->addCss('assets/css/plugins/slick-theme.css', true)
            ->addCss('assets/css/sp-detail.css', true);

        $slug = preg_split('/-/', $slug);
        $id = count($slug) ? $slug[count($slug) - 1] : 0;

        // Load list brand ----------------
        $brands = \ProductBrandsExt::find([
            'conditions' => 'status = ' . \ProductBrandsExt::STATUS_ENABLED
        ]);

        $product = \ProductsExt::findFirst($id);
        if($product instanceof \ProductsExt) {
            $category = \ProductCategoriesExt::findFirst($product->getCid() ? $product->getCid():0);
        } else {
            $product = new \ProductsExt();
        }

        // Load seo
        $seo = \SeoExt::findFirst([
            'conditions' => "type=".\SeoExt::TYPE_PRODUCT." AND object_id={$id}"
        ]);
        if($seo instanceof \SeoExt) {
            $this->tag->setTitle($seo->getTitle() ? $seo->getTitle() : $product->getTitle());
        } else {
            $seo = new \SeoExt();
            $this->tag->setTitle($product->getTitle());
        }

        $other_products = [];
        if($category) {
            $other_products = \ModelHelper::toArray(\ProductsExt::find([
                'conditions' => 'cid=' . $category->getId(),
                'limit' => [
                    'number' =>10,
                    'offset' => 0
                ],
                'order' => 'pos ASC, created_time DESC'
            ]));
        }

        $illustration = strpos($product->getIllustration(), 'http:') > -1 ? $product->getIllustration() : (file_exists($this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'products' .
            DS . date('Y-m-d', strtotime($product->getCreatedTime())) . DS . $product->getIllustration()) ?
            $this->config->application->baseUri . 'uploads/products/' . date('Y-m-d', strtotime($product->getCreatedTime())) . '/' . $product->getIllustration() :
            $this->config->application->baseUri . 'uploads/products/no-image.png');

        $sale_off = $product->getSaleOff() & strtotime($product->getSaleOffTo()) > time()? true : false;
        $currency = \CurrencyExt::findFirst($product->getCurrencyId());

        $this->view->setVars([
            'category' => $category ? $category : new \ProductCategoriesExt(),
            'product' => $product,
            'brands' => $brands,
            'illustration' => $illustration,
            'sale_off' => $sale_off,
            'currency' => $currency instanceof \CurrencyExt ? $currency->getSymbol() : '',
            'other_products'=> $other_products,
            'meta_keyword' => $seo->getKeyword(),
            'meta_description' => $seo->getDescription()
        ]);
    }
}