<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class YearController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList()
    {
        $arr_fields = array(
            'id',
            'year',
            'end_year',
            'total_items',
            'brand_id',
            'model_id',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND year LIKE '%{$key}%'";
        }

        // Request private data
        $brand = $this->request->get('brand');
        $models = $this->request->get('model');
        if (!empty($brand)) {
            $condition .= " AND brand_id=" . $brand;
        }
        if (!empty($models)) {
            $condition .= " AND model_id=" . $models;
        }

        $total = \ProductYearsExt::count(array('conditions' => $condition));

        $years = \ProductYearsExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $brands = \ModelHelper::toArray(\ProductBrandsExt::find(), 'id');
        $models = \ModelHelper::toArray(\ProductModelsExt::find(), 'id');

        $result = array();
        $status = \ProductYearsExt::statusArr();
        foreach ($years as $key => $m) {
            if ($m instanceof \ProductYears) {
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getYear();
                $result[$key][] = $m->getEndYear();
                $result[$key][] = $m->getTotalItems() ? $m->getTotalItems() :0;
                $result[$key][] = isset($brands[$m->getBrandId()]) ? $brands[$m->getBrandId()]['brand_name'] : '';
                $result[$key][] = isset($models[$m->getModelId()]) ? $models[$m->getModelId()]['model_name'] : '';
                $result[$key][] = $status[$m->getStatus()];
                $result[$key][] = "<a href='/admin/year/edit/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/year/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction()
    {

        if ($this->request->isAjax()) {
            $this->view->disable();
            switch ($this->request->get('option')) {
                case 'getModel':
                    $this->__loadModelsByBrand($this->request->get('brand'));
                    break;
                default:
                    break;
            }
            return;
        }

        $year = new \ProductYearsExt();
        if ($this->request->isPost()) {

            $year->setYear($this->request->getPost('year'));
            $year->setEndYear($this->request->getPost('end_year'));
            $year->setBrandId($this->request->getPost('brand'));
            $year->setModelId($this->request->getPost('model'));
            $year->setStatus($this->request->getPost('status'));

            if ($year->save()) {
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create year success!'));
                $this->response->redirect('admin/year/index');
            } else {
                $msg = [];
                foreach ($year->getMessages() as $message) {
                    $msg[] = ' - ' . $message->getMessage();
                }
                $this->flashSession->error($this->_getTranslation()->_('Create year property error!') . '<br/>' . implode('<br/>', $msg));
            }

        }

        $brands = \ProductBrands::find();
        $models = \ProductModelsExt::find([
            'conditions' => "brand=" . ($year->getBrandId() ? $year->getBrandId() : 0)
        ]);

        $this->view->setVars([
            'year' => $year,
            'req' => $this->request,
            'brands' => $brands,
            'models' => $models,
            'status' => \ProductYearsExt::statusArr()
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0)
    {

        if ($this->request->isAjax()) {
            $this->view->disable();
            switch ($this->request->get('option')) {
                case 'getModel':
                    $this->__loadModelsByBrand($this->request->get('brand'));
                    break;
                default:
                    break;
            }
            return;
        }

        $year = \ProductYearsExt::findFirst($id);
        if ($year instanceof \ProductYears) {
            if ($this->request->isPost()) {

                $year->setYear($this->request->getPost('year'));
                $year->setEndYear($this->request->getPost('end_year'));
                $year->setBrandId($this->request->getPost('brand'));
                $year->setModelId($this->request->getPost('model'));
                $year->setStatus($this->request->getPost('status'));

                if ($year->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Update year success!'));
                    $this->response->redirect('admin/year/index');
                } else {
                    $msg = [];
                    foreach ($year->getMessages() as $message) {
                        $msg[] = ' - ' . $message->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Update error!') . '<br/>' . implode('<br/>', $msg));
                }

            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Year selected not found!'));
            $year = new \ProductYearsExt();
        }

        $brands = \ProductBrands::find();
        $models = \ProductModelsExt::find([
            'condition' => "brand=" . $year->getBrandId()
        ]);

        $this->view->setVars([
            'year' => $year,
            'req' => $this->request,
            'brands' => $brands,
            'models' => $models,
            'status' => \ProductYearsExt::statusArr()
        ]);
    }

    private function __loadModelsByBrand($brand_id = 0)
    {
        $models = \ProductModelsExt::find([
            'conditions' => 'brand=' . $brand_id
        ]);

        $result = [];
        if (count($models)) {
            foreach ($models as $m) {
                $result[$m->getId()] = $m->getModelName();
            }
        }

        echo json_encode($result);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0)
    {
        $this->view->disable();
        // Option to display flash message
        $year = \ProductYearsExt::findFirst($id);
        if ($year instanceof \ProductYearsExt) {
            if (!$year->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/year/index');
    }
}