<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class ModelController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');

        // Load list brand
        $brands = \ProductBrandsExt::find();

        $this->view->setVar('brands', $brands);
    }

    private function __loadList()
    {
        $arr_fields = array(
            'id',
            'model_name',
            'alias',
            '',
            'total_items',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND model_name LIKE '%{$key}%'";
        }

        // Filter by private data
        $brand = $this->request->get('brand');
        if ($brand) {
            $condition .= " AND brand = " . $brand;
        }

        $total = \ProductModelsExt::count(array('conditions' => $condition));

        $models = \ProductModelsExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $brands = \ModelHelper::toArray(\ProductBrandsExt::find(), 'id');

        $result = array();
        $status = \ProductModelsExt::statusArr();

        foreach ($models as $key => $m) {
            if ($m instanceof \ProductModels) {
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getModelName();
                $result[$key][] = $m->getAlias();
                $result[$key][] = $m->getTotalItems() ? $m->getTotalItems() : 0;
                $result[$key][] = isset($brands[$m->getBrand()]) ? $brands[$m->getBrand()]['brand_name'] : $m->getBrand();
                $result[$key][] = $status[$m->getStatus()];
                $result[$key][] = "<a href='/admin/model/edit/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/model/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction()
    {
        $model = new \ProductModelsExt();

        $brands= \ProductBrandsExt::find();
        $status = \ProductModelsExt::statusArr();

        if ($this->request->isPost()) {

            $model->setModelName($this->request->getPost('model'));
            $model->setAlias($this->request->getPost('alias'));
            $model->setBrand($this->request->getPost('brand'));
            $model->setStatus($this->request->getPost('status'));

            if ($model->save()) {
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create model success!'));
                $this->response->redirect('admin/model/index');
            } else {
                $msg = [];
                foreach ($model->getMessages() as $message) {
                    $msg[] = ' - ' . $message->getMessage();
                }
                $this->flashSession->error($this->_getTranslation()->_('Create error!') . '<br/>' . implode('<br/>', $msg));
            }
        }

        $this->view->setVars([
            'model' => $model, 'brands' => $brands, 'req' => $this->request, 'status' => $status
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0)
    {
        $model = \ProductModelsExt::findFirst($id);

        $brands= \ProductBrandsExt::find();
        $status = \ProductModelsExt::statusArr();

        if ($model instanceof \ProductModels) {
            if ($this->request->isPost()) {

                $model->setModelName($this->request->getPost('model'));
                $model->setAlias($this->request->getPost('alias'));
                $model->setBrand($this->request->getPost('brand'));
                $model->setStatus($this->request->getPost('status'));

                if ($model->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Update model success!'));
                    $this->response->redirect('admin/model/index');
                } else {
                    $msg = [];
                    foreach ($model->getMessages() as $message) {
                        $msg[] = ' - ' . $message->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Update error!') . '<br/>' . implode('<br/>', $msg));
                }

            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Model selected not found!'));
            $model = new \ProductModelsExt();
        }

        $this->view->setVars([
            'model' => $model, 'brands' => $brands, 'req' => $this->request, 'status' => $status
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0)
    {
        $this->view->disable();
        // Option to display flash message
        $model = \ProductModelsExt::findFirst($id);
        if ($model instanceof \ProductModels) {
            if (!$model->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/model/index');
    }
}