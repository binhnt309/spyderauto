<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class ArticleController extends ControllerBase
{

    public function indexAction()
    {

        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList()
    {

        $arr_fields = array(
            '',
            'id',
            'title',
            'thumb',
            'cid',
            'created_at',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND title LIKE '%{$key}%'";
        }

        $total = \ArticlesExt::count(array('conditions' => $condition));

        $articles = \ArticlesExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));
        $categories = \ModelHelper::toArray(\ArtCategoriesExt::find([
            'columns' => 'id,name'
        ]), 'id');

        $result = array();
        $status = \ArticlesExt::statusArr();
        foreach ($articles as $key => $m) {
            if ($m instanceof \ArticlesExt) {
                $dir = $this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'articles' .DS. date('Y-m-d', strtotime($m->getCreatedAt())) . DS;
                if (file_exists($dir . $m->getThumb()) & $m->getThumb() != null) {
                    $thumb = '/uploads/articles/' . date('Y-m-d', strtotime($m->getCreatedAt())) . '/' . $m->getThumb();
                } else {
                    $thumb = '/uploads/articles/default.png';
                }

                $result[$key][] = "<input type='checkbox' value='{$m->getId()}' class='checkbox'/>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getTitle();
                $result[$key][] = "<img src='{$thumb}'/>";
                $result[$key][] = isset($categories[$m->getCid()]) ? $categories[$m->getCid()]['name'] : '---';
                $result[$key][] = $m->getCreatedAt();
                $result[$key][] = $status[$m->getStatus()];
                $result[$key][] = "<a href='/admin/article/edit/{$m->getId()}'>" . $this->_getTranslation()->_('Edit') . "</a>
                                        | <a onclick='__delete(event)' class='delete' href='/admin/article/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction()
    {
        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/tinymce/tinymce.min.js');

        $article = new \ArticlesExt();

        if ($this->request->isPost()) {
            $article->setTitle($this->request->getPost('title'));
            $article->setCid($this->request->getPost('cid'));
            $article->setBrief($this->request->getPost('brief'));
            $article->setContent($this->request->getPost('content'));
            $article->setTop($this->request->getPost('top') ? 1 : 0);
            $article->setBottom($this->request->getPost('bottom') ? 1 : 0);
            $article->setTag($this->request->getPost('tag'));
            $article->setPos($this->request->getPost('pos'));
            $article->setStatus($this->request->getPost('status'));

            // Upload file
            require_once $this->config->application->helper . DIRECTORY_SEPARATOR . 'FileUpload.php';
            $upload_dir = $this->config->application->uploadDir . DS . 'articles' . DS . date('Y-m-d', time()) . DS;
            // Check upload product dir exist
            if (!is_dir($upload_dir)) {
                $old_mask = umask(0);
                mkdir($upload_dir, 0777);
                umask($old_mask);
            } // ------

            $valid_extensions = array('png', 'jpg', 'gif', 'jpeg', 'JPEG');
            $upload = new \FileUpload('abbreviation');
            $result = $upload->handleUpload($upload_dir, $valid_extensions);

            $uploaded = false;
            if ($result) {
                $uploaded = true;
                $article->setThumb($upload->getFileName());
            } else if (@$_FILES['abbreviation']['name'] != '') {
                $this->flashSession->error($this->_getTranslation()->_($upload->getErrorMsg()));
            } else {
                $uploaded = true;
            }
            if ($uploaded) {
                if ($article->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Create article success'));
                    $this->response->redirect('admin/article/index');
                } else {
                    $msg = '';
                    foreach ($article->getMessages() as $m) {
                        $msg .= '<br/> - ' . $m->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Create article error') . $msg);
                }
            }
        }

        $categories = \ArtCategoriesExt::find()->toArray();
        $categories = \ModelHelper::loadCategory($categories, 0);
        $category_options = \ModelHelper::rendHtmlOption($categories, 1, '---');

        $this->view->setVars([
            'article' => $article,
            'req' => $this->request,
            'status' => \ArtCategoriesExt::statusArr(),
            'category_options' => $category_options,
            'abbreviation' => '/uploads/articles/default.png'
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0)
    {
        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/tinymce/tinymce.min.js');

        $article = \ArticlesExt::findFirst($id);

        if ($article instanceof \ArticlesExt) {
            if ($this->request->isPost()) {

                $article->setTitle($this->request->getPost('title'));
                $article->setCid($this->request->getPost('cid'));
                $article->setBrief($this->request->getPost('brief'));
                $article->setContent($this->request->getPost('content'));
                $article->setTag($this->request->getPost('tag'));
                $article->setTop($this->request->getPost('top') ? 1 : 0);
                $article->setBottom($this->request->getPost('bottom') ? 1 : 0);
                $article->setPos($this->request->getPost('pos'));
                $article->setStatus($this->request->getPost('status'));

                // Upload file
                require_once $this->config->application->helper . DIRECTORY_SEPARATOR . 'FileUpload.php';
                $upload_dir = $this->config->application->uploadDir . DS . 'articles' .
                    DS . date('Y-m-d', strtotime($article->getCreatedAt())) . DS;
                // Check upload product dir exist
                if (!is_dir($upload_dir)) {
                    $old_mask = umask(0);
                    mkdir($upload_dir, 0777);
                    umask($old_mask);
                } // ------

                $valid_extensions = array('png', 'jpg', 'gif', 'jpeg', 'JPEG');
                $upload = new \FileUpload('abbreviation');
                $result = $upload->handleUpload($upload_dir, $valid_extensions);

                $uploaded = false;
                if ($result) {
                    $uploaded = true;
                    $article->setThumb($upload->getFileName());
                } else if (@$_FILES['abbreviation']['name'] != '') {
                    $this->flashSession->error($this->_getTranslation()->_($upload->getErrorMsg()));
                } else {
                    $uploaded = true;
                }
                if ($uploaded) {
                    if ($article->save()) {
                        $this->view->disable();
                        $this->flashSession->success($this->_getTranslation()->_('Update article success'));
                        $this->response->redirect('admin/article/index');
                    } else {
                        $msg = '';
                        foreach ($article->getMessages() as $m) {
                            $msg .= '<br/> - ' . $m->getMessage();
                        }
                        $this->flashSession->error($this->_getTranslation()->_('Update article error') . $msg);
                    }
                }
            }
        } else {
            $article = new \ArticlesExt();
            $this->flashSession->warning($this->_getTranslation()->_('Can not find this article'));
        }

        $categories = \ArtCategoriesExt::find()->toArray();
        $categories = \ModelHelper::loadCategory($categories, 0);
        $category_options = \ModelHelper::rendHtmlOption($categories, 1, '---', $article->getCid());
        $abbreviation = file_exists($this->config->application->baseDir . 'public' . DS . 'uploads' . DS
            . 'articles' . DS . date('Y-m-d', strtotime($article->getCreatedAt())) . DS . $article->getThumb()) & $article->getThumb() != null ?
            '/uploads/articles/' . date('Y-m-d', strtotime($article->getCreatedAt())) . '/' . $article->getThumb()
            :
            '/uploads/articles/default.png';

        $this->view->setVars([
            'article' => $article,
            'req' => $this->request,
            'status' => \ArtCategoriesExt::statusArr(),
            'category_options' => $category_options,
            'abbreviation' => $abbreviation
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0)
    {

    }
}