<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class CategoryController extends ControllerBase {

    public function indexAction() {

        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');

    }

    private function __loadList()
    {
        $arr_fields = array(
            'id',
            'name',
            'total',
            'parent_id',
            'des',
            'pos',
            'created_time',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND name LIKE '%{$key}%' OR des LIKE '%{$key}%'";
        }

        // Filter by private data
        $cid = $this->request->get('cid');
        if ($cid) {
            $condition .= " AND parent_id = " . $cid;
        } // -----------

        $total = \ProductCategoriesExt::count(array('conditions' => $condition));

        $categories = \ProductCategoriesExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $cats = \ModelHelper::toArray(\ProductCategoriesExt::find([
            'columns'=> 'id, name'
        ]), 'id');

        $result = array();
        $status = \ProductCategoriesExt::statusArr();

        foreach ($categories as $key => $m) {
            if ($m instanceof \ProductCategories) {
                $result[$key][] = "<input type='checkbox' value='{$m->getId()}' class='checkbox'/>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getName();
                $result[$key][] = $m->getTotal() ? $m->getTotal() : 0;
                $result[$key][] = isset($cats[$m->getParentId()]) ? $cats[$m->getParentId()]['name'] : '{ ROOT }';
                $result[$key][] = $m->getPos() ? $m->getPos() : 0;
                $result[$key][] = $m->getCreatedTime();
                $result[$key][] = $status[$m->getStatus()];
                $result[$key][] = "<a href='/admin/category/edit/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/category/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction() {

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/tinymce/tinymce.min.js');

        $category = new \ProductCategoriesExt();

        if ($this->request->isPost()) {

            $category->setName($this->request->getPost('name'));
            $category->setSlug($this->request->getPost('slug'));
            $category->setParentId($this->request->getPost('parent_id'));
            $category->setPos($this->request->getPost('pos'));
            $category->setDes($this->request->getPost('des'));
            $category->setStatus($this->request->getPost('status'));

            // Upload file
            require_once $this->config->application->helper . DIRECTORY_SEPARATOR . 'FileUpload.php';
            $upload_dir = $this->config->application->uploadDir . DS . 'products' . DS . date('Y-m-d', time()) . DS;
            // Check upload product dir exist
            if (!is_dir($upload_dir)) {
                $old_mask = umask(0);
                mkdir($upload_dir, 0777);
                umask($old_mask);
            } // ------

            $valid_extensions = array('png', 'jpg', 'gif', 'jpeg', 'JPEG');
            $upload = new \FileUpload('thumb');
            $result = $upload->handleUpload($upload_dir, $valid_extensions);

            $uploaded = false;
            if ($result) {
                $uploaded = true;
                $category->setThumb($upload->getFileName());
            } else if (@$_FILES['thumb']['name'] != '' & $_FILES['thumb']['size'] != 0) {
                // Has file & upload false
                $this->flashSession->error($this->_getTranslation()->_($upload->getErrorMsg()));
            } else {
                $uploaded = true;
            }

            if($uploaded) {
                if ($category->save()) {
                    $this->view->disable();

                    $seo = new \SeoExt();
                    $seo->setType(\SeoExt::TYPE_CATEGORY);
                    $seo->setObjectId($category->getId());
                    $seo->setKeyword($this->request->getPost('keyword'));
                    $seo->setDescription($this->request->getPost('description'));
                    $seo->save();

                    $this->flashSession->success($this->_getTranslation()->_('Create category success!'));
                    $this->response->redirect('admin/category/index');
                } else {
                    $msg = [];
                    foreach ($category->getMessages() as $message) {
                        $msg[] = ' - ' . $message->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Create error!') . '<br/>' . implode('<br/>', $msg));
                }
            }

        }

        $categories = \ProductCategoriesExt::find([
            'conditions' => 'status=' .\ProductCategoriesExt::STATUS_APPROVED,
            'order' => 'parent_id, name'
        ])->toArray();
        $category_options = \ModelHelper::rendHtmlOption(\ModelHelper::loadCategory($categories, 0), 1, '---');

        $this->view->setVars([
            'category' => $category,
            'category_options' => $category_options,
            'req' => $this->request,
            'status' => \ProductCategoriesExt::statusArr(),
            'seo' => new \SeoExt(),
            'thumb' => $this->config->application->baseUri . 'uploads/products/no-image.png'
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0) {

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/tinymce/tinymce.min.js');

        $category = \ProductCategoriesExt::findFirst($id);
        $thumb = $this->config->application->baseUri . 'uploads/products/no-image.png';

        if ($category instanceof \ProductCategories) {

            $upload_dir = $this->config->application->uploadDir . DS . 'products' .
                DS . date('Y-m-d', strtotime($category->getCreatedTime())) . DS;

            if(file_exists($upload_dir . DS . $category->getThumb()) & $category->getThumb() != null) {
                $thumb = $this->config->application->baseUri . 'uploads/products/' .
                    date('Y-m-d', strtotime($category->getCreatedTime())) . '/' . $category->getThumb();
            }

            $seo = \SeoExt::findFirst([
                'conditions' => 'type='.\SeoExt::TYPE_CATEGORY.' AND object_id = ' . $category->getId()
            ]);
            if (!$seo) {
                $seo = new \SeoExt();
            }

            /**
             * Is Post
             */
            if ($this->request->isPost()) {

                $category->setName($this->request->getPost('name'));
                $category->setSlug($this->request->getPost('slug'));
                $category->setParentId($this->request->getPost('parent_id'));
                $category->setPos($this->request->getPost('pos'));
                $category->setDes($this->request->getPost('des'));
                $category->setStatus($this->request->getPost('status'));

                // Upload file
                require_once $this->config->application->helper . DIRECTORY_SEPARATOR . 'FileUpload.php';

                // Check upload product dir exist
                if (!is_dir($upload_dir)) {
                    $old_mask = umask(0);
                    mkdir($upload_dir, 0777);
                    umask($old_mask);
                } // ------

                $valid_extensions = array('png', 'jpg', 'gif', 'jpeg', 'JPEG');
                $upload = new \FileUpload('thumb');
                $result = $upload->handleUpload($upload_dir, $valid_extensions);

                $uploaded = false;
                if ($result) {
                    $uploaded = true;
                    $category->setThumb($upload->getFileName());
                } else if (@$_FILES['thumb']['name'] != '' & $_FILES['thumb']['size'] != 0) {
                    // Has file & upload false
                    $this->flashSession->error($this->_getTranslation()->_($upload->getErrorMsg()));
                } else {
                    $uploaded = true;
                }

                if($uploaded) {
                    if ($category->save()) {
                        $this->view->disable();

                        $seo->setType(\SeoExt::TYPE_CATEGORY);
                        $seo->setObjectId($category->getId());
                        $seo->setKeyword($this->request->getPost('keyword'));
                        $seo->setDescription($this->request->getPost('description'));
                        $seo->save();

                        $this->flashSession->success($this->_getTranslation()->_('Update category success!'));
                        $this->response->redirect('admin/category/index');
                    } else {
                        $msg = [];
                        foreach ($category->getMessages() as $message) {
                            $msg[] = ' - ' . $message->getMessage();
                        }
                        $this->flashSession->error($this->_getTranslation()->_('Update error!') . '<br/>' . implode('<br/>', $msg));
                    }
                }

            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Model selected not found!'));
            $category = new \ProductModelsExt();
            $seo = new \SeoExt();
        }

        $categories = \ProductCategoriesExt::find([
            'conditions' => 'id != ' . $id. ' AND status=' .\ProductCategoriesExt::STATUS_APPROVED,
            'order' => 'parent_id, name',
            'columns' => 'id, parent_id, name'
        ])->toArray();

        $category_options = \ModelHelper::rendHtmlOption(\ModelHelper::loadCategory($categories, 0), 1, '---');

        $this->view->setVars([
            'category' => $category,
            'category_options' => $category_options,
            'req' => $this->request,
            'status' => \ProductCategoriesExt::statusArr(),
            'seo' => $seo,
            'thumb' => $thumb
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0) {
        $this->view->disable();
        // Option to display flash message
        $cate = \ProductCategoriesExt::findFirst($id);
        if ($cate instanceof \ProductCategories) {
            if (!$cate->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/category/index');
    }
}