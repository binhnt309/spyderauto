<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/24/15
 * Time: 3:13 PM
 */

namespace Application\Controllers\Backend;

class CurrencyController extends ControllerBase
{

    /**
     *  Load List
     */
    public function indexAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList()
    {

        $arr_fields = array(
            '',
            'id',
            'name',
            'abbreviation',
            'symbol',
            'country_id'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND name LIKE '%{$key}%'";
        }

        $total = \CurrencyExt::count(array('conditions' => $condition));

        $currencies = \CurrencyExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));
        $result = array();
        $countries = \ModelHelper::toArray(\LocationsExt::find([
            'conditions' => "type=" . \LocationsExt::TYPE_COUNTRY
        ]), 'id');

        foreach ($currencies as $key => $m) {
            if ($m instanceof \Currencies) {
                $result[$key][] = "<input type='checkbox' value='{$m->getId()}' class='checkbox'/>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getName();
                $result[$key][] = $m->getAbbreviation();
                $result[$key][] = $m->getSymbol();
                $result[$key][] = isset($countries[$m->getCountryId()]) ? $countries[$m->getCountryId()]['name'] : '';
                $result[$key][] = "<a href='/admin/currency/edit/{$m->getId()}'>" . $this->_getTranslation()->_('Edit') . "</a>
                                        | <a onclick='__delete(event)' class='delete' href='/admin/currency/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction()
    {
        $currency = new \CurrencyExt();
        if ($this->request->isPost()) {

            $currency->setName($this->request->getPost('name'));
            $currency->setAbbreviation($this->request->getPost('abbreviation'));
            $currency->setSymbol($this->request->getPost('symbol'));
            $currency->setCountryId($this->request->getPost('country'));

            if ($currency->save()) {
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create currency success!'));
                $this->response->redirect('admin/currency/index');
            } else {
                $msg = [];
                foreach ($currency->getMessages() as $message) {
                    $msg[] = ' - ' . $message->getMessage();
                }
                $this->flashSession->error($this->_getTranslation()->_('Create currency error!') . '<br/>' . implode('<br/>', $msg));
            }

        }

        $countries = \LocationsExt::find([
            'conditions' => 'type="' . \LocationsExt::TYPE_COUNTRY . '"',
            'order' => 'name'
        ]);

        $this->view->setVars([
            'currency' => $currency,
            'req' => $this->request,
            'countries' => $countries
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0)
    {
        $currency = \CurrencyExt::findFirst($id);

        if ($currency instanceof \CurrencyExt) {
            if ($this->request->isPost()) {

                $currency->setName($this->request->getPost('name'));
                $currency->setAbbreviation($this->request->getPost('abbreviation'));
                $currency->setSymbol($this->request->getPost('symbol'));
                $currency->setCountryId($this->request->getPost('country'));

                if ($currency->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Update currency success!'));
                    $this->response->redirect('admin/currency/index');
                } else {
                    $msg = [];
                    foreach ($currency->getMessages() as $message) {
                        $msg[] = ' - ' . $message->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Update currency error!') . '<br/>' . implode('<br/>', $msg));
                }

            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Currency not found!'));
            $currency = new \CurrencyExt();
        }

        $countries = \LocationsExt::find([
            'conditions' => 'type="' . \LocationsExt::TYPE_COUNTRY . '"',
            'order' => 'name'
        ]);

        $this->view->setVars([
            'currency' => $currency,
            'req' => $this->request,
            'countries' => $countries
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0)
    {
        $this->view->disable();

        if ($this->request->isPost()) {
            // Delete by list
            $ids = $this->request->getPost('ids');
            if ($ids) {
                $sql = "DELETE FROM " . \CurrencyExt::getTable() . " WHERE id IN ({$ids})";
                if (!$this->db->query($sql)) {
                    $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
                } else {
                    $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
                }
                $this->response->redirect('admin/currency/index');
            }

            return;
        }

        // Option to display flash message
        $currency = \CurrencyExt::findFirst($id);
        if ($currency instanceof \CurrencyExt) {
            if (!$currency->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/currency/index');
    }
}