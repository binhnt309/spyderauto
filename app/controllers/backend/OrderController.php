<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class OrderController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList()
    {
        $arr_fields = array(
            '',
            'id',
            'customer_name', // email
            'customer_mobile', // phone
            //'customer_address',
            'country_label',
            'total_items',
            'amount',
            'fee_amount',
            'created_time',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND year LIKE '%{$key}%'";
        }

        $total = \OrdersExt::count(array('conditions' => $condition));

        $orders = \OrdersExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $result = array();
        $status = \OrdersExt::statusArr();
        foreach ($orders as $key => $m) {
            if ($m instanceof \OrdersExt) {
                $result[$key][] = "<input type='checkbox' value='{$m->getId()}'/>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getCustomerName() .'<br/><i style="font-size: 12px;">' . $m->getCustomerEmail() . '</i>';
                $result[$key][] = $m->getCustomerMobile() . ' <br/> ' . $m->getCustomerPhone();
                //$result[$key][] = $m->getCustomerAddress();
                $result[$key][] = $m->getCountryLabel();
                $result[$key][] = $m->getTotalItems();
                $result[$key][] = $m->getAmount();
                $result[$key][] = $m->getFeeAmount();
                $result[$key][] = $m->getCreatedAt();
                $result[$key][] = isset($status[$m->getStatus()]) ? $status[$m->getStatus()] : $status[\OrdersExt::STATUS_PAID];
                $result[$key][] = "<a href='/admin/order/view/{$m->getId()}'>". $this->_getTranslation()->_('View') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction()
    {

        if ($this->request->isAjax()) {
            $this->view->disable();
            switch ($this->request->get('option')) {
                case 'getModel':
                    $this->__loadModelsByBrand($this->request->get('brand'));
                    break;
                default:
                    break;
            }
            return;
        }

        $year = new \ProductYearsExt();
        if ($this->request->isPost()) {

            $year->setYear($this->request->getPost('year'));
            $year->setBrandId($this->request->getPost('brand'));
            $year->setModelId($this->request->getPost('model'));
            $year->setStatus($this->request->getPost('status'));

            if ($year->save()) {
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create year success!'));
                $this->response->redirect('admin/year/index');
            } else {
                $msg = [];
                foreach ($year->getMessages() as $message) {
                    $msg[] = ' - ' . $message->getMessage();
                }
                $this->flashSession->error($this->_getTranslation()->_('Create year property error!') . '<br/>' . implode('<br/>', $msg));
            }

        }

        $brands = \ProductBrands::find();
        $models = \ProductModelsExt::find([
            'conditions' => "brand=" . ($year->getBrandId() ? $year->getBrandId() : 0)
        ]);

        $this->view->setVars([
            'year' => $year,
            'req' => $this->request,
            'brands' => $brands,
            'models' => $models,
            'status' => \ProductYearsExt::statusArr()
        ]);
    }

    /**
     * @param $order_id
     */
    public function viewAction($order_id) {

        $order = \OrdersExt::findFirst($order_id);
        $orderItems = \OrderItemsExt::find([
            'conditions' => "order_id=$order_id"
        ]);

        $this->view->setVars([
            'order' => $order,
            'orderItems' => $orderItems
        ]);

    }
}