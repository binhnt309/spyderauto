<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 12/15/14
 * Time: 10:02 AM
 */
namespace Application\Controllers\Backend;

class UserController extends ControllerBase
{
    /**
     * View list user
     */
    public function indexAction()
    {
        if($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');

        /*$user = \UserExt::find(array(
            'status' => \UserExt::STATUS_APPROVED,
            'order' => 'username'
        ));

        $this->view->setVars(
            array(
                'user' => $user
            )
        );*/
    }

    public function createAction()
    {
        // Check permission
        /*if($this->session->get('member')['username'] != 'admin') {
            $this->view->disable();
            $this->flashSession->warning($this->_getTranslation()->_('You have not permission execute this action!'));
            $this->response->redirect('admin/user/index');
        }*/ // ------------

        // Post create new memeber
        if ($this->request->isPost()) {
            $user = \UserExt::findFirst("username='{$this->request->getPost('email')}'");
            if ($user) { // Email has existed
                $this->flashSession->error($this->_getTranslation()->_('Email has been existed!'));
            } else {
                $user = new \UserExt();
                $user->setFullName($this->request->getPost('full_name'));
                $user->setUsername($this->request->getPost('username'));
                $user->setEmail($this->request->getPost('email'));
                $user->setPwd($this->security->hash($this->request->getPost('pwd')));
                $user->setGroup($this->request->getPost('group'));
                $user->setStatus($this->request->getPost('status'));

                if ($user->save()) {
                    $this->view->disable(); // Option to display flash message
                    $this->flashSession->success($this->_getTranslation()->_('Create success!'));
                    $this->response->redirect('admin/user/index');
                } else {
                    // Save false
                    $_msg = '';
                    if ($user->getMessages()) {
                        foreach ($user->getMessages() as $msg) {
                            $messages[$msg->getField()] = $msg->getMessage();
                            $_msg .= '<br/> - ' . $msg->getMessage();
                        }
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Create error!') . $_msg);
                }
            }
        }
        // Load list user group
        $userGroups = \UserGroupExt::find([
            'conditions' => 'status = ' . \UserGroupExt::STATUS_ENABLED
        ]);

        $this->view->setVars([
                'user' => new \UserExt(),
                'req' => $this->request,
                'userGroups' => $userGroups
            ]
        );
    }

    public function updateAction($id)
    {
        $this->editAction($id);
    }

    public function editAction($id)
    {
        // Check permission
        if($this->session->get('member')['username'] != 'admin' & $this->session->get('member')['id'] != $id) {
            $this->view->disable();
            $this->flashSession->warning($this->_getTranslation()->_('You have not permission edit this user!'));
            $this->response->redirect('admin/user/index');
        } // ------------

        $user = \UserExt::findFirst("id=$id");

        // User not found
        if (!$user) {
            $this->flashSession->warning($this->_getTranslation()->_("Can't find this member!"));
        }

        if (!$user) {
            $user = new \UserExt();
            $user->setId(-1);
        }

        if ($this->request->isPost()) {
            $user->setFullName($this->request->getPost('full_name'));
            if (!empty($this->request->getPost('pwd'))) {
                $user->setPwd($this->security->hash($this->request->getPost('pwd')));
            }
            if($user->getUsername() == 'admin') {
                $user->setGroup($this->request->getPost('group'));
                $user->setStatus($this->request->getPost('status'));
            }

            if ($user->save()) {
                // Update session logger if update them self
                if($this->session->get('member')['id'] == $user->getId()) {
                    $this->session->set('member', $user->toArray());
                } // -----------
                $this->view->disable(); // Option to display flash message
                $this->flashSession->success($this->_getTranslation()->_('Update success!'));
                $this->response->redirect('admin/user/index');
            } else {
                // Update fail
                $_msg = '';
                if ($user->getMessages()) {
                    foreach ($user->getMessages() as $msg) {
                        $messages[$msg->getField()] = $msg->getMessage();
                        $_msg .= '<br/> - ' . $msg->getMessage();
                    }
                }
                $this->flashSession->error($this->_getTranslation()->_('Update error!') . $_msg);
            }
        }

        // Load list member group
        $memberGroups = \UserGroupExt::find([
            'conditions' => 'status = ' . \UserGroupExt::STATUS_ENABLED
        ]);

        $this->view->setVars(
            ['user'=> $user, 'req'=>$this->request, 'userGroups' => $memberGroups]
        );
    }

    /**
     * Delete member
     * @param $id
     */
    public function deleteAction($id)
    {
        $this->view->disable(); // Option to display flash message
        $member = \UserExt::findFirst($id);
        if ($member) {
            // Check administrator
            if($member->uname == 'admin' & $this->session->get('member')['id'] != $member->id) {
                $this->flashSession->warning($this->_getTranslation()->_('You have not permission delete this user!'));
            } else {
                if (!$member->delete()) {
                    $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
                } else {
                    $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
                }
            } // ------------

        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/user/index');
    }

    private function __loadList()
    {
        $arr_fields = array(
            'id',
            'full_name',
            'username',
            'group',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";; //status != '" . \BaseMember::STATUS_DELETED . "'
        if (!empty($key)) {
            $condition .= " AND username LIKE '%{$key}%' OR full_name LIKE '%{$key}%'";
        }

        $total = \UserExt::count(array('conditions' => $condition));

        $member = \UserExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));
        $result = array();
        $groups = \ModelHelper::toArray(\UserGroupExt::find(), 'id');
        $user_group = isset($groups[$this->session->get('member')['group']]) ?
            $groups[$this->session->get('member')['group']]['name'] : '';

        foreach ($member as $key => $m) {
            if($m instanceof \User) {
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getFullName();
                $result[$key][] = $m->getUsername();
                $result[$key][] = isset($groups[$m->getGroup()]) ? $groups[$m->getGroup()]['name'] : '---';
                $result[$key][] = $m->getStatus();
                $result[$key][] = $m->getStatus() != \UserExt::STATUS_DELETED ?
                    (((strtolower($user_group) == 'admin' || $m->getId() == $this->session->get('member')['id']) & ($m->getUsername() != 'admin'))
                    || $this->session->get('member')['username'] == 'admin' ?
                        "<a href='/admin/member/edit/{$m->getId()}'>" . $this->_getTranslation()->_('Edit') . "</a>" .
                        ($m->getId() == $this->session->get('member')['id'] ?
                            "" : " | <a onclick='__delete(event)' class='delete' href='/admin/member/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>")
                        : "")
                    : '';
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }
}