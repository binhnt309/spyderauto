<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/9/15
 * Time: 10:47 AM
 */
namespace Application\Controllers\Backend;

class SystemController extends ControllerBase
{

    public function indexAction()
    {
        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/tinymce/tinymce.min.js');

        if ($this->request->isPost()) {

            $error = [];

            $data = $this->request->getPost();

            if(count($_FILES)) {
                foreach($_FILES as $f => $d) {
                    if(empty($d['name']) & $d['size'] == 0) {
                        continue;
                    }
                    $config = \SystemExt::findFirst([
                        'conditions' => "keyword = '{$f}'"
                    ]);
                    require_once $this->config->application->helper . DIRECTORY_SEPARATOR . 'FileUpload.php';

                    $upload_dir = $this->config->application->uploadDir . DS . 'logo' . DS;
                    $valid_extensions = array('png', 'jpg', 'gif', 'jpeg', 'JPEG');
                    $upload = new \FileUpload($config->getKeyword());
                    $result = $upload->handleUpload($upload_dir, $valid_extensions);

                    if (!$result) {
                        $error[] = $upload->getErrorMsg();
                    } else {
                        $config->setValue($upload->getFileName());
                        $config->save();
                    }
                }
            }

            foreach ($data as $key => $value) {
                if (!empty($value) || isset($_FILES[$key])) {

                    $config = \SystemExt::findFirst([
                        'conditions' => "keyword = '{$key}'"
                    ]);
                    $config->setValue($value);

                    if (!$config->save()) {
                        foreach ($config->getMessages() as $m) {
                            array_push($error, $m->getMessage());
                        }

                    }
                }
            }

            if(!empty($error)) {
                $this->flashSession->error(implode('<br/>', $error));
            }else {
                $this->flashSession->success($this->_getTranslation()->_('Update system config success'));
            }

        }
        // Load config
        $systems = \SystemExt::find([
            'order' => 'pos'
        ]);

        $this->view->setVars([
            'req' => $this->request,
            'config' => $systems
        ]);
    }

}