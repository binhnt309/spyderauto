<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class LocationController extends ControllerBase
{

    public function indexAction()
    {

        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');

    }

    private function __loadList()
    {
        $arr_fields = array(
            '',
            'id',
            'name',
            'alias',
            'parent_id',
            'type'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND name LIKE '%{$key}%' OR alias LIKE '%{$key}%'";
        }

        // Filter by private data
        $type = $this->request->get('type');
        if ($type) {
            $condition .= " AND type = " . $type;
        } // -----------

        $location_id = $this->request->get('l_id');
        if ($type) {
            $condition .= " AND id = " . $location_id . ' AND parent_id = ' . $location_id;
        } // -----------

        $total = \LocationsExt::count(array('conditions' => $condition));

        $locations = \LocationsExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $locations_arr = \ModelHelper::toArray($locations, 'id');

        $locationType = \LocationsExt::locationType();
        $result = array();

        foreach ($locations as $key => $m) {
            if ($m instanceof \LocationsExt) {
                $result[$key][] = "<input type='checkbox' value='{$m->getId()}' class='checkbox'/>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getName();
                $result[$key][] = $m->getAlias();
                $result[$key][] = isset($locations_arr[$m->getParentId()]) ? $locations_arr[$m->getParentId()]['name'] : '---';
                $result[$key][] = $locationType[$m->getType()];
                $result[$key][] = "<a href='/admin/location/edit/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/lcoation/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction()
    {
        $location = new \LocationsExt();

        if ($this->request->isPost()) {

            $location->setName($this->request->getPost('name'));
            $location->setAlias($this->request->getPost('alias'));
            $location->setParentId($this->request->getPost('parent_id'));
            $location->setType($this->request->getPost('type'));

            if ($location->save()) {
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create location success!'));
                $this->response->redirect('admin/location/index');
            } else {
                $msg = [];
                foreach ($location->getMessages() as $message) {
                    $msg[] = ' - ' . $message->getMessage();
                }
                $this->flashSession->error($this->_getTranslation()->_('Create location error!') . '<br/>' . implode('<br/>', $msg));
            }

        }

        $this->view->setVars([
            'location' => $location,
            'locations' => \LocationsExt::find([
                    'conditions' => 'id != 0'
                ]),
            'req' => $this->request,
            'type' => \LocationsExt::locationType()
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0)
    {

        $location = \LocationsExt::findFirst($id);

        if($location) {
            if ($this->request->isPost()) {

                $location->setName($this->request->getPost('name'));
                $location->setAlias($this->request->getPost('alias'));
                $location->setParentId($this->request->getPost('parent_id'));
                $location->setType($this->request->getPost('type'));

                if ($location->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Update location success!'));
                    $this->response->redirect('admin/location/index');
                } else {
                    $msg = [];
                    foreach ($location->getMessages() as $message) {
                        $msg[] = ' - ' . $message->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Update location error!') . '<br/>' . implode('<br/>', $msg));
                }

            }
        } else {
            $location = new \LocationsExt();
            $this->flashSession->warning($this->_getTranslation()->_('Location not found'));
        }

        $this->view->setVars([
            'location' => $location,
            'locations' => \LocationsExt::find([
                    'conditions' => 'id != 0'
                ]),
            'req' => $this->request,
            'type' => \LocationsExt::locationType()
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0)
    {
        $this->view->disable();
        // Option to display flash message
        $location = \LocationsExt::findFirst($id);
        if ($location instanceof \LocationsExt) {
            if (!$location->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/location/index');
    }
}