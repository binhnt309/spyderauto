<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

use Phalcon\Db;
use Phalcon\Exception;
use Phalcon\Mvc\Model\Transaction;

class CustomerController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList()
    {
        $arr_fields = array(
            '',
            'id',
            'name',
            'email',
            'phone',
            'address',
            'country',
            //'created_time',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND (name LIKE '%{$key}%' OR email LIKE '%{$key}%' OR phone LIKE '%{$key}% OR mobile LIKE '%{$key}%)";
        }

        $total = \CustomerExt::count(array('conditions' => $condition));

        $customers = \CustomerExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $countries = \LocationsExt::find([
            'conditions' => 'type="' . \LocationsExt::TYPE_COUNTRY . '"'
        ]);
        $countries_arr = \ModelHelper::toArray($countries, 'id');

        $result = array();
        $status = \CustomerExt::statusArr();
        foreach ($customers as $key => $m) {
            if ($m instanceof \CustomerExt) {
                $result[$key][] = "<input type='checkbox' class='checkbox' value='{$m->getId()}'/>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getFullname();
                $result[$key][] = $m->getEmail();
                $result[$key][] = $m->getPhone() . ' <br/> ' . $m->getMobile();
                $result[$key][] = $m->getAddress();
                $result[$key][] = isset($countries_arr[$m->getCountry()]) ? $countries_arr[$m->getCountry()]['name'] : '';
                //$result[$key][] = $m->getCreatedTime();
                $result[$key][] = isset($status[$m->getStatus()]) ? $status[$m->getStatus()] : $status[0];
                $result[$key][] = "<a href='/admin/customer/edit/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/customer/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction()
    {

        $this->assets->collection('backendJs')
            ->addJs('assets/js/helper.js');

        $customer = new \CustomerExt();
        $countries = \LocationsExt::find([
            'conditions' => 'type=' . \LocationsExt::TYPE_COUNTRY
        ]);

        if ($this->request->isPost()) {
            $customer->setName($this->request->getPost('name'));
            $customer->setEmail($this->request->getPost('email'));
            $customer->setMobile($this->request->getPost('mobile'));
            $customer->setPhone($this->request->getPost('phone'));
            $customer->setAddress($this->request->getPost('address'));
            $customer->setCountry($this->request->getPost('country'));

            $customer->setStatus($this->request->getPost('status'));

            if ($customer->save()) {
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create new customer success'));
                $this->response->redirect('admin/customer/index');
            } else {
                $msg = '';
                foreach ($customer->getMessages() as $m) {
                    $msg .= '<br/> - ' . $m->getMessage();
                }
                $this->flashSession->error($this->_getTranslation()->_('Create new customer error') . $msg);
            }
        }

        $this->view->setVars([
            'customer' => $customer,
            'req' => $this->request,
            'status' => \CustomerExt::statusArr(),
            'countries' => $countries
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0)
    {
        $this->assets->collection('backendJs')
            ->addJs('assets/js/helper.js');

        $customer = \CustomerExt::findFirst($id);
        $countries = \LocationsExt::find([
            'conditions' => 'type=' . \LocationsExt::TYPE_COUNTRY
        ]);

        if ($customer instanceof \CustomerExt) {
            if ($this->request->isPost()) {
                $customer->setName($this->request->getPost('name'));
                $customer->setEmail($this->request->getPost('email'));
                $customer->setMobile($this->request->getPost('mobile'));
                $customer->setPhone($this->request->getPost('phone'));
                $customer->setAddress($this->request->getPost('address'));
                $customer->setCountry($this->request->getPost('country'));

                $customer->setStatus($this->request->getPost('status'));

                if ($customer->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Update new customer success'));
                    $this->response->redirect('admin/customer/index');
                } else {
                    $msg = '';
                    foreach ($customer->getMessages() as $m) {
                        $msg .= '<br/> - ' . $m->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Update new customer error') . $msg);
                }
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Customer not exist'));
            $customer = new \CustomerExt();
        }


        $this->view->setVars([
            'customer' => $customer,
            'req' => $this->request,
            'status' => \CustomerExt::statusArr(),
            'countries' => $countries
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0)
    {

    }

    /**
     * View list coupon
     */
    public function promotionAction()
    {
        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');

        if ($this->request->isAjax()) {
            switch ($this->request->get('option')) {
                default:
                    $this->__loadCoupon();
                    break;
            }
            $this->view->disable();
            return;
        }
    }

    private function __loadCoupon()
    {
        $arr_fields = array(
            '',
            'id',
            'code',
            'amount',
            'init',
            'start',
            'end',
            'mail_send',
            'active'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND (code LIKE '%{$key}%' OR amount LIKE '%{$key}%')";
        }

        $total = \CouponExt::count(array('conditions' => $condition));

        $coupon = \CouponExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $result = array();
        $unit = \CouponExt::unitArr();
        foreach ($coupon as $key => $m) {
            if ($m instanceof \CouponExt) {
                $result[$key][] = "<input type='checkbox' class='checkbox' value='{$m->getId()}'/>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getCode();
                $result[$key][] = $m->getAmount();
                $result[$key][] = isset($unit[$m->getUnit()]) ? $unit[$m->getUnit()] : $unit[\CouponExt::UNIT_MONEY];
                $result[$key][] = $m->getStart();
                $result[$key][] = $m->getEnd();
                $result[$key][] = $m->getMailSend() ? $m->getMailSend() : 0;
                $result[$key][] = "<input type='checkbox' disabled class='form-control' " . ($m->getActive() ? 'checked' : '') . "/>";
                $result[$key][] = "<a href='/admin/customer/editPromotion/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a>" .
                    (strtotime($m->getEnd() > time()) ? '' : " | <a onclick='__toggleCoupon(event)' class='toggle-coupon' href='/admin/customer/toggleCoupon/{$m->getId()}'>"
                        . $this->_getTranslation()->_($m->getActive() ? 'DeActive' : 'Active') . "</a>")
                    . ' <a class="btn btn-default" href="javascript:;" onclick="openModalDialog(' . $m->getId() . ')">Send mail</a>';
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    public function createPromotionAction()
    {
        $req = $this->request;

        $this->assets->collection('backendCss')
            ->addCss('assets/css/jquery.datetimepicker.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/jquery-ui/jquery.datetimepicker.js');

        if ($req->isPost()) {
            try {
                $manager = new Transaction\Manager();
                $tran = $manager->get();
                $coupon_id = $this->__saveCoupon(new \CouponExt(), $tran);

                $users = $req->getPost('uid');
                if (!empty($users)) {
                    $this->__saveCouponBelongMember($coupon_id, $users, $tran);
                }

                $tran->commit();

                $this->flashSession->success($this->closeHtmlButton . 'Save coupon success');
                $this->response->redirect('admin/customer/promotion');

                $this->view->disable();

            } catch (Exception $e) {
                $this->flashSession->error($this->closeHtmlButton . 'Save coupon error. <br/>' . $e->getMessage());
            }

        }

        $this->view->setVars([
            'req' => $req,
            'coupon' => new \CouponExt(),
            'unit' => \CouponExt::unitArr(),
            'customer' => \CustomerExt::find([
                //'columns' => 'id, username',
                'conditions' => 'status=' . \CustomerExt::STATUS_APPROVED,
                'order' => 'username'
            ]),
            'coupon_user_selected' => []
        ]);
    }

    public function editPromotionAction($id = 0)
    {
        $req = $this->request;

        $this->assets->collection('backendCss')
            ->addCss('assets/css/jquery.datetimepicker.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/jquery-ui/jquery.datetimepicker.js');

        $coupon = \CouponExt::findFirst($id);
        if (!$coupon) {
            $this->view->disable();
            $this->flashSession->warning($this->closeHtmlButton . 'Coupon your search not found');
            $this->response->redirect('admin/customer/promotion');
            return;
        }
        if ($req->isPost()) {
            $manager = new Transaction\Manager();
            $tran = $manager->get();
            $coupon_id = $this->__saveCoupon($coupon, $tran);
            $users = $req->getPost('uid');
            if (!empty($users)) {
                $this->__saveCouponBelongMember($coupon_id, $users, $tran);
            }

            $tran->commit();

            $this->flashSession->success($this->closeHtmlButton . 'Save coupon success');
            $this->response->redirect('admin/customer/promotion');
            $this->view->disable();
        }

        // Load list user associate with this coupon
        $coupon_users = \CouponUserExt::find([
            'conditions' => 'coupon_id=' . $coupon->getId()
        ]);
        $uids = [];
        if (count($coupon_users)) {
            foreach ($coupon_users as $u) {
                $uids[] = $u->getUid();
            }
        }

        $this->view->setVars([
            'req' => $req,
            'coupon' => $coupon,
            'unit' => \CouponExt::unitArr(),
            'customer' => \CustomerExt::find([
                //'columns' => 'id, username',
                'conditions' => 'status=' . \CustomerExt::STATUS_APPROVED,
                'order' => 'username'
            ]),
            'coupon_user_selected' => $uids
        ]);
    }

    /**
     * @param int $id
     */
    public function toggleCouponAction($id = 0)
    {
        $this->view->disable();

        $coupon = \CouponExt::findFirst($id);
        if ($coupon instanceof \CouponExt) {
            if ($coupon->getEnd() >= time()) {
                $coupon->setActive($coupon->getActive() == \CouponExt::ACTIVE ? \CouponExt::DE_ACTIVE : \CouponExt::ACTIVE);
                if ($coupon->save()) {
                    $this->flashSession->success($this->closeHtmlButton .
                        ($coupon->getActive() == \CouponExt::ACTIVE ? 'Active success' : 'De-active success'));
                } else {
                    $err = [];
                    foreach ($coupon->getMessages() as $m) {
                        $err[] = $m->getMessage();
                    }
                    $this->flashSession->error($this->closeHtmlButton .
                        ($coupon->getActive() == \CouponExt::ACTIVE ? 'De-active error' : 'Active error') . implode('<br/>', $err));
                }
            } else {
                $this->flashSession->warning($this->closeHtmlButton . 'Can\'t do this action, because time invalid');
            }
        } else {
            $this->flashSession->warning($this->closeHtmlButton . 'Coupon not found');
        }

        $this->response->redirect('admin/customer/promotion');
    }

    /**
     * @param \CouponExt $coupon
     * @param Transaction $tran
     * @return int
     */
    private function __saveCoupon(\CouponExt $coupon, Transaction $tran)
    {
        $req = $this->request;
        $coupon->setTransaction($tran);

        $coupon->setCode($req->getPost('code'));
        $coupon->setAmount($req->getPost('amount'));
        $coupon->setLimitAmount($req->getPost('limit_amount') ? $req->getPost('limit_amount') : 0);
        $coupon->setUnit($req->getPost('unit'));
        $coupon->setStart($req->getPost('start'));
        $coupon->setEnd($req->getPost('end'));
        $coupon->setActive($req->getPost('active'));

        if ($coupon->save()) {
            return $coupon->getId();
        } else {
            $msg = [];
            foreach ($coupon->getMessages() as $m) {
                $msg[] = ' - ' . $m->getMessage();
            }
            $tran->rollback(implode('<br/>', $msg));
        }
    }

    /**
     * @param int $coupon_id
     * @param array $user_ids
     * @param Transaction $tran
     */
    private function __saveCouponBelongMember($coupon_id = 0, $user_ids = [], Transaction $tran)
    {
        // Load current user of coupon
        $users = \CouponUserExt::find('coupon_id=' . $coupon_id);
        $current_users = [];
        if (count($users)) {
            foreach ($users as $u) {
                $current_users[$u->getUid()] = $u->getUid();
            }
        } // --------

        foreach ($user_ids as $uid) {
            if (isset($current_users[$uid])) {
                unset($current_users[$uid]);
                continue;
            } // ------------

            $user = new \CouponUserExt();
            $user->setUid($uid);
            $user->setCouponId($coupon_id);

            if (!$user->save()) {
                $msg = [];
                foreach ($user->getMessages() as $m) {
                    $msg[] = ' - ' . $m->getMessage();
                }
                $tran->rollback(implode('<br/>', $msg));
                break;
            }

        }

        // Delete list current user have been unset
        if ($current_users) {
            $this->db->query('DELETE FROM `coupon_user` WHERE `uid` IN (' . implode(',', $current_users) . ') AND `coupon_id`=' . $coupon_id);
        }
    }

    /**
     * @param int $coupon_id
     */
    public function loadMailOfCouponAction($coupon_id = 0)
    {
        // Load list user has coupon
        $this->view->disable();
        $emails = [];
        $user_coupons = \CouponUserExt::find('coupon_id=' . $coupon_id);
        if (count($user_coupons)) {
            $uid = [];
            foreach ($user_coupons as $u) {
                $uid[] = $u->getUid();
            }
            $users = \CustomerExt::find('id IN (' . implode(',', $uid) . ')');
            if (count($users)) {
                foreach ($users as $u) {
                    $emails[] = $u->getEmail();
                }
            }
        }

        // Update times send mail
        $coupon = \CouponExt::findFirst($coupon_id);
        if($coupon instanceof \CouponExt) {
            $total_send = $coupon->getMailSend() + 1;
            $coupon->setMailSend($total_send);

            $content = json_decode($coupon->getMailContent(), true);
            $content[] = [
                'subject' => $this->request->getPost('subject'),
                'body' => $this->request->getPost('message'),
                'time' => date('Y-m-d H:i:s')
            ];
            $coupon->setMailContent(json_encode($content));

            $coupon->save();
        }

        echo json_encode($emails);
    }

    /**
     * Send email with post: subject, body message & to email
     */
    public function sendMailAction()
    {
        $this->view->disable();

        $subject = $this->request->getPost('subject');
        $message = $this->request->getPost('message');
        $to = $this->request->getPost('to');

        echo json_encode([
            'success' => \SendMail::sendMailByGoogleSsl($subject, $message, $to),
            'email' => $to
        ]);
    } // ----------------

}