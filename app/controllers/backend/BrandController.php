<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class BrandController extends ControllerBase {

    public function indexAction() {
        if($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList(){
        $arr_fields = array(
            'id',
            'brand_name',
            'total_items',
            'brief',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND brand_name LIKE '%{$key}%'";
        }

        $total = \ProductBrandsExt::count(array('conditions' => $condition));

        $brands = \ProductBrandsExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $result = array();
        $status = \ProductBrandsExt::statusArr();
        foreach ($brands as $key => $m) {
            if($m instanceof \ProductBrands) {
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getBrandName();
                $result[$key][] = $m->getTotalItems() ? $m->getTotalItems() : 0;
                $result[$key][] = $m->getBrief();
                $result[$key][] = $status[$m->getStatus()];
                $result[$key][] = "<a href='/admin/brand/edit/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/brand/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction() {
        $brand = new \ProductBrandsExt();

        if($this->request->isPost()){
            $brand->setBrandName($this->request->getPost('brand'));
            $brand->setBrief($this->request->getPost('brief'));
            $brand->setStatus($this->request->getPost('status'));

            if($brand->save()) {
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create brand success!'));
                $this->response->redirect('admin/brand/index');
            } else {
                $msg = [];
                foreach($brand->getMessages() as $message){
                    $msg[] = ' - ' . $message->getMessage();
                }
                $this->flashSession->error($this->_getTranslation()->_('Create error!') . '<br/>' . implode('<br/>', $msg));
            }
        }

        $this->view->setVars([
            'brand' => $brand, 'req' => $this->request, 'status' => \ProductBrandsExt::statusArr()
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0) {
        $brand = \ProductBrandsExt::findFirst($id);
        if($brand instanceof \ProductBrands) {
            if($this->request->isPost()) {

                $brand->setBrandName($this->request->getPost('brand'));
                $brand->setBrief($this->request->getPost('brief'));
                $brand->setStatus($this->request->getPost('status'));

                if($brand->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Update brand success!'));
                    $this->response->redirect('admin/brand/index');
                } else {
                    $msg = [];
                    foreach($brand->getMessages() as $message){
                        $msg[] = ' - ' . $message->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Update error!') . '<br/>' . implode('<br/>', $msg));
                }

            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Brand selected not found!'));
            $brand = new \ProductBrandsExt();
        }

        $this->view->setVars([
            'brand' => $brand, 'req' => $this->request, 'status' => \ProductBrandsExt::statusArr()
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0) {
        $this->view->disable();
        // Option to display flash message
        $brand = \ProductBrandsExt::findFirst($id);
        if ($brand instanceof \ProductBrands) {
            if (!$brand->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/brand/index');
    }
}