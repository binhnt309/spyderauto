<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 12/10/14
 * Time: 3:26 PM
 */
namespace Application\Controllers\Backend;

class AuthController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        // Set link for css, js
        $this->assets->collection('loginCss')
            ->addCss('assets/css/bootstrap.min.css', true)
            ->addCss('assets/font-awesome/css/font-awesome.css', true)
            ->addCss('assets/css/sb-admin.css', true)
            /*->setTargetPath('assets/css/min/be_login.css')
            ->setTargetUri('assets/css/min/be_login.css')
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Cssmin())*/;

        $this->assets->collection('loginJs')
            ->addJs('assets/js/jquery-1.10.2.js', true)/*->addJs('https://ajax.googleapis.com/ajax/libs/angularjs/1.2.27/angular.min.js', false, false)
            ->addJs('assets/js/angular-compiler.js', true)*/
        ;

        // Set layout
        $this->view->setLayout('login');
        $this->view->setVar('base_url', $this->url->getBaseUri());
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
    }

    public function loginAction()
    {

        // Check login
        if ($this->session->get('isAuth')) {
            $this->view->disable();
            $this->response->redirect('admin/');
        } else {

            \Phalcon\Tag::setTitle('Login');
            // Add js for login page
            $this->assets->collection('loginJs')->addJs('assets/js/login.js', true)
                /*->setTargetPath('assets/js/application.js')
                ->setTargetUri('assets/js/application.js')
                ->join(true)
                ->addFilter(new \Phalcon\Assets\Filters\Jsmin())*/;

            $req = $this->request;
            $this->view->setVar('errType', '');

            // Check cookie -----------------------------
            /*if ($this->cookies->get('remember')->getValue() == 1) {

                $crypt = new \Phalcon\Crypt();
                $key = 'hikzNt48$2%aRfkd';
                // Check username and password
                $username = $this->cookies->get('username');

                $pwd = $crypt->decryptBase64($this->cookies->get('pwd') ?
                    $this->cookies->get('pwd')->getValue() : 'Password incorrect or has been changed', $key);

                $approved = \BaseMember::STATUS_APPROVED;
                // Check username (or email) and password
                $member = \BaseMember::findFirst("uname='{$username}' AND status='{$approved}'");
                if ($member) {
                    if ($this->security->checkHash($pwd, $member->pwd)) {

                        $this->session->set('isAuth', true);
                        // Disable view to redirect working
                        $this->view->disable();

                        // Re-update time
                        $this->cookies->set('remember', 1, time() + 15 * 86400); // 15 days
                        $this->cookies->set('username', $username, time() + 15 * 86400); // 15 days
                        $this->cookies->set('pwd', $this->cookies->get('pwd'), time() + 15 * 86400); // 15 days

                        // check has url redirect
                        if ($req->get('returnUrl')) {
                            $this->response->redirect(base64_decode($req->get('returnUrl')));
                        } else {
                            // Default redirect to admin page
                            $this->response->redirect('admin/');
                        }
                    } else {
                        $this->flashSession->error($this->_getTranslation()->_('Password in-correct or has been changed!'));
                        $this->view->setVar('errType', 'pwd');
                    }
                } else {
                    $this->flashSession->error($this->_getTranslation()->_('Username or email does not exited!'));
                    $this->view->setVar('errType', 'uname');
                }
            }*/ //------------------------------

            // Check post data ------------------------
            if ($req->isPost()) {

                // Check remember login
                $approved = \UserExt::STATUS_APPROVED;
                $credential = $this->request->getPost('credential');
                // Check username (or email) and password
                $user = \UserExt::findFirst("(username='{$credential}' OR email='{$credential}') AND status='{$approved}'");

                if ($user) {
                    if ($this->security->checkHash($req->getPost('pwd'), $user->getPwd())) {

                        // Check remember login
                        /*if ($this->request->getPost('remember')) {
                            $crypt = new \Phalcon\Crypt();
                            $key = 'hikzNt48$2%aRfkd';
                            $pwd = $crypt->encryptBase64($this->request->getPost('pwd'), $key);

                            $this->cookies->set('remember', true, time() + 15 * 86400); // 15 days
                            $this->cookies->set('username', $this->request->getPost('email'), time() + 15 * 86400); // 15 days
                            $this->cookies->set('pwd', $pwd, time() + 15 * 86400); // 15 days

                        } else {
                            // Unset remember cookie
                            $this->cookies->set('remember', '', time() - 1);
                            $this->cookies->set('username', '', time() - 1);
                            $this->cookies->set('pwd', '', time() - 1);
                        }*/

                        //$this->session->set('member', $member);
                        $this->session->set('member', ($user->toArray()));
                        $this->session->set('isAuth', true);

                        // check has url redirect
                        if ($req->get('returnUrl')) {
                            $this->response->redirect(base64_decode($this->request->get('returnUrl')));
                        } else {
                            // Default redirect to admin page
                            $this->response->redirect('admin/');
                        }
                    } else {
                        $this->flashSession->error($this->_getTranslation()->_('Password in-correct!'));
                        $this->view->setVar('errType', 'pwd');
                    }
                } else {
                    $this->flashSession->error($this->_getTranslation()->_('Username or email does not exited!'));
                    $this->view->setVar('errType', 'username');
                }
            } // ----------------------------------

            $this->view->setVars(array(
                'req' => $req,
                'remember' => $this->cookies->get('remember')->getValue()
            ));
        }

    }

    public function logoutAction()
    {
        $this->view->disable();
        $this->session->remove('isAuth');
        $this->flashSession->output();
        $this->flashSession->success($this->_getTranslation()->_('Logout success!'));
        //$this->cookies->reset();
        $this->response->redirect('admin/auth/login');
    }

}