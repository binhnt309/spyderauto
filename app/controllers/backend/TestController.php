<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/24/15
 * Time: 9:26 AM
 */

namespace Application\Controllers\Backend;

class TestController extends ControllerBase
{

    public function addCountryAction()
    {

        $this->view->disable();
        include $this->config->application->helper . 'simple_html_dom.php';
        $html = '<select id="enterAddressCountryCode" class="enterAddressFormField" name="enterAddressCountryCode">
                    <option value="">--</option>
                    <option value="AF">Afghanistan</option>
                    <option value="AX">Aland Islands</option>
                    <option value="AL">Albania</option>
                    <option value="DZ">Algeria</option>
                    <option value="AS">American Samoa</option>
                    <option value="AD">Andorra</option>
                    <option value="AO">Angola</option>
                    <option value="AI">Anguilla</option>
                    <option value="AQ">Antarctica</option>
                    <option value="AG">Antigua and Barbuda</option>
                    <option value="AR">Argentina</option>
                    <option value="AM">Armenia</option>
                    <option value="AW">Aruba</option>
                    <option value="AU">Australia</option>
                    <option value="AT">Austria</option>
                    <option value="AZ">Azerbaijan</option>
                    <option value="BS">Bahamas, The</option>
                    <option value="BH">Bahrain</option>
                    <option value="BD">Bangladesh</option>
                    <option value="BB">Barbados</option>
                    <option value="BY">Belarus</option>
                    <option value="BE">Belgium</option>
                    <option value="BZ">Belize</option>
                    <option value="BJ">Benin</option>
                    <option value="BM">Bermuda</option>
                    <option value="BT">Bhutan</option>
                    <option value="BO">Bolivia</option>
                    <option value="BQ">Bonaire, Saint Eustatius and Saba</option>
                    <option value="BA">Bosnia and Herzegovina</option>
                    <option value="BW">Botswana</option>
                    <option value="BV">Bouvet Island</option>
                    <option value="BR">Brazil</option>
                    <option value="IO">British Indian Ocean Territory</option>
                    <option value="BN">Brunei Darussalam</option>
                    <option value="BG">Bulgaria</option>
                    <option value="BF">Burkina Faso</option>
                    <option value="BI">Burundi</option>
                    <option value="KH">Cambodia</option>
                    <option value="CM">Cameroon</option>
                    <option value="CA">Canada</option>
                    <option value="CV">Cape Verde</option>
                    <option value="KY">Cayman Islands</option>
                    <option value="CF">Central African Republic</option>
                    <option value="TD">Chad</option>
                    <option value="CL">Chile</option>
                    <option value="CN">China</option>
                    <option value="CX">Christmas Island</option>
                    <option value="CC">Cocos (Keeling) Islands</option>
                    <option value="CO">Colombia</option>
                    <option value="KM">Comoros</option>
                    <option value="CG">Congo</option>
                    <option value="CD">Congo, The Democratic Republic of the</option>
                    <option value="CK">Cook Islands</option>
                    <option value="CR">Costa Rica</option>
                    <option value="CI">Cote D\'ivoire</option>
                    <option value="HR">Croatia</option>
                    <option value="CW">Curaçao</option>
                    <option value="CY">Cyprus</option>
                    <option value="CZ">Czech Republic</option>
                    <option value="DK">Denmark</option>
                    <option value="DJ">Djibouti</option>
                    <option value="DM">Dominica</option>
                    <option value="DO">Dominican Republic</option>
                    <option value="EC">Ecuador</option>
                    <option value="EG">Egypt</option>
                    <option value="SV">El Salvador</option>
                    <option value="GQ">Equatorial Guinea</option>
                    <option value="ER">Eritrea</option>
                    <option value="EE">Estonia</option>
                    <option value="ET">Ethiopia</option>
                    <option value="FK">Falkland Islands (Malvinas)</option>
                    <option value="FO">Faroe Islands</option>
                    <option value="FJ">Fiji</option>
                    <option value="FI">Finland</option>
                    <option value="FR">France</option>
                    <option value="GF">French Guiana</option>
                    <option value="PF">French Polynesia</option>
                    <option value="TF">French Southern Territories</option>
                    <option value="GA">Gabon</option>
                    <option value="GM">Gambia, The</option>
                    <option value="GE">Georgia</option>
                    <option value="DE">Germany</option>
                    <option value="GH">Ghana</option>
                    <option value="GI">Gibraltar</option>
                    <option value="GR">Greece</option>
                    <option value="GL">Greenland</option>
                    <option value="GD">Grenada</option>
                    <option value="GP">Guadeloupe</option>
                    <option value="GU">Guam</option>
                    <option value="GT">Guatemala</option>
                    <option value="GG">Guernsey</option>
                    <option value="GN">Guinea</option>
                    <option value="GW">Guinea-Bissau</option>
                    <option value="GY">Guyana</option>
                    <option value="HT">Haiti</option>
                    <option value="HM">Heard Island and the McDonald Islands</option>
                    <option value="VA">Holy See</option>
                    <option value="HN">Honduras</option>
                    <option value="HK">Hong Kong</option>
                    <option value="HU">Hungary</option>
                    <option value="IS">Iceland</option>
                    <option value="IN">India</option>
                    <option value="ID">Indonesia</option>
                    <option value="IQ">Iraq</option>
                    <option value="IE">Ireland</option>
                    <option value="IM">Isle of Man</option>
                    <option value="IL">Israel</option>
                    <option value="IT">Italy</option>
                    <option value="JM">Jamaica</option>
                    <option value="JP">Japan</option>
                    <option value="JE">Jersey</option>
                    <option value="JO">Jordan</option>
                    <option value="KZ">Kazakhstan</option>
                    <option value="KE">Kenya</option>
                    <option value="KI">Kiribati</option>
                    <option value="KR">Korea, Republic of</option>
                    <option value="XK">Kosovo</option>
                    <option value="KW">Kuwait</option>
                    <option value="KG">Kyrgyzstan</option>
                    <option value="LA">Lao People\'s Democratic Republic</option>
                    <option value="LV">Latvia</option>
                    <option value="LB">Lebanon</option>
                    <option value="LS">Lesotho</option>
                    <option value="LR">Liberia</option>
                    <option value="LY">Libya</option>
                    <option value="LI">Liechtenstein</option>
                    <option value="LT">Lithuania</option>
                    <option value="LU">Luxembourg</option>
                    <option value="MO">Macao</option>
                    <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                    <option value="MG">Madagascar</option>
                    <option value="MW">Malawi</option>
                    <option value="MY">Malaysia</option>
                    <option value="MV">Maldives</option>
                    <option value="ML">Mali</option>
                    <option value="MT">Malta</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MQ">Martinique</option>
                    <option value="MR">Mauritania</option>
                    <option value="MU">Mauritius</option>
                    <option value="YT">Mayotte</option>
                    <option value="MX">Mexico</option>
                    <option value="FM">Micronesia, Federated States of</option>
                    <option value="MD">Moldova, Republic of</option>
                    <option value="MC">Monaco</option>
                    <option value="MN">Mongolia</option>
                    <option value="ME">Montenegro</option>
                    <option value="MS">Montserrat</option>
                    <option value="MA">Morocco</option>
                    <option value="MZ">Mozambique</option>
                    <option value="MM">Myanmar</option>
                    <option value="NA">Namibia</option>
                    <option value="NR">Nauru</option>
                    <option value="NP">Nepal</option>
                    <option value="NL">Netherlands</option>
                    <option value="AN">Netherlands Antilles</option>
                    <option value="NC">New Caledonia</option>
                    <option value="NZ">New Zealand</option>
                    <option value="NI">Nicaragua</option>
                    <option value="NE">Niger</option>
                    <option value="NG">Nigeria</option>
                    <option value="NU">Niue</option>
                    <option value="NF">Norfolk Island</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="NO">Norway</option>
                    <option value="OM">Oman</option>
                    <option value="PK">Pakistan</option>
                    <option value="PW">Palau</option>
                    <option value="PS">Palestinian Territories</option>
                    <option value="PA">Panama</option>
                    <option value="PG">Papua New Guinea</option>
                    <option value="PY">Paraguay</option>
                    <option value="PE">Peru</option>
                    <option value="PH">Philippines</option>
                    <option value="PN">Pitcairn</option>
                    <option value="PL">Poland</option>
                    <option value="PT">Portugal</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="QA">Qatar</option>
                    <option value="RE">Reunion</option>
                    <option value="RO">Romania</option>
                    <option value="RU">Russian Federation</option>
                    <option value="RW">Rwanda</option>
                    <option value="BL">Saint Barthelemy</option>
                    <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                    <option value="KN">Saint Kitts and Nevis</option>
                    <option value="LC">Saint Lucia</option>
                    <option value="MF">Saint Martin</option>
                    <option value="PM">Saint Pierre and Miquelon</option>
                    <option value="VC">Saint Vincent and the Grenadines</option>
                    <option value="WS">Samoa</option>
                    <option value="SM">San Marino</option>
                    <option value="ST">Sao Tome and Principe</option>
                    <option value="SA">Saudi Arabia</option>
                    <option value="SN">Senegal</option>
                    <option value="RS">Serbia</option>
                    <option value="SC">Seychelles</option>
                    <option value="SL">Sierra Leone</option>
                    <option value="SG">Singapore</option>
                    <option value="SX">Sint Maarten</option>
                    <option value="SK">Slovakia</option>
                    <option value="SI">Slovenia</option>
                    <option value="SB">Solomon Islands</option>
                    <option value="SO">Somalia</option>
                    <option value="ZA">South Africa</option>
                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                    <option value="ES">Spain</option>
                    <option value="LK">Sri Lanka</option>
                    <option value="SR">Suriname</option>
                    <option value="SJ">Svalbard and Jan Mayen</option>
                    <option value="SZ">Swaziland</option>
                    <option value="SE">Sweden</option>
                    <option value="CH">Switzerland</option>
                    <option value="TW">Taiwan</option>
                    <option value="TJ">Tajikistan</option>
                    <option value="TZ">Tanzania, United Republic of</option>
                    <option value="TH">Thailand</option>
                    <option value="TL">Timor-leste</option>
                    <option value="TG">Togo</option>
                    <option value="TK">Tokelau</option>
                    <option value="TO">Tonga</option>
                    <option value="TT">Trinidad and Tobago</option>
                    <option value="TN">Tunisia</option>
                    <option value="TR">Turkey</option>
                    <option value="TM">Turkmenistan</option>
                    <option value="TC">Turks and Caicos Islands</option>
                    <option value="TV">Tuvalu</option>
                    <option value="UG">Uganda</option>
                    <option value="UA">Ukraine</option>
                    <option value="AE">United Arab Emirates</option>
                    <option value="GB">United Kingdom</option>
                    <option value="US">United States</option>
                    <option value="UM">United States Minor Outlying Islands</option>
                    <option value="UY">Uruguay</option>
                    <option value="UZ">Uzbekistan</option>
                    <option value="VU">Vanuatu</option>
                    <option value="VE">Venezuela</option>
                    <option selected="" value="VN">Vietnam</option>
                    <option value="VG">Virgin Islands, British</option>
                    <option value="VI">Virgin Islands, U.S.</option>
                    <option value="WF">Wallis and Futuna</option>
                    <option value="EH">Western Sahara</option>
                    <option value="YE">Yemen</option>
                    <option value="ZM">Zambia</option>
                    <option value="ZW">Zimbabwe</option>
                    </select>';

        $html = '<select id="state" name="state"><option value="default">-Select-</option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District Of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option><option value="PR">Puerto Rico</option><option value="VI">Virgin Islands</option><option value="MP">Northern Mariana Islands</option><option value="GU">Guam</option><option value="AS">American Samoa</option><option value="PW">Palau</option><option value="AA">Armed Forces (AA)</option><option value="AE">Armed Forces (AE)</option><option value="AP">Armed Forces (AP)</option></select>';
        $html = str_get_html($html);
        echo '<pre>';
        foreach ($html->find('select', 0)->children() as $k => $v) {
            $country = new \LocationsExt();
            $country->setParentId(232);
            $country->setAlias($v->value);
            $country->setName($v->innertext);
            $country->setType(2);
            if ($country->getAlias())
                $country->save();

            unset($country);

            echo $v->value . ' - ' . $v->innertext . '<br/>';
        }
    }

    public function readCsvFileAction()
    {
        $start = microtime(true);
        $file = fopen($this->config->application->baseDir . "headlight_template.csv", "r");

        $inc = 0;
        $result = [];
        $index = 0;
        $model_key = 0;

        while (!feof($file) & $inc < 2500) {

            $data = fgetcsv($file);
            if ($inc == 0) {
                $inc++;
                continue;
            }
            if (!empty($data[0])) {
                $index = $inc;
                $model_key = 0;

                $result[$index]['title'] = $data[0];
                $result[$index]['sku'] = $data[2];
                $result[$index]['thumb'] = $data[5];
                $result[$index]['price'] = $data[8];
                $result[$index]['stock'] = $data[10];
                $result[$index]['description'] = $data[6];
            }

            if (!$data[1]) {
                continue;
            } // Check if has relationship or not

            // Find list
            $relationship = preg_split('/\|/', $data[1]);

            if (!isset($result[$index]['relationship'])) {
                $result[$index]['relationship'] = [];
            }
            if (isset($relationship[1]))
                $result[$index]['relationship']['make'] = preg_split('/=/', $relationship[1])[1];

            // Check isset model key
            if (!isset($result[$index]['relationship']['model'])) {
                $result[$index]['relationship']['model'] = [];
            } // ------------------

            if (!isset($result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]])) {

                $result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]] = [];

            }

            if (!empty($result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]])) {
                if ($result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]][0] > preg_split('/=/', $relationship[0])[1]) {
                    $result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]][0] = preg_split('/=/', $relationship[0])[1];
                }

                if (!isset($result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]][1])) {
                    $result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]][1] = preg_split('/=/', $relationship[0])[1];
                } elseif ($result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]][1] < preg_split('/=/', $relationship[0])[1]) {
                    $result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]][1] = preg_split('/=/', $relationship[0])[1];
                }

            } else {
                $result[$index]['relationship']['model'][preg_split('/=/', $relationship[2])[1]][0] = preg_split('/=/', $relationship[0])[1];
            }

            $inc++;
            $model_key++;
        }

        /*foreach ($result as $k => $r) {
            var_dump($r['relationship']);
        } die();*/

        // Save data
        foreach ($result as $k => $v) {

            $relationship = [];

            // Check make
            $make = \ProductBrandsExt::findFirst("brand_name='{$v['relationship']['make']}'");
            if(!$make instanceof \ProductBrandsExt) {
                $make = new \ProductBrandsExt();
                $make->setBrandName($v['relationship']['make']);
                $make->setStatus(\ProductBrandsExt::STATUS_ENABLED);
                $make->save();

            }

            if(isset($v['relationship']['model'])) {
                $inc = 0;
                foreach($v['relationship']['model'] as $k_model => $v_model) {
                    $model = \ProductModelsExt::findFirst("brand={$make->getId()} AND model_name='{$k_model}'");
                    if(!$model instanceof \ProductModelsExt) {
                        $model = new \ProductModelsExt();
                        $model->setBrand($make->getId());
                        $model->setModelName($k_model);
                        $model->save();
                    }
                    // Check year
                    $end_year = isset($v_model[1]) ? $v_model[1] : $v_model[0];
                    $year = \ProductYearsExt::findFirst("brand_id={$make->getId()} AND model_id={$model->getId()} AND year={$v_model[0]} AND end_year={$end_year}");
                    if(!$year instanceof \ProductYearsExt) {
                        $year = new \ProductYearsExt();
                        $year->setBrandId($make->getId());
                        $year->setModelId($model->getId());
                        $year->setYear($v_model[0]);
                        $year->setEndYear($end_year);
                        if(!$year->save()) {
                            var_dump($year->getMessages());
                        }
                    }
                    $relationship[$inc]['brand'] = $make->getId();
                    $relationship[$inc]['model'] = $model->getId();
                    $relationship[$inc]['year'] = $year->getId();
                    $relationship[$inc]['start_year'] = $v_model[0];
                    $relationship[$inc]['end_year'] = $end_year;
                    $relationship[$inc]['label'] = $make->getBrandName() . ', ' . $model->getModelName() . ', ' . $year->getYear() . ' - ' . $year->getEndYear();

                    $inc++;
                }
            } else {
                $relationship[]['brand'] = $make->getId();
            }

            // Save new product
            $product = new \ProductsExt();

            $product->setTitle($v['title']);
            $product->setSku($v['sku']);
            $product->setAlias($v['title']);
            $product->setIllustration($v['thumb']);
            $product->setPrice($v['price']);
            $product->setStock($v['stock']);
            $product->setDes($v['description']);
            $product->setStatus(\ProductsExt::STATUS_APPROVED);

            if($product->save()) {
                // Save relationship
                foreach($relationship as $r) {
                    $product_vehicle = new \ProductVehicleExt();

                    $product_vehicle->setProductId($product->getId());
                    $product_vehicle->setBrand($r['brand']);
                    $product_vehicle->setModel($r['model']);
                    $product_vehicle->setYear($r['year']);
                    $product_vehicle->setStartYear($r['start_year']);
                    $product_vehicle->setEndYear($r['end_year']);
                    $product_vehicle->setLabel($r['label']);

                    $product_vehicle->save();
                }
            } else {
                var_dump($product->getMessages());
            }

        }

        echo 'Time executed: ' . (microtime(true) - $start);

        fclose($file);
        die();
    }
}