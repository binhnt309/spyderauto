<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/30/15
 * Time: 10:11 PM
 */
namespace Application\Controllers\Backend;

class FeeController extends ControllerBase {
    public function indexAction(){
        if($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList(){
        $arr_fields = array(
            '',
            'id',
            'country_name',
            'time_delivery',
            'amount'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND country_name LIKE '%{$key}%'";
        }

        $total = \ShippingFeeExt::count(array('conditions' => $condition));

        $ship_fees = \ShippingFeeExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $result = array();
        $currencies = \ModelHelper::toArray(\CurrencyExt::find(), 'id');

        foreach ($ship_fees as $key => $m) {
            if($m instanceof \ShippingFeeExt) {
                $result[$key][] = "<input type='checkbox' value='".$m->getId()."' class='checkbox'/>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getCountryName();
                $result[$key][] = $m->getCityName();
                $result[$key][] = $m->getTimeDelivery();
                $result[$key][] = $m->getAmount();
                $result[$key][] = $currencies[$m->getCurrencyId()]['abbreviation'];
                $result[$key][] = "<a href='/admin/fee/edit/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/fee/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    public function createAction(){

        $ship_fee = new \ShippingFeeExt();
        $countries = \LocationsExt::find([
            'conditions' =>'type=' . \LocationsExt::TYPE_COUNTRY,
            'order' => 'name'
        ]);

        $cities = \LocationsExt::find([
            'conditions' => "type=" . \LocationsExt::TYPE_PROVINCE,
            'order' => 'parent_id, name'
        ]);

        $currencies = \CurrencyExt::find();
        $currencies_arr = \ModelHelper::toArray($currencies, 'id');

        if ($this->request->isPost()) {

            $countries_arr = \ModelHelper::toArray($countries, 'id');
            $cities_arr = \ModelHelper::toArray($cities, 'id');

            $ship_fee->setCountryId($this->request->getPost('country_id'));
            $ship_fee->setCountryName($countries_arr[$ship_fee->getCountryId()]['name']);

            $ship_fee->setCityId($this->request->getPost('city_id'));
            $ship_fee->setCityName($cities_arr[$ship_fee->getCityId()]['name']);

            $ship_fee->setTimeDelivery($this->request->getPost('time_delivery'));
            $ship_fee->setAmount($this->request->getPost('amount'));
            $ship_fee->setCurrencyId($this->request->getPost('currency'));
            $ship_fee->setCurrencySymbol($currencies_arr[$ship_fee->getCurrencyId()]['symbol']);

            if ($ship_fee->save()) {
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create ship fee success!'));
                $this->response->redirect('admin/fee/index');
            } else {
                $msg = [];
                foreach ($ship_fee->getMessages() as $message) {
                    $msg[] = ' - ' . $message->getMessage();
                }
                $this->flashSession->error($this->_getTranslation()->_('Create ship fee error!') . '<br/>' . implode('<br/>', $msg));
            }

        }

        $this->view->setVars([
            'fee' => $ship_fee,
            'currencies' => $currencies,
            'req' => $this->request,
            'countries' => $countries,
            'country' => 0,
            'cities' => $cities,
            'city' => 0
        ]);
    }

    public function editAction($id=0){

        $ship_fee = \ShippingFeeExt::findFirst($id);
        $countries = \LocationsExt::find([
            'conditions' =>'type=' . \LocationsExt::TYPE_COUNTRY,
            'order' => 'name'
        ]);

        $cities = \LocationsExt::find([
            'conditions' => "type=" . \LocationsExt::TYPE_PROVINCE,
            'order' => 'parent_id, name'
        ]);

        $currencies = \CurrencyExt::find();
        $currencies_arr = \ModelHelper::toArray($currencies, 'id');

        if($ship_fee instanceof \ShippingFeeExt) {
            if ($this->request->isPost()) {

                $countries_arr = \ModelHelper::toArray($countries, 'id');
                $cities_arr = \ModelHelper::toArray($cities, 'id');

                $ship_fee->setCountryId($this->request->getPost('country_id'));
                $ship_fee->setCountryName($countries_arr[$ship_fee->getCountryId()]['name']);

                $ship_fee->setCityId($this->request->getPost('city_id'));
                $ship_fee->setCityName($cities_arr[$ship_fee->getCityId()]['name']);

                $ship_fee->setTimeDelivery($this->request->getPost('time_delivery'));
                $ship_fee->setAmount($this->request->getPost('amount'));
                $ship_fee->setCurrencyId($this->request->getPost('currency'));
                $ship_fee->setCurrencySymbol($currencies_arr[$ship_fee->getCurrencyId()]['symbol']);

                if ($ship_fee->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Create ship fee success!'));
                    $this->response->redirect('admin/fee/index');
                } else {
                    $msg = [];
                    foreach ($ship_fee->getMessages() as $message) {
                        $msg[] = ' - ' . $message->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Create ship fee error!') . '<br/>' . implode('<br/>', $msg));
                }

            } // End post process --------
        } else {
            $ship_fee = new \ShippingFeeExt();
            $this->flashSession->warning($this->_getTranslation()->_('Can not find this ship fee setting'));
        } // -----------

        $this->view->setVars([
            'fee' => $ship_fee,
            'currencies' => $currencies,
            'req' => $this->request,
            'countries' => $countries,
            'cities' => $cities,
            'country' => $ship_fee->getCountryId(),
            'city' => $ship_fee->getCityId()
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0) {
        $this->view->disable();

        $fee = \ShippingFeeExt::findFirst($id);
        if(!$fee instanceof \ShippingFeeExt) {
            $this->flashSession->warning('Can\'t find this fee setup');
        } else {
            if($fee->delete()) {
                $this->flashSession->success('Delete fee success');
            } else {
                $msg = '';
                foreach($fee->getMessages() as $m) {
                    $m .= '<br/> - ' . $m->getMessage();
                }
                $this->flashSession->error('Delete fee error!' . $msg);
            }
        }

        $this->response->redirect('admin/fee/index');
    }
}