<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class BannerController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList()
    {
        $arr_fields = array(
            '',
            'id',
            'title',
            'image',
            'link',
            'pos',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND title LIKE '%{$key}%'";
        }

        $total = \BannersExt::count(array('conditions' => $condition));

        $banners = \BannersExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $result = array();
        $status = \BannersExt::statusArr();
        foreach ($banners as $key => $m) {
            if ($m instanceof \Banners) {
                $dir = $this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'banner' . DS;

                $img = '/uploads/banner/default.png';
                if (file_exists($dir . $m->getImage())) {
                    $img = '/uploads/banner/' . $m->getImage();
                }

                $result[$key][] = "<input type='checkbox' value='{$m->getId()}' />";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getTitle();
                $result[$key][] = "<img src='{$img}' style='max-width: 200px;'/>";
                $result[$key][] = $m->getLink() ? "<a href='{$m->getLink()} target='_blank'>{$m->getLink()}</a>" : '';
                $result[$key][] = $m->getPos() ? $m->getPos() : 0;
                $result[$key][] = $status[$m->getStatus()];
                $result[$key][] = "<a href='/admin/banner/edit/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/banner/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction()
    {
        $banner = new \BannersExt();

        if ($this->request->isPost()) {

            $banner->setTitle($this->request->getPost('title'));
            $banner->setLink($this->request->getPost('link'));
            $banner->setPos($this->request->getPost('pos'));
            $banner->setStatus($this->request->getPost('status'));

            // Upload file
            require_once $this->config->application->helper . DIRECTORY_SEPARATOR . 'FileUpload.php';
            $upload_dir = $this->config->application->uploadDir . DS . 'banner' . DS;
            // Check upload product dir exist
            if (!is_dir($upload_dir)) {
                $old_mask = umask(0);
                mkdir($upload_dir, 0777);
                umask($old_mask);
            } // ------

            $valid_extensions = array('png', 'jpg', 'gif', 'jpeg', 'JPEG');
            $upload = new \FileUpload('image');
            $result = $upload->handleUpload($upload_dir, $valid_extensions);

            $uploaded = false;
            if ($result) {
                $uploaded = true;
                $banner->setImage($upload->getFileName());
            } else if (@$_FILES['image']['name'] != '') {
                $this->flashSession->error($this->_getTranslation()->_($upload->getErrorMsg()));
            } else {
                $uploaded = true;
            } // ------------

            if ($uploaded) {
                if ($banner->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Create banner success!'));
                    $this->response->redirect('admin/banner/index');
                } else {
                    $msg = [];
                    foreach ($banner->getMessages() as $message) {
                        $msg[] = ' - ' . $message->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Create banner error!') . '<br/>' . implode('<br/>', $msg));
                }
            }
        }

        $this->view->setVars([
            'banner' => $banner,
            'req' => $this->request,
            'status' => \BannersExt::statusArr(),
            'image' => '/uploads/banner/default.png'
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0)
    {

        $banner = \BannersExt::findFirst($id);

        if ($banner instanceof \BannersExt) {
            if ($this->request->isPost()) {

                $banner->setTitle($this->request->getPost('title'));
                $banner->setLink($this->request->getPost('link'));
                $banner->setPos($this->request->getPost('pos'));
                $banner->setStatus($this->request->getPost('status'));

                // Upload file
                require_once $this->config->application->helper . DIRECTORY_SEPARATOR . 'FileUpload.php';
                $upload_dir = $this->config->application->uploadDir . DS . 'banner' . DS;
                // Check upload product dir exist
                if (!is_dir($upload_dir)) {
                    $old_mask = umask(0);
                    mkdir($upload_dir, 0777);
                    umask($old_mask);
                } // ------

                $valid_extensions = array('png', 'jpg', 'gif', 'jpeg', 'JPEG');
                $upload = new \FileUpload('image');
                $result = $upload->handleUpload($upload_dir, $valid_extensions);

                $uploaded = false;
                if ($result) {
                    $uploaded = true;
                    $banner->setImage($upload->getFileName());
                } else if (@$_FILES['image']['name'] != '') {
                    $this->flashSession->error($this->_getTranslation()->_($upload->getErrorMsg()));
                } else {
                    $uploaded = true;
                } // ------------

                if ($uploaded) {
                    if ($banner->save()) {
                        $this->view->disable();
                        $this->flashSession->success($this->_getTranslation()->_('Update banner success!'));
                        $this->response->redirect('admin/banner/index');
                    } else {
                        $msg = [];
                        foreach ($banner->getMessages() as $message) {
                            $msg[] = ' - ' . $message->getMessage();
                        }
                        $this->flashSession->error($this->_getTranslation()->_('Update banner error!') . '<br/>' . implode('<br/>', $msg));
                    }
                }
            }
        } else {
            $banner = new \BannersExt();
            $this->flashSession->warning($this->_getTranslation()->_('Banner information not found'));
        }

        $img = 'uploads/banner/default.png';
        if (file_exists($this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'banner' . DS . $banner->getImage())
            & $banner->getImage() != ''
        ) {
            $img = '/uploads/banner/' . $banner->getImage();
        }
        $this->view->setVars([
            'banner' => $banner,
            'req' => $this->request,
            'status' => \BannersExt::statusArr(),
            'image' => $img
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0)
    {
        $this->view->disable();
        // Option to display flash message
        $year = \BannersExt::findFirst($id);
        if ($year instanceof \BannersExt) {
            if (!$year->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/banner/index');
    }
}