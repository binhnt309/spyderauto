<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class TransactionController extends ControllerBase {

    public function indexAction() {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }
    private function __loadList()
    {
        $arr_fields = array(
            'id',
            'order_code',
            'tx_transaction',
            'amount',
            'created_at',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND (order_code LIKE '%{$key}%' OR tx_transaction LIKE '%$key%')";
        }

        $total = \TransactionsExt::count(array('conditions' => $condition));

        $trans = \TransactionsExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $result = array();
        $status = \TransactionsExt::statusArr();
        foreach ($trans as $key => $m) {
            if ($m instanceof \TransactionsExt) {
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getTxTransaction();
                $result[$key][] = $m->getTxTransaction();
                $result[$key][] = $m->getAmount();
                $result[$key][] = $m->getCreatedAt();
                $result[$key][] = isset($status[$m->getStatus()]) ? $status[$m->getStatus()] : $status[\OrdersExt::STATUS_WAIT];
                $result[$key][] = $m->getStatus() < \TransactionsExt::STATUS_APPROVED ?
                    "<a href='/admin/transaction/approve/{$m->getId()}' class='btn btn-primary'>". $this->_getTranslation()->_('Approved') . "</a> " .
                    "<a href='/admin/transaction/refund/{$m->getId()}' class='btn btn-danger'>". $this->_getTranslation()->_('Refund') . "</a>" : "";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * @param int $trans_id
     */
    public function approveAction($trans_id = 0) {
        $this->view->disable();

        $trans = \TransactionsExt::findFirst($trans_id);
        if($trans instanceof \TransactionsExt) {
            $trans->setStatus(\TransactionsExt::STATUS_APPROVED);
            if($trans->save()) {
                $this->flashSession->success($this->closeHtmlButton . 'Approve transaction ' . $trans->getTxTransaction() . ' success!');
            } else {
                $msg = [];
                foreach($trans->getMessages() as $m) {
                    $msg[] = $m->getMessage();
                }
                $this->flashSession->error($this->closeHtmlButton . 'Approve transaction ' . $trans->getTxTransaction() . ' fail! ' . implode(', ', $msg));
            }
        } else {
            $this->flashSession->warning($this->closeHtmlButton . 'Transaction not found!');
        }

        $this->response->redirect('admin/transaction/index');
    }


    /**
     * @param int $trans_id
     */
    public function refundAction($trans_id = 0) {
        $this->view->disable();

        $trans = \TransactionsExt::findFirst($trans_id);
        if($trans instanceof \TransactionsExt) {
            $trans->setStatus(\TransactionsExt::STATUS_REFUND);
            if($trans->save()) {
                $this->flashSession->success($this->closeHtmlButton . 'Refund transaction ' . $trans->getTxTransaction() . ' success!');
            } else {
                $msg = [];
                foreach($trans->getMessages() as $m) {
                    $msg[] = $m->getMessage();
                }
                $this->flashSession->error($this->closeHtmlButton . 'Refund transaction ' . $trans->getTxTransaction() . ' fail! ' . implode(', ', $msg));
            }
        } else {
            $this->flashSession->warning($this->closeHtmlButton . 'Transaction not found!');
        }

        $this->response->redirect('admin/transaction/index');
    }
}