<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class ArtCategoryController extends ControllerBase {

    public function indexAction() {

        if($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList(){
        $arr_fields = array(
            '',
            'id',
            'name',
            'alias',
            'parent_id',
            'in_menu',
            'in_footer',
            'pos',
            'total_items',
            'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND (name LIKE '%{$key}%' OR alias LIKE '%{$key}%')";
        }

        // Request private data

        $total = \ArtCategoriesExt::count(array('conditions' => $condition));

        $art_categories = \ArtCategoriesExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        $art_cate_arr = \ModelHelper::toArray(\ArtCategoriesExt::find([
            'columns' => 'id, name'
        ]), 'id');

        $result = array();
        $status = \ProductYearsExt::statusArr();
        foreach ($art_categories as $key => $m) {
            if ($m instanceof \ArtCategoriesExt) {
                $result[$key][] = "<input type='checkbox' value='{$m->getId()}' class='checkbox'/>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $m->getName() . '<br/> <i>' . $m->getAlias() . '</i>';
                $result[$key][] = isset($art_cate_arr[$m->getParentId()]) ? $art_cate_arr[$m->getParentId()]['name'] : '';
                $result[$key][] = "<input type='checkbox' value='{$m->getId()}' ".($m->getInMenu() ?'checked':'')." />";
                $result[$key][] = "<input type='checkbox' value='{$m->getId()}' ".($m->getInFooter() ?'checked':'')." />";
                $result[$key][] = "<input type='text' value='{$m->getPos()}' class='form-control' style='width: 50px' />";
                $result[$key][] = $m->getTotalItems() ? $m->getTotalItems() : 0;
                $result[$key][] = $status[$m->getStatus()];
                $result[$key][] = "<a href='/admin/art_category/edit/{$m->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/art_category/delete/{$m->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction() {
        $art_category = new \ArtCategoriesExt();

        if($this->request->isPost()){
            $art_category->setName($this->request->getPost('name'));
            $art_category->setAlias($this->request->getPost('alias'));
            $art_category->setParentId($this->request->getPost('parent_id'));
            $art_category->setInMenu($this->request->getPost('in_menu') ? 1 : 0);
            $art_category->setInFooter($this->request->getPost('in_footer') ? 1 : 0);
            $art_category->setPos($this->request->getPost('pos'));
            $art_category->setStatus($this->request->getPost('status'));

            if($art_category->save()) {
                $this->view->disable();
                $this->flashSession->success($this->_getTranslation()->_('Create article category success!'));
                $this->response->redirect('admin/art_category/index');
            } else {
                $msg = [];
                foreach($art_category->getMessages() as $message){
                    $msg[] = ' - ' . $message->getMessage();
                }
                $this->flashSession->error($this->_getTranslation()->_('Create article category error!') . '<br/>' . implode('<br/>', $msg));
            }
        }

        $this->view->setVars([
            'art_category' => $art_category,
            'req' => $this->request,
            'status' => \ArtCategoriesExt::statusArr(),
            'category_options' => \ModelHelper::rendHtmlOption(
                    \ModelHelper::loadCategory(
                        \ArtCategoriesExt::find([
                            'conditions' => 'status=' .\ArtCategoriesExt::STATUS_ENABLED,
                            'order' => 'parent_id, name'
                        ])->toArray(),
                        0
                    ),
                    1,
                    '&nbsp;&nbsp;&nbsp;&nbsp;',
                    $art_category->getParentId()
                )
        ]);
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0) {
        $art_category = \ArtCategoriesExt::findFirst($id);

        if($art_category instanceof \ArtCategoriesExt) {
            if($this->request->isPost()){
                $art_category->setName($this->request->getPost('name'));
                $art_category->setAlias($this->request->getPost('alias'));
                $art_category->setParentId($this->request->getPost('parent_id'));
                $art_category->setInMenu($this->request->getPost('in_menu') ? 1 : 0);
                $art_category->setInFooter($this->request->getPost('in_footer') ? 1 : 0);
                $art_category->setPos($this->request->getPost('pos'));
                $art_category->setStatus($this->request->getPost('status'));

                if($art_category->save()) {
                    $this->view->disable();
                    $this->flashSession->success($this->_getTranslation()->_('Create article category success!'));
                    $this->response->redirect('admin/art_category/index');
                } else {
                    $msg = [];
                    foreach($art_category->getMessages() as $message){
                        $msg[] = ' - ' . $message->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Create article category error!') . '<br/>' . implode('<br/>', $msg));
                }
            }
        } else {
            $art_category = new \ArtCategoriesExt();
            $this->flashSession->warning($this->_getTranslation()->_('Can not found this category'));
        }

        $this->view->setVars([
            'art_category' => $art_category,
            'req' => $this->request,
            'status' => \ArtCategoriesExt::statusArr(),
            'category_options' => \ModelHelper::rendHtmlOption(
                    \ModelHelper::loadCategory(
                        \ArtCategoriesExt::find([
                            'conditions' => 'id != ' . $art_category->getId() . ' AND status=' .\ArtCategoriesExt::STATUS_ENABLED,
                            'order' => 'parent_id, name'
                        ])->toArray(),
                        0
                    ),
                    1,
                    '&nbsp;&nbsp;&nbsp;&nbsp;',
                    $art_category->getParentId()
                )
        ]);
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0) {
        $this->view->disable();
        // Option to display flash message
        $art_category = \ArtCategoriesExt::findFirst($id);

        if ($art_category instanceof \ArtCategoriesExt) {
            if (!$art_category->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/art_category/index');
    }
}