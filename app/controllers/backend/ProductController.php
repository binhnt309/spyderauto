<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/21/15
 * Time: 11:28 AM
 */

namespace Application\Controllers\Backend;

class ProductController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $this->__loadList();
            return;
        } // End load ajax list ---------

        $this->assets->collection('backendCss')
            ->addCss('assets/css/plugins/dataTables/dataTables.bootstrap.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/metisMenu/jquery.metisMenu.js')
            ->addJs('assets/js/plugins/dataTables/jquery.dataTables.js')
            ->addJs('assets/js/plugins/dataTables/dataTables.bootstrap.js');
    }

    private function __loadList()
    {
        $arr_fields = array(
            'id',
            'title',
            'price',
            //'brand_id',
            'illustration',
            'stock',
            'sale',
            //'status'
        );
        $get_order = $this->request->get('order');
        $order = $arr_fields[$get_order[0]['column']] . ' ' . $get_order[0]['dir'];

        $draw = $this->request->get('draw');
        $key = $this->request->get('key');

        $start = $this->request->get('start');
        $limit = $this->request->get('length');
        if (empty($limit) || $limit === 0) {
            $limit = 10;
        }

        $condition = "1=1";
        if (!empty($key)) {
            $condition .= " AND title LIKE '%{$key}%' OR sku LIKE '%{$key}%'";
        }

        // Request private data
        $brand = $this->request->get('brand');
        $models = $this->request->get('model');
        if (!empty($brand)) {
            $condition .= " AND brand_id=" . $brand;
        }
        if (!empty($models)) {
            $condition .= " AND model_id=" . $models;
        }

        $total = \ProductsExt::count(array('conditions' => $condition));

        $products = \ProductsExt::find(array(
            'conditions' => $condition,
            'order' => $order,
            'limit' => array('number' => $limit, 'offset' => $start)
        ));

        //$brands = \ModelHelper::toArray(\ProductBrandsExt::find(), 'id');
        $currencies = \ModelHelper::toArray(\CurrencyExt::find(), 'id');

        $result = array();
        //$status = \ProductsExt::statusArr();
        foreach ($products as $key => $v) {
            if ($v instanceof \ProductsExt) {
                $img = $v->getIllustration();
                $folder = $this->config->application->baseUri . 'uploads/products/' .
                    date('Y-m-d', strtotime($v->getCreatedTime())) . '/';
                $file_exist = !empty($img) & file_exists($this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'products' .
                        DS . date('Y-m-d', strtotime($v->getCreatedTime())) . DS . $v->getIllustration());

                $result[$key][] = "<input type='checkbox' value='{$v->getId()}' class='checkbox'>";
                $result[$key][] = $key + 1 + $start;
                $result[$key][] = $v->getTitle() . '<br/><i>SKU: ' . $v->getSku() . '</i>';
                $result[$key][] = ($v->getPrice() ? $v->getPrice() : 0)
                    . ' (' . (isset($currencies[$v->getCurrencyId()]) ? $currencies[$v->getCurrencyId()]['symbol'] : '$') . ')';
                //$result[$key][] = isset($brands[$v->getBrandId()]) ? $brands[$v->getBrandId()]['brand_name'] : '';
                $result[$key][] = strpos($v->getIllustration(), 'http:') > -1 ? "<img src='" . $v->getIllustration() . "'/>" : ($file_exist ? "<img src='{$folder}{$v->getIllustration()}' alt='{$v->getTitle()}'/>" :
                    "<img src='" . $this->config->application->baseUri . "uploads/products/no-image.png'>");
                $result[$key][] = $v->getStock();
                $result[$key][] = $v->getSale() ? $v->getSale() : 0;
                //$result[$key][] = $status[$v->getStatus()];
                $result[$key][] = "<a href='/admin/product/edit/{$v->getId()}'>"
                    . $this->_getTranslation()->_('Edit') . "</a> | <a onclick='__delete(event)' class='delete' href='/admin/product/delete/{$v->getId()}'>" . $this->_getTranslation()->_('Delete') . "</a>";
            }
        }

        echo json_encode(
            array(
                'draw' => $draw,
                'recordsFiltered' => $total,
                'recordsTotal' => $total,
                'data' => $result
            )
        );
    }

    /**
     * Create new
     */
    public function createAction()
    {
        $req = $this->request;
        if ($req->isAjax()) {
            $this->view->disable();
            switch ($req->get('option')) {
                case 'loadModel':
                    $this->__loadModel($req->get('id'));
                    break;
                case 'loadYear':
                    $this->__loadYear($req->get('id'));
                    break;
                case 'upload':
                    $this->__upload();
                    break;
                default:
                    break;
            }
            return;
        }
        // ------------
        $this->assets->collection('backendCss')
            ->addCss('assets/css/jquery.datetimepicker.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/tinymce/tinymce.min.js')
            ->addJs('assets/js/plugins/jquery-ui/jquery.datetimepicker.js');

        $product = new \ProductsExt();

        if ($this->request->isPost()) {

            $product->setTitle($req->getPost('title'));
            $product->setSku($req->getPost('sku'));
            $product->setAlias($req->getPost('alias'));
            $product->setBrief($req->getPost('brief'));
            $product->setCid((int)$req->getPost('cid'));
            $product->setPrice($req->getPost('price'));
            $product->setStock($req->getPost('stock'));
            $product->setIllustration($req->getPost('url_link'));
            $product->setCurrencyId($req->getPost('currency'));
            $product->setStatus($req->getPost('status'));
            $product->setSaleOff($req->getPost('sale_off'));
            $product->setSaleOffTo($req->getPost('sale_off_to')); // Date end of sale-off
            $product->setPos($req->getPost('pos', 'int', 0));
            $product->setIsFront($req->getPost('is_front') ? 1 : 0);
            $product->setIsFeature($req->getPost('is_feature') ? 1 : 0);
            $product->setDes($req->getPost('des'));
            $product->setTags($req->getPost('tags'));
            $product->setSale(0);

            // Thumbs process
            $thumbs = $req->getPost('thumbs');
            if(!empty($thumbs)) {
                $thumbs = explode(';', $thumbs);
                if(!empty($thumbs)) {
                    foreach($thumbs as $k => $t) {
                        if(empty($t)) {
                            unset($thumbs[$k]);
                            continue;
                        }
                    }
                }
            } // ------------------
            $product->setThumbs(json_encode($thumbs));

            // Upload file
            require_once $this->config->application->helper . DIRECTORY_SEPARATOR . 'FileUpload.php';
            $upload_dir = $this->config->application->uploadDir . 'products' . DS . date('Y-m-d', time()) . DS;
            // Check upload product dir exist
            if (!is_dir($upload_dir)) {
                $old_mask = umask(0);
                mkdir($upload_dir, 0777);
                umask($old_mask);
            } // ------

            $valid_extensions = array('png', 'jpg', 'gif', 'jpeg', 'JPEG');
            $upload = new \FileUpload('illustration');
            $result = $upload->handleUpload($upload_dir, $valid_extensions);

            $uploaded = false;
            if ($result) {
                $uploaded = true;
                $product->setIllustration($upload->getFileName());
            } else if (@$_FILES['illustration']['name'] != '') {
                $this->flashSession->error($this->_getTranslation()->_($upload->getErrorMsg()));
            } else {
                $uploaded = true;
            } // ------------

            if ($uploaded) {
                if ($product->save()) {
                    $this->view->disable();

                    /**
                     * Remove thumb from temporary folder
                     */
                    $thumb_dir = $upload_dir  . DS . 'thumbs' . DS;
                    // Check upload product dir exist
                    if (!is_dir($thumb_dir)) {
                        $old_mask = umask(0);
                        mkdir($thumb_dir, 0777);
                        umask($old_mask);
                    } // ------
                    if(!empty($thumbs)) {
                        foreach($thumbs as $t) {
                            @rename($this->config->application->uploadDir . 'tmp' .DS . $t, $thumb_dir . $t);
                        }
                    } // -------------- end upload thumb --------------

                    // Save properties
                    $this->__saveProperties($product->getId(), $req->getPost('properties'));

                    // Update total in category, brand, model, year
                    $category = \ProductCategoriesExt::findFirst($product->getCid());
                    if ($category instanceof \ProductCategoriesExt) {
                        $category->setTotal(($category->getTotal() ? $category->getTotal() : 0) + 1)->save();
                    }
                    $brand = \ProductBrandsExt::findFirst($product->getBrandId() ? $product->getBrandId() : 0);
                    if ($brand instanceof \ProductBrandsExt) {
                        $brand->setTotalItems(($brand->getTotalItems() ? $brand->getTotalItems() : 0) + 1)->save();
                    }
                    $model = \ProductModelsExt::findFirst($product->getModelId() ? $product->getModelId() : 0);
                    if ($model instanceof \ProductModelsExt) {
                        $model->setTotalItems(($model->getTotalItems() ? $model->getTotalItems() : 0) + 1)->save();
                    }
                    $year = \ProductYearsExt::findFirst($product->getYearId() ? $product->getYearId() : 0);
                    if ($year instanceof \ProductYearsExt) {
                        $year->setTotalItems(($year->getTotalItems() ? $year->getTotalItems() : 0) + 1)->save();
                    }

                    // Create new seo
                    $seo = new \SeoExt();
                    $seo->setObjectId($product->getId());
                    $seo->setType(\SeoExt::TYPE_PRODUCT);
                    $seo->setDescription($req->getPost('description'));
                    $seo->setKeyword($req->getPost('keyword'));
                    $seo->save();

                    $this->flashSession->success($this->_getTranslation()->_($this->closeHtmlButton . 'Create product success'));
                    $this->response->redirect('admin/product/index');
                } else {
                    $msg = '';
                    foreach ($product->getMessages() as $m) {
                        $msg .= '<br/> - ' . $m->getMessage();
                    }
                    $this->flashSession->error($this->_getTranslation()->_('Create product error') . $msg);
                }
            }

        } //------------

        $brands = \ProductBrandsExt::find();
        $currencies = \CurrencyExt::find();
        $product_illustration = $this->config->application->baseUri . 'uploads/products/no-image.png';
        $categories = \ProductCategoriesExt::find()->toArray();
        $categories = \ModelHelper::loadCategory($categories, 0);
        $category_options = \ModelHelper::rendHtmlOption($categories, 1, '---');

        $this->view->setVars(
            [
                'product' => $product,
                'currencies' => $currencies,
                'brands' => $brands,
                'models' => \ProductModels::find(['conditions' => 'id=0']),
                'years' => \ProductYears::find(['conditions' => 'id=0']),
                'req' => $req,
                'product_illustration' => $product_illustration,
                'status' => \ProductsExt::statusArr(),
                'category_options' => $category_options,
                'seo' => new \SeoExt(),
                'properties' => [],
                'action' => 'create'
            ]
        );
    }

    /**
     * @param int $id
     */
    public function editAction($id = 0)
    {
        $req = $this->request;
        if ($req->isAjax()) {
            $this->view->disable();
            switch ($req->get('option')) {
                case 'loadModel':
                    $this->__loadModel($req->get('id'));
                    break;
                case 'loadYear':
                    $this->__loadYear($req->get('id'));
                    break;
                case 'upload':
                    $this->__upload();
                    break;
                default:
                    break;
            }
            return;
        }
        // ------------
        $this->assets->collection('backendCss')
            ->addCss('assets/css/jquery.datetimepicker.css', true);

        $this->assets->collection('backendJs')
            ->addJs('assets/js/plugins/tinymce/tinymce.min.js')
            ->addJs('assets/js/plugins/jquery-ui/jquery.datetimepicker.js');

        $product = \ProductsExt::findFirst($id);
        $product_vehicle = [];

        if ($product instanceof \ProductsExt) {
            $seo = \SeoExt::findFirst([
                'conditions' => "type=" . \SeoExt::TYPE_PRODUCT . ' AND object_id=' . $product->getId()
            ]);
            if (!$seo) {
                $seo = new \SeoExt();
            }

            // Find vehicle has associate with this product
            $vehicle = \ProductVehicleExt::find(['conditions' => "product_id='" . $product->getId() . "'"]);

            if (count($vehicle)) {

                foreach ($vehicle as $v) {
                    if ($v instanceof \ProductVehicleExt) {
                        $prop = [];
                        if ($v->getBrand()) {
                            $prop[] = $v->getBrand();
                        }
                        if ($v->getModel()) {
                            $prop[] = $v->getModel();
                        }
                        if ($v->getYear()) {
                            $prop[] = $v->getYear();
                        }
                        $product_vehicle[implode(',', $prop)] = $v->getLabel();
                    }
                }
            } //-------------------

            if ($this->request->isPost()) {

                $product->setTitle($req->getPost('title'));
                $product->setSku($req->getPost('sku'));
                $product->setAlias($req->getPost('alias'));
                $product->setBrief($req->getPost('brief'));
                $product->setCid($req->getPost('cid'));
                $product->setPrice($req->getPost('price'));
                $product->setStock($req->getPost('stock'));
                $product->setIllustration($req->getPost('url_link'));
                $product->setCurrencyId($req->getPost('currency'));
                $product->setStatus($req->getPost('status'));
                $product->setSaleOff($req->getPost('sale_off'));
                $product->setSaleOffTo($req->getPost('sale_off_to')); // Date end of sale-off
                $product->setPos($req->getPost('pos', 'int', 0));
                $product->setIsFront($req->getPost('is_front') ? 1 : 0);
                $product->setIsFeature($req->getPost('is_feature') ? 1 : 0);
                $product->setDes($req->getPost('des'));
                $product->setTags($req->getPost('tags'));
                $product->setIllustration($req->getPost('cur_illustration'));
                $product->setSale(0);

                $current_thumbs = $product->getThumbs();
                $current_thumbs = json_decode($current_thumbs, true);
                var_dump($current_thumbs);

                // Thumbs process
                $thumbs = $req->getPost('thumbs');
                if(!empty($thumbs)) {
                    $thumbs = explode(';', $thumbs);
                    if(!empty($thumbs)) {
                        foreach($thumbs as $k => $t) {
                            if(empty($t)) {
                                unset($thumbs[$k]);
                                continue;
                            }
                        }
                    }
                } // ------------------
                $product->setThumbs(json_encode($thumbs));

                // Upload file
                require_once $this->config->application->helper . DS . 'FileUpload.php';
                $upload_dir = $this->config->application->uploadDir . DS . 'products' . DS . date('Y-m-d', strtotime($product->getCreatedTime()) . DS);
                // Check upload product dir exist
                if (!is_dir($upload_dir)) {
                    $old_mask = umask(0);
                    mkdir($upload_dir, 0777);
                    umask($old_mask);
                } // ------

                $valid_extensions = array('png', 'jpg', 'gif', 'jpeg', 'JPEG');
                $upload = new \FileUpload('illustration');
                $result = $upload->handleUpload($upload_dir, $valid_extensions);

                $uploaded = false;
                if ($result) {
                    $uploaded = true;
                    $product->setIllustration($upload->getFileName());
                } else if (@$_FILES['illustration']['name'] != '') {
                    $this->flashSession->error($this->_getTranslation()->_($upload->getErrorMsg()));
                } else {
                    $uploaded = true;
                }

                if ($uploaded) {
                    if ($product->save()) {
                        $this->view->disable();
                        /**
                         * Remove thumb from temporary folder
                         */
                        $thumb_dir = $upload_dir . DS . 'thumbs' . DS;
                        // Check upload product dir exist
                        if (!is_dir($thumb_dir)) {
                            $old_mask = umask(0);
                            mkdir($thumb_dir, 0777);
                            umask($old_mask);
                        } // ------
                        if(!empty($thumbs)) {
                            foreach($thumbs as $t) {
                                if(in_array($t, $current_thumbs)) {
                                    unset($current_thumbs[array_search($t, $current_thumbs)]);
                                }
                                @rename($this->config->application->uploadDir . 'tmp' .DS . $t, $thumb_dir . $t);
                            }
                        } // -------------- end upload thumb --------------

                        // Unset thumb has been deleted
                        if(!empty($current_thumbs)) {
                            foreach($current_thumbs as $thumb) {
                                @unlink($thumb_dir . $thumb);
                            }
                        } // ----------

                        // Save properties
                        $this->__saveProperties($product->getId(), $req->getPost('properties'));

                        // Update seo
                        $seo->setObjectId($product->getId());
                        $seo->setType(\SeoExt::TYPE_PRODUCT);
                        $seo->setDescription($req->getPost('description'));
                        $seo->setKeyword($req->getPost('keyword'));
                        $seo->save();

                        $this->flashSession->success($this->_getTranslation()->_($this->closeHtmlButton . 'Update product success'));
                        $this->response->redirect('admin/product/index');
                    } else {
                        $msg = '';
                        foreach ($product->getMessages() as $m) {
                            $msg .= '<br/> - ' . $m->getMessage();
                        }
                        $this->flashSession->error($this->_getTranslation()->_($this->closeHtmlButton . 'Update product error') . $msg);
                    }
                }

            } //------------

        } else {
            $product = new \ProductsExt();
            $seo = new \SeoExt();
            $this->flashSession->warning($this->_getTranslation()->_($this->closeHtmlButton . 'Can not find this product in system'));
        }

        $brands = \ProductBrandsExt::find();
        $currencies = \CurrencyExt::find();
        $categories = \ProductCategoriesExt::find()->toArray();
        $categories = \ModelHelper::loadCategory($categories, 0);
        $category_options = \ModelHelper::rendHtmlOption($categories, 1, '---', $product->getCid());
        $product_illustration = strpos($product->getIllustration(), 'http:') > -1 ?
            $product->getIllustration() : (file_exists($this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'products' .
                DS . date('Y-m-d', strtotime($product->getCreatedTime())) . DS . $product->getIllustration()) & $product->getIllustration() != '' ?
                $this->config->application->baseUri . 'uploads/products/' . date('Y-m-d', strtotime($product->getCreatedTime())) . '/' . $product->getIllustration() :
                $this->config->application->baseUri . 'uploads/products/no-image.png');

        $this->view->setVars(
            [
                'product' => $product,
                'currencies' => $currencies,
                'brands' => $brands,
                'models' => \ProductModels::find(['conditions' => 'brand=' . ((int)$product->getBrandId() ? $product->getBrandId() : 0)]),
                'years' => \ProductYears::find(['conditions' => 'model_id=' . ((int)$product->getModelId() ? $product->getModelId() : 0)]),
                'req' => $req,
                'product_illustration' => $product_illustration,
                'status' => \ProductsExt::statusArr(),
                'category_options' => $category_options,
                'seo' => $seo ? $seo : new \SeoExt(),
                'properties' => $product_vehicle,
                'action' => 'edit'
            ]
        );

    }

    private function __loadModel($brand_id = 0)
    {
        $models = \ProductModels::find([
            'conditions' => 'brand=' . $brand_id,
            'order' => 'model_name ASC',
            'columns' => 'id, model_name AS name'
        ]);

        echo json_encode(\ModelHelper::toArray($models, 'id'));
    }

    private function __loadYear($model_id = 0)
    {
        $years = \ProductYearsExt::find([
            'columns' => 'id, year, end_year',
            'conditions' => 'model_id=' . $model_id,
            'order' => 'year'
        ]);

        if (count($years)) {
            foreach ($years as $k => $y) {
                $result[$k]['id'] = $y->id;
                $result[$k]['year'] = $y->year . ' - ' . $y->end_year;
            }
        }

        echo json_encode($result); //json_encode(\ModelHelper::toArray($years, 'id'));
    }

    /**
     * @param int $id
     */
    public function deleteAction($id = 0)
    {
        $this->view->disable();

        if ($this->request->isPost()) {
            // Delete by list
            $ids = $this->request->getPost('ids');
            if ($ids) {
                $products = \ProductsExt::find("id IN ($ids)");
                if (count($products)) {
                    foreach ($products as $p) {
                        if ($p instanceof \ProductsExt) {
                            if ($p->delete()) {
                                // Unlink image
                                if (file_exists($file = $this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'products' .
                                    DS . date('Y-m-d', strtotime($p->getCreatedTime())) . DS . $p->getIllustration())
                                )
                                    unlink($file);
                            }
                        }
                    }
                    $this->flashSession->success($this->_getTranslation()->_($this->closeHtmlButton . 'Delete list success!'));
                } else {
                    $this->flashSession->warning($this->_getTranslation()->_($this->closeHtmlButton . 'No product has been found!'));
                }
            }
            $this->response->redirect('admin/product/index');

            return;
        }

        // Option to display flash message
        $product = \ProductsExt::findFirst($id);
        if ($product instanceof \ProductsExt) {
            if (!$product->delete()) {
                $this->flashSession->error($this->_getTranslation()->_('Delete error!'));
            } else {
                $this->flashSession->success($this->_getTranslation()->_('Delete success!'));
                // Unlink image
                if (file_exists($file = $this->config->application->baseDir . 'public' . DS . 'uploads' . DS . 'products' .
                    DS . date('Y-m-d', strtotime($product->getCreatedTime())) . DS . $product->getIllustration())
                )
                    unlink($file);
            }
        } else {
            $this->flashSession->warning($this->_getTranslation()->_('Data not found!'));
        }

        $this->response->redirect('admin/product/index');
    }

    /**
     * @param $product_id
     * @param $selected
     */
    private function __saveProperties($product_id, $selected)
    {
        // Load all properties of this product
        $product_prop = \ProductVehicleExt::find('product_id=' . $product_id);

        // Check current properties of this product
        if (count($product_prop)) {

            foreach ($product_prop as $v) {
                if ($v instanceof \ProductVehicleExt) {
                    $mix = [];
                    if ($v->getBrand()) {
                        $mix[] = $v->getBrand();
                    }
                    if ($v->getModel()) {
                        $mix[] = $v->getModel();
                    }
                    if ($v->getYear()) {
                        $mix[] = $v->getYear();
                    }

                    if ($key = array_search(implode(',', $mix), $selected)) {
                        unset($selected[$key]);
                    } else {
                        // Delete this property
                        $v->delete();
                    }
                } // End if
            } // End for

        }

        if (!empty($selected)) {
            // Insert new properties
            foreach ($selected as $value) {

                $label = [];
                $prop = explode(',', $value); // brand, model, year

                $propObj = new \ProductVehicleExt();

                $propObj->setProductId($product_id);

                $propObj->setBrand($prop[0]);
                $brand = \ProductBrandsExt::findFirst($prop[0]);
                if ($brand instanceof \ProductBrandsExt) {
                    $label[] = $brand->getBrandName();
                }

                if (isset($prop[1])) {
                    $propObj->setModel($prop[1]);
                    $model = \ProductModelsExt::findFirst($prop[1]);
                    if ($model instanceof \ProductModelsExt) {
                        $label[] = $model->getModelName();
                    }
                }

                if (isset($prop[2])) {
                    $propObj->setYear($prop[2]);
                    $year = \ProductYearsExt::findFirst($prop[2]);
                    if ($year instanceof \ProductYearsExt) {
                        $label[] = $year->getYear() . ($year->getEndYear() ? ' - ' . $year->getEndYear() : '');
                        $propObj->setStartYear($year->getYear());
                        $propObj->setEndYear($year->getEndYear());
                    }
                }

                $propObj->setLabel(implode(', ', $label));

                // Save new property
                $propObj->save();
            }
        }
    }

    /**
     * @param bool $isAjax
     * @return array
     */
    private function __upload($isAjax = true)
    {
        // Upload image
        require_once $this->config->application->helper . DS . 'FileUpload.php';
        $upload_dir = $this->config->application->uploadDir . 'tmp' . DS;

        $valid_extensions = array('jpg', 'png', 'gif', 'jpeg');
        $upload = new \FileUpload('img_file');

        $result = $upload->handleUpload($upload_dir, $valid_extensions);
        if ($isAjax) {
            if ($result) {
                echo json_encode([
                    'success' => true,
                    'file_name' => $upload->getFileName(),
                    'img_url' => $this->config->application->baseUri . 'public/uploads/tmp/' . $upload->getFileName(),
                    'msg' => '<div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Upload file "' . $upload->getFileName() . '" success</div>'
                ]);
            } else {
                echo json_encode([
                    'success' => false,
                    'msg' => '<div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . $upload->getErrorMsg() . '</div>'
                ]);
            }
        } else {
            if ($result) {
                return [
                    'success' => true
                ];
            } else {
                return [
                    'success' => false,
                    'msg' => $upload->getErrorMsg()
                ];
            }
        } // -------------

    }
}