<?php

class ArticlesExt extends Articles
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_DENIED = 2;
    const STATUS_DELETED = 3;

    public static function statusArr() {
        return [
            self::STATUS_PENDING => "PENDING",
            self::STATUS_APPROVED => 'APPROVED',
            self::STATUS_DENIED => 'DENIED'
        ];
    }

    public function getSource() {
        return 'articles';
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_at', 'update_at'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'update_at',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
    }

    public function beforeValidation() {

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'title',
            'message' => 'Title must be required'
        ]));

        return $this->validationHasFailed() != true;
    }
}
