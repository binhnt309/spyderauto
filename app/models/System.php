<?php

class System extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $keyword;

    /**
     *
     * @var string
     */
    protected $alias;

    /**
     *
     * @var string
     */
    protected $value;

    /**
     *
     * @var string
     */
    protected $des;

    /**
     *
     * @var integer
     */
    protected $pos;

    /**
     *
     * @var string
     */
    protected $link;

    /**
     *
     * @var string
     */
    protected $field_type;

    /**
     * Method to set the value of field keyword
     *
     * @param string $keyword
     * @return $this
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Method to set the value of field alias
     *
     * @param string $alias
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Method to set the value of field value
     *
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Method to set the value of field des
     *
     * @param string $des
     * @return $this
     */
    public function setDes($des)
    {
        $this->des = $des;

        return $this;
    }

    /**
     * Method to set the value of field pos
     *
     * @param integer $pos
     * @return $this
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Method to set the value of field link
     *
     * @param string $link
     * @return $this
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Method to set the value of field field_type
     *
     * @param string $field_type
     * @return $this
     */
    public function setFieldType($field_type)
    {
        $this->field_type = $field_type;

        return $this;
    }

    /**
     * Returns the value of field keyword
     *
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Returns the value of field alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Returns the value of field value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Returns the value of field des
     *
     * @return string
     */
    public function getDes()
    {
        return $this->des;
    }

    /**
     * Returns the value of field pos
     *
     * @return integer
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Returns the value of field link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Returns the value of field field_type
     *
     * @return string
     */
    public function getFieldType()
    {
        return $this->field_type;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'keyword' => 'keyword', 
            'alias' => 'alias', 
            'value' => 'value', 
            'des' => 'des', 
            'pos' => 'pos', 
            'link' => 'link', 
            'field_type' => 'field_type'
        );
    }

}
