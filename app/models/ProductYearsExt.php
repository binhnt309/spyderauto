<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */

class ProductYearsExt extends ProductYears{

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;
    const STATUS_DELETED = 2;

    public static function statusArr() {
        return [
            self::STATUS_DISABLED => 'DISABLED',
            self::STATUS_ENABLED => 'ENABLED'
        ];
    }

    public function getSource() {
        return 'product_years';
    }

    static function getTable() {
        $instance = new ProductYears();
        return $instance->getSource();
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));

        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\SoftDelete(
            [
                'field' => 'status',
                'value' => self::STATUS_DELETED
            ]
        ));
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation() {

        /*$this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => [
                'year',
                'brand_id',
                'model_id'
            ],
            'message' => 'Year belong the model and brand must be unique!'
        ]));*/

        return $this->validationHasFailed() != true;
    }

}