<?php

class ShippingFee extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $country_id;

    /**
     *
     * @var string
     */
    protected $country_name;

    /**
     *
     * @var integer
     */
    protected $city_id;

    /**
     *
     * @var string
     */
    protected $city_name;

    /**
     *
     * @var double
     */
    protected $amount;

    /**
     *
     * @var integer
     */
    protected $currency_id;

    /**
     *
     * @var string
     */
    protected $currency_symbol;

    /**
     *
     * @var string
     */
    protected $time_delivery;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field country_id
     *
     * @param integer $country_id
     * @return $this
     */
    public function setCountryId($country_id)
    {
        $this->country_id = $country_id;

        return $this;
    }

    /**
     * Method to set the value of field country_name
     *
     * @param string $country_name
     * @return $this
     */
    public function setCountryName($country_name)
    {
        $this->country_name = $country_name;

        return $this;
    }

    /**
     * Method to set the value of field city_id
     *
     * @param integer $city_id
     * @return $this
     */
    public function setCityId($city_id)
    {
        $this->city_id = $city_id;

        return $this;
    }

    /**
     * Method to set the value of field city_name
     *
     * @param string $city_name
     * @return $this
     */
    public function setCityName($city_name)
    {
        $this->city_name = $city_name;

        return $this;
    }

    /**
     * Method to set the value of field amount
     *
     * @param double $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Method to set the value of field currency_id
     *
     * @param integer $currency_id
     * @return $this
     */
    public function setCurrencyId($currency_id)
    {
        $this->currency_id = $currency_id;

        return $this;
    }

    /**
     * Method to set the value of field currency_symbol
     *
     * @param string $currency_symbol
     * @return $this
     */
    public function setCurrencySymbol($currency_symbol)
    {
        $this->currency_symbol = $currency_symbol;

        return $this;
    }

    /**
     * Method to set the value of field time_delivery
     *
     * @param string $time_delivery
     * @return $this
     */
    public function setTimeDelivery($time_delivery)
    {
        $this->time_delivery = $time_delivery;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field country_id
     *
     * @return integer
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Returns the value of field country_name
     *
     * @return string
     */
    public function getCountryName()
    {
        return $this->country_name;
    }

    /**
     * Returns the value of field city_id
     *
     * @return integer
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * Returns the value of field city_name
     *
     * @return string
     */
    public function getCityName()
    {
        return $this->city_name;
    }

    /**
     * Returns the value of field amount
     *
     * @return double
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Returns the value of field currency_id
     *
     * @return integer
     */
    public function getCurrencyId()
    {
        return $this->currency_id;
    }

    /**
     * Returns the value of field currency_symbol
     *
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->currency_symbol;
    }

    /**
     * Returns the value of field time_delivery
     *
     * @return string
     */
    public function getTimeDelivery()
    {
        return $this->time_delivery;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'country_id' => 'country_id', 
            'country_name' => 'country_name', 
            'city_id' => 'city_id', 
            'city_name' => 'city_name', 
            'amount' => 'amount', 
            'currency_id' => 'currency_id', 
            'currency_symbol' => 'currency_symbol', 
            'time_delivery' => 'time_delivery'
        );
    }

}
