<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */

class ProductsExt extends Products
{

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_UNPUBLISHED = 2;
    const STATUS_DELETED = 3;

    public static function statusArr()
    {
        return [
            self::STATUS_PENDING => 'Pending',
            self::STATUS_APPROVED => 'Approved',
            self::STATUS_UNPUBLISHED => 'UnPublished'
        ];
    }

    public function getSource()
    {
        return 'products';
    }

    static function getTable() {
        $instance = new Products();
        return $instance->getSource();
    }

    public function initialize()
    {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => [
                    'field' => [
                        'created_time',
                        'modified_time'
                    ],
                    'format' => 'Y-m-d H:i:s'
                ],
                'beforeValidateOnUpdate' => [
                    'field' => ['modified_time'],
                    'format' => 'Y-m-d H:i:s'
                ]
            )
        ));

    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation()
    {
        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => ['sku'],
            'message' => 'SKU has been existed'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'sku',
            'message' => 'SKU is not be empty'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'title',
            'message'=> 'Title is not be empty'
        ]));

        return $this->validationHasFailed() != true;
    }

}