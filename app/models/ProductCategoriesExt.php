<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */

class ProductCategoriesExt extends ProductCategories{

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_UNPUBLISHED = 2;
    const STATUS_DELETED = 3;

    public static function statusArr()
    {
        return [
            self::STATUS_PENDING => 'Pending',
            self::STATUS_APPROVED => 'Approved',
            self::STATUS_UNPUBLISHED => 'UnPublished'
        ];
    }

    public function getSource() {
        return 'product_categories';
    }

    static function getTable() {
        $instance = new ProductCategories();
        return $instance->getSource();
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation()
    {

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'name',
            'message' => 'Name can\'t be blank!'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'slug',
            'message' => 'Slug can\'t be blank!'
        ]));

        return $this->validationHasFailed() != true;
    }

}