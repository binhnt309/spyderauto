<?php

class Orders extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $customer_id;

    /**
     *
     * @var string
     */
    protected $code;

    /**
     *
     * @var string
     */
    protected $customer_name;

    /**
     *
     * @var string
     */
    protected $customer_email;

    /**
     *
     * @var string
     */
    protected $customer_phone;

    /**
     *
     * @var string
     */
    protected $customer_mobile;

    /**
     *
     * @var string
     */
    protected $customer_address;

    /**
     *
     * @var string
     */
    protected $customer_address_2;

    /**
     *
     * @var integer
     */
    protected $customer_district;

    /**
     *
     * @var string
     */
    protected $district_label;

    /**
     *
     * @var integer
     */
    protected $customer_province;

    /**
     *
     * @var string
     */
    protected $province_label;

    /**
     *
     * @var string
     */
    protected $zipcode;

    /**
     *
     * @var integer
     */
    protected $customer_country;

    /**
     *
     * @var string
     */
    protected $country_label;

    /**
     *
     * @var integer
     */
    protected $amount;

    /**
     *
     * @var integer
     */
    protected $total_items;

    /**
     *
     * @var integer
     */
    protected $shipping_fee;

    /**
     *
     * @var double
     */
    protected $fee_amount;

    /**
     *
     * @var string
     */
    protected $promotion_code;

    /**
     *
     * @var string
     */
    protected $discount;

    /**
     *
     * @var string
     */
    protected $created_at;

    /**
     *
     * @var string
     */
    protected $modified_at;

    /**
     *
     * @var integer
     */
    protected $status;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field customer_id
     *
     * @param string $customer_id
     * @return $this
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;

        return $this;
    }

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Method to set the value of field customer_name
     *
     * @param string $customer_name
     * @return $this
     */
    public function setCustomerName($customer_name)
    {
        $this->customer_name = $customer_name;

        return $this;
    }

    /**
     * Method to set the value of field customer_email
     *
     * @param string $customer_email
     * @return $this
     */
    public function setCustomerEmail($customer_email)
    {
        $this->customer_email = $customer_email;

        return $this;
    }

    /**
     * Method to set the value of field customer_phone
     *
     * @param string $customer_phone
     * @return $this
     */
    public function setCustomerPhone($customer_phone)
    {
        $this->customer_phone = $customer_phone;

        return $this;
    }

    /**
     * Method to set the value of field customer_mobile
     *
     * @param string $customer_mobile
     * @return $this
     */
    public function setCustomerMobile($customer_mobile)
    {
        $this->customer_mobile = $customer_mobile;

        return $this;
    }

    /**
     * Method to set the value of field customer_address
     *
     * @param string $customer_address
     * @return $this
     */
    public function setCustomerAddress($customer_address)
    {
        $this->customer_address = $customer_address;

        return $this;
    }

    /**
     * Method to set the value of field customer_address_2
     *
     * @param string $customer_address_2
     * @return $this
     */
    public function setCustomerAddress2($customer_address_2)
    {
        $this->customer_address_2 = $customer_address_2;

        return $this;
    }

    /**
     * Method to set the value of field customer_district
     *
     * @param integer $customer_district
     * @return $this
     */
    public function setCustomerDistrict($customer_district)
    {
        $this->customer_district = $customer_district;

        return $this;
    }

    /**
     * Method to set the value of field district_label
     *
     * @param string $district_label
     * @return $this
     */
    public function setDistrictLabel($district_label)
    {
        $this->district_label = $district_label;

        return $this;
    }

    /**
     * Method to set the value of field customer_province
     *
     * @param integer $customer_province
     * @return $this
     */
    public function setCustomerProvince($customer_province)
    {
        $this->customer_province = $customer_province;

        return $this;
    }

    /**
     * Method to set the value of field province_label
     *
     * @param string $province_label
     * @return $this
     */
    public function setProvinceLabel($province_label)
    {
        $this->province_label = $province_label;

        return $this;
    }

    /**
     * Method to set the value of field zipcode
     *
     * @param string $zipcode
     * @return $this
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Method to set the value of field customer_country
     *
     * @param integer $customer_country
     * @return $this
     */
    public function setCustomerCountry($customer_country)
    {
        $this->customer_country = $customer_country;

        return $this;
    }

    /**
     * Method to set the value of field country_label
     *
     * @param string $country_label
     * @return $this
     */
    public function setCountryLabel($country_label)
    {
        $this->country_label = $country_label;

        return $this;
    }

    /**
     * Method to set the value of field amount
     *
     * @param integer $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Method to set the value of field total_items
     *
     * @param integer $total_items
     * @return $this
     */
    public function setTotalItems($total_items)
    {
        $this->total_items = $total_items;

        return $this;
    }

    /**
     * Method to set the value of field shipping_fee
     *
     * @param integer $shipping_fee
     * @return $this
     */
    public function setShippingFee($shipping_fee)
    {
        $this->shipping_fee = $shipping_fee;

        return $this;
    }

    /**
     * Method to set the value of field fee_amount
     *
     * @param double $fee_amount
     * @return $this
     */
    public function setFeeAmount($fee_amount)
    {
        $this->fee_amount = $fee_amount;

        return $this;
    }

    /**
     * Method to set the value of field promotion_code
     *
     * @param string $promotion_code
     * @return $this
     */
    public function setPromotionCode($promotion_code)
    {
        $this->promotion_code = $promotion_code;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param string $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Method to set the value of field created_at
     *
     * @param string $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Method to set the value of field modified_at
     *
     * @param string $modified_at
     * @return $this
     */
    public function setModifiedAt($modified_at)
    {
        $this->modified_at = $modified_at;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field customer_id
     *
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Returns the value of field customer_name
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customer_name;
    }

    /**
     * Returns the value of field customer_email
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    /**
     * Returns the value of field customer_phone
     *
     * @return string
     */
    public function getCustomerPhone()
    {
        return $this->customer_phone;
    }

    /**
     * Returns the value of field customer_mobile
     *
     * @return string
     */
    public function getCustomerMobile()
    {
        return $this->customer_mobile;
    }

    /**
     * Returns the value of field customer_address
     *
     * @return string
     */
    public function getCustomerAddress()
    {
        return $this->customer_address;
    }

    /**
     * Returns the value of field customer_address_2
     *
     * @return string
     */
    public function getCustomerAddress2()
    {
        return $this->customer_address_2;
    }

    /**
     * Returns the value of field customer_district
     *
     * @return integer
     */
    public function getCustomerDistrict()
    {
        return $this->customer_district;
    }

    /**
     * Returns the value of field district_label
     *
     * @return string
     */
    public function getDistrictLabel()
    {
        return $this->district_label;
    }

    /**
     * Returns the value of field customer_province
     *
     * @return integer
     */
    public function getCustomerProvince()
    {
        return $this->customer_province;
    }

    /**
     * Returns the value of field province_label
     *
     * @return string
     */
    public function getProvinceLabel()
    {
        return $this->province_label;
    }

    /**
     * Returns the value of field zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Returns the value of field customer_country
     *
     * @return integer
     */
    public function getCustomerCountry()
    {
        return $this->customer_country;
    }

    /**
     * Returns the value of field country_label
     *
     * @return string
     */
    public function getCountryLabel()
    {
        return $this->country_label;
    }

    /**
     * Returns the value of field amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Returns the value of field total_items
     *
     * @return integer
     */
    public function getTotalItems()
    {
        return $this->total_items;
    }

    /**
     * Returns the value of field shipping_fee
     *
     * @return integer
     */
    public function getShippingFee()
    {
        return $this->shipping_fee;
    }

    /**
     * Returns the value of field fee_amount
     *
     * @return double
     */
    public function getFeeAmount()
    {
        return $this->fee_amount;
    }

    /**
     * Returns the value of field promotion_code
     *
     * @return string
     */
    public function getPromotionCode()
    {
        return $this->promotion_code;
    }

    /**
     * Returns the value of field discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Returns the value of field created_at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns the value of field modified_at
     *
     * @return string
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Returns the value of field status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'customer_id' => 'customer_id', 
            'code' => 'code', 
            'customer_name' => 'customer_name', 
            'customer_email' => 'customer_email', 
            'customer_phone' => 'customer_phone', 
            'customer_mobile' => 'customer_mobile', 
            'customer_address' => 'customer_address', 
            'customer_address_2' => 'customer_address_2', 
            'customer_district' => 'customer_district', 
            'district_label' => 'district_label', 
            'customer_province' => 'customer_province', 
            'province_label' => 'province_label', 
            'zipcode' => 'zipcode', 
            'customer_country' => 'customer_country', 
            'country_label' => 'country_label', 
            'amount' => 'amount', 
            'total_items' => 'total_items', 
            'shipping_fee' => 'shipping_fee', 
            'fee_amount' => 'fee_amount', 
            'promotion_code' => 'promotion_code', 
            'discount' => 'discount', 
            'created_at' => 'created_at', 
            'modified_at' => 'modified_at', 
            'status' => 'status'
        );
    }

}
