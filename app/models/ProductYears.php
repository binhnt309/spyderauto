<?php

class ProductYears extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $year;

    /**
     *
     * @var integer
     */
    protected $end_year;

    /**
     *
     * @var integer
     */
    protected $model_id;

    /**
     *
     * @var integer
     */
    protected $brand_id;

    /**
     *
     * @var integer
     */
    protected $total_items;

    /**
     *
     * @var string
     */
    protected $created_time;

    /**
     *
     * @var integer
     */
    protected $status;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field year
     *
     * @param integer $year
     * @return $this
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Method to set the value of field end_year
     *
     * @param integer $end_year
     * @return $this
     */
    public function setEndYear($end_year)
    {
        $this->end_year = $end_year;

        return $this;
    }

    /**
     * Method to set the value of field model_id
     *
     * @param integer $model_id
     * @return $this
     */
    public function setModelId($model_id)
    {
        $this->model_id = $model_id;

        return $this;
    }

    /**
     * Method to set the value of field brand_id
     *
     * @param integer $brand_id
     * @return $this
     */
    public function setBrandId($brand_id)
    {
        $this->brand_id = $brand_id;

        return $this;
    }

    /**
     * Method to set the value of field total_items
     *
     * @param integer $total_items
     * @return $this
     */
    public function setTotalItems($total_items)
    {
        $this->total_items = $total_items;

        return $this;
    }

    /**
     * Method to set the value of field created_time
     *
     * @param string $created_time
     * @return $this
     */
    public function setCreatedTime($created_time)
    {
        $this->created_time = $created_time;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Returns the value of field end_year
     *
     * @return integer
     */
    public function getEndYear()
    {
        return $this->end_year;
    }

    /**
     * Returns the value of field model_id
     *
     * @return integer
     */
    public function getModelId()
    {
        return $this->model_id;
    }

    /**
     * Returns the value of field brand_id
     *
     * @return integer
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * Returns the value of field total_items
     *
     * @return integer
     */
    public function getTotalItems()
    {
        return $this->total_items;
    }

    /**
     * Returns the value of field created_time
     *
     * @return string
     */
    public function getCreatedTime()
    {
        return $this->created_time;
    }

    /**
     * Returns the value of field status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'year' => 'year', 
            'end_year' => 'end_year', 
            'model_id' => 'model_id', 
            'brand_id' => 'brand_id', 
            'total_items' => 'total_items', 
            'created_time' => 'created_time', 
            'status' => 'status'
        );
    }

}
