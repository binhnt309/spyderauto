<?php

class ContactExt extends Contact
{
    public function getSource() {
        return 'contact';
    }

    static function getTable() {
        $instance = new ContactExt();
        return $instance->getSource();
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time'//, 'modified_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                )/*,
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_time',
                    'format' => 'Y-m-d H:i:s'
                )*/
            )
        ));
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation() {

        return $this->validationHasFailed() != true;
    }

}
