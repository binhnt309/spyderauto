<?php

class BannersExt extends Banners
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;

    public static function statusArr()
    {
        return [
            self::STATUS_PENDING => 'Pending',
            self::STATUS_APPROVED => 'Approved'
        ];
    }
    public function getSource() {
        return 'banners';
    }

    static function getTable() {
        $instance = new Banners();
        return $instance->getSource();
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_at', 'updated_at'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'updated_at',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation() {
        return true;
    }

}
