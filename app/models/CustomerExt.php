<?php

class CustomerExt extends Customers
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_SUSPEND = 2;
    const STATUS_DENIED = 3;

    public static function statusArr()
    {
        return [
            self::STATUS_PENDING => 'Pending',
            self::STATUS_APPROVED => 'Approved',
            self::STATUS_SUSPEND => 'Suspend',
            self::STATUS_DENIED => 'Denied'
        ];
    }

    public function getSource()
    {
        return 'customers';
    }

    static function getTable()
    {
        $instance = new CustomerExt();
        return $instance->getSource();
    }

    public function initialize()
    {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time', 'modified_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_time',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation()
    {
        $this->validation(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'username',
            'message' => 'Username is not be blank'
        ]));

        $this->validation(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => 'username',
            'message' => 'Username has been exist!'
        ]));

        $this->validation(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'email',
            'message' => 'Email is not be blank'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => [
                'email'
            ],
            'message' => 'Email has been exist!'
        ]));

        return $this->validationHasFailed() != true;
    }

}
