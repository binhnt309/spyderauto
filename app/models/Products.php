<?php

class Products extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $title;

    /**
     *
     * @var string
     */
    protected $sku;

    /**
     *
     * @var string
     */
    protected $alias;

    /**
     *
     * @var integer
     */
    protected $cid;

    /**
     *
     * @var integer
     */
    protected $is_front;

    /**
     *
     * @var integer
     */
    protected $is_feature;

    /**
     *
     * @var integer
     */
    protected $pos;

    /**
     *
     * @var integer
     */
    protected $brand_id;

    /**
     *
     * @var integer
     */
    protected $model_id;

    /**
     *
     * @var integer
     */
    protected $year_id;

    /**
     *
     * @var double
     */
    protected $price;

    /**
     *
     * @var double
     */
    protected $sale_off;

    /**
     *
     * @var integer
     */
    protected $currency_id;

    /**
     *
     * @var string
     */
    protected $sale_off_to;

    /**
     *
     * @var string
     */
    protected $brief;

    /**
     *
     * @var string
     */
    protected $des;

    /**
     *
     * @var string
     */
    protected $thumbs;

    /**
     *
     * @var string
     */
    protected $illustration;

    /**
     *
     * @var string
     */
    protected $main_image;

    /**
     *
     * @var integer
     */
    protected $stock;

    /**
     *
     * @var integer
     */
    protected $sale;

    /**
     *
     * @var string
     */
    protected $tags;

    /**
     *
     * @var string
     */
    protected $created_time;

    /**
     *
     * @var string
     */
    protected $modified_time;

    /**
     *
     * @var integer
     */
    protected $status;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Method to set the value of field sku
     *
     * @param string $sku
     * @return $this
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Method to set the value of field alias
     *
     * @param string $alias
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Method to set the value of field cid
     *
     * @param integer $cid
     * @return $this
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Method to set the value of field is_front
     *
     * @param integer $is_front
     * @return $this
     */
    public function setIsFront($is_front)
    {
        $this->is_front = $is_front;

        return $this;
    }

    /**
     * Method to set the value of field is_feature
     *
     * @param integer $is_feature
     * @return $this
     */
    public function setIsFeature($is_feature)
    {
        $this->is_feature = $is_feature;

        return $this;
    }

    /**
     * Method to set the value of field pos
     *
     * @param integer $pos
     * @return $this
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Method to set the value of field brand_id
     *
     * @param integer $brand_id
     * @return $this
     */
    public function setBrandId($brand_id)
    {
        $this->brand_id = $brand_id;

        return $this;
    }

    /**
     * Method to set the value of field model_id
     *
     * @param integer $model_id
     * @return $this
     */
    public function setModelId($model_id)
    {
        $this->model_id = $model_id;

        return $this;
    }

    /**
     * Method to set the value of field year_id
     *
     * @param integer $year_id
     * @return $this
     */
    public function setYearId($year_id)
    {
        $this->year_id = $year_id;

        return $this;
    }

    /**
     * Method to set the value of field price
     *
     * @param double $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Method to set the value of field sale_off
     *
     * @param double $sale_off
     * @return $this
     */
    public function setSaleOff($sale_off)
    {
        $this->sale_off = $sale_off;

        return $this;
    }

    /**
     * Method to set the value of field currency_id
     *
     * @param integer $currency_id
     * @return $this
     */
    public function setCurrencyId($currency_id)
    {
        $this->currency_id = $currency_id;

        return $this;
    }

    /**
     * Method to set the value of field sale_off_to
     *
     * @param string $sale_off_to
     * @return $this
     */
    public function setSaleOffTo($sale_off_to)
    {
        $this->sale_off_to = $sale_off_to;

        return $this;
    }

    /**
     * Method to set the value of field brief
     *
     * @param string $brief
     * @return $this
     */
    public function setBrief($brief)
    {
        $this->brief = $brief;

        return $this;
    }

    /**
     * Method to set the value of field des
     *
     * @param string $des
     * @return $this
     */
    public function setDes($des)
    {
        $this->des = $des;

        return $this;
    }

    /**
     * Method to set the value of field thumbs
     *
     * @param string $thumbs
     * @return $this
     */
    public function setThumbs($thumbs)
    {
        $this->thumbs = $thumbs;

        return $this;
    }

    /**
     * Method to set the value of field illustration
     *
     * @param string $illustration
     * @return $this
     */
    public function setIllustration($illustration)
    {
        $this->illustration = $illustration;

        return $this;
    }

    /**
     * Method to set the value of field main_image
     *
     * @param string $main_image
     * @return $this
     */
    public function setMainImage($main_image)
    {
        $this->main_image = $main_image;

        return $this;
    }

    /**
     * Method to set the value of field stock
     *
     * @param integer $stock
     * @return $this
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Method to set the value of field sale
     *
     * @param integer $sale
     * @return $this
     */
    public function setSale($sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Method to set the value of field tags
     *
     * @param string $tags
     * @return $this
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Method to set the value of field created_time
     *
     * @param string $created_time
     * @return $this
     */
    public function setCreatedTime($created_time)
    {
        $this->created_time = $created_time;

        return $this;
    }

    /**
     * Method to set the value of field modified_time
     *
     * @param string $modified_time
     * @return $this
     */
    public function setModifiedTime($modified_time)
    {
        $this->modified_time = $modified_time;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns the value of field sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Returns the value of field alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Returns the value of field cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Returns the value of field is_front
     *
     * @return integer
     */
    public function getIsFront()
    {
        return $this->is_front;
    }

    /**
     * Returns the value of field is_feature
     *
     * @return integer
     */
    public function getIsFeature()
    {
        return $this->is_feature;
    }

    /**
     * Returns the value of field pos
     *
     * @return integer
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Returns the value of field brand_id
     *
     * @return integer
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * Returns the value of field model_id
     *
     * @return integer
     */
    public function getModelId()
    {
        return $this->model_id;
    }

    /**
     * Returns the value of field year_id
     *
     * @return integer
     */
    public function getYearId()
    {
        return $this->year_id;
    }

    /**
     * Returns the value of field price
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Returns the value of field sale_off
     *
     * @return double
     */
    public function getSaleOff()
    {
        return $this->sale_off;
    }

    /**
     * Returns the value of field currency_id
     *
     * @return integer
     */
    public function getCurrencyId()
    {
        return $this->currency_id;
    }

    /**
     * Returns the value of field sale_off_to
     *
     * @return string
     */
    public function getSaleOffTo()
    {
        return $this->sale_off_to;
    }

    /**
     * Returns the value of field brief
     *
     * @return string
     */
    public function getBrief()
    {
        return $this->brief;
    }

    /**
     * Returns the value of field des
     *
     * @return string
     */
    public function getDes()
    {
        return $this->des;
    }

    /**
     * Returns the value of field thumbs
     *
     * @return string
     */
    public function getThumbs()
    {
        return $this->thumbs;
    }

    /**
     * Returns the value of field illustration
     *
     * @return string
     */
    public function getIllustration()
    {
        return $this->illustration;
    }

    /**
     * Returns the value of field main_image
     *
     * @return string
     */
    public function getMainImage()
    {
        return $this->main_image;
    }

    /**
     * Returns the value of field stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Returns the value of field sale
     *
     * @return integer
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * Returns the value of field tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Returns the value of field created_time
     *
     * @return string
     */
    public function getCreatedTime()
    {
        return $this->created_time;
    }

    /**
     * Returns the value of field modified_time
     *
     * @return string
     */
    public function getModifiedTime()
    {
        return $this->modified_time;
    }

    /**
     * Returns the value of field status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'products';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Products[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Products
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
