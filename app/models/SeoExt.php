<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */

class SeoExt extends Seo{

    const TYPE_PRODUCT = 2;
    const TYPE_CATEGORY = 1;
    const TYPE_ART_CATEGORY = 3;
    const TYPE_ARTICLE = 4;

    public function getSource() {
        return 'seo';
    }

    static function getTable() {
        $instance = new ProductModels();
        return $instance->getSource();
    }

    public function initialize()
    {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => [
                    'field' => [
                        'created_time',
                        'modified_time'
                    ],
                    'format' => 'Y-m-d H:i:s'
                ],
                'beforeValidateOnUpdate' => [
                    'field' => ['modified_time'],
                    'format' => 'Y-m-d H:i:s'
                ]
            )
        ));

    }

    /**
     * @return bool
     */
    public function beforeValidation(){
        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => [
                'type',
                'object_id'
            ],
            'message' => 'ID belong to this type has been exist!'
        ]));
        return $this->validationHasFailed() != true;
    }
}