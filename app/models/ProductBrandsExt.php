<?php

/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */
class ProductBrandsExt extends ProductBrands
{

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    public static function statusArr()
    {
        return [self::STATUS_DISABLED => 'DISABLED', self::STATUS_ENABLED => 'ENABLED'];
    }

    public function getSource()
    {
        return 'product_brands';
    }

    static function getTable() {
        $instance = new ProductBrands();
        return $instance->getSource();
    }

    /**
     * @return bool
     */
    public function beforeValidation(){
        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => [
                'brand_name'
            ],
            'message' => 'Brand name must be unique!'
        ]));
        return $this->validationHasFailed() != true;
    }
}