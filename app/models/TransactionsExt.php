<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:07 PM
 */

class TransactionsExt extends Transactions{

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REFUND = 2;

    public static function statusArr(){
        return [
            self::STATUS_PENDING => 'Pending',
            self::STATUS_APPROVED => 'Approved',
            self::STATUS_REFUND => 'Refund'
        ];
    }

    public function getSource() {
        return 'transactions';
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_at', 'modified_at'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_at',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
    }

}