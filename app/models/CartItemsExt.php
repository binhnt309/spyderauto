<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */

class CartItemsExt extends \CartItems {

    public function getSource() {
        return 'cart_items';
    }
    
    static function getTable() {
        $instance = new CartItemsExt();
        return $instance->getSource();
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time', 'modified_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_time',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
    }

    public function beforeValidation() {

        return $this->validationHasFailed() != true;
    }

}