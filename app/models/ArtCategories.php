<?php

class ArtCategories extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $alias;

    /**
     *
     * @var integer
     */
    protected $parent_id;

    /**
     *
     * @var integer
     */
    protected $in_menu;

    /**
     *
     * @var integer
     */
    protected $in_footer;

    /**
     *
     * @var integer
     */
    protected $pos;

    /**
     *
     * @var integer
     */
    protected $total_items;

    /**
     *
     * @var string
     */
    protected $image;

    /**
     *
     * @var string
     */
    protected $fix_link;

    /**
     *
     * @var integer
     */
    protected $status;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field alias
     *
     * @param string $alias
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Method to set the value of field parent_id
     *
     * @param integer $parent_id
     * @return $this
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;

        return $this;
    }

    /**
     * Method to set the value of field in_menu
     *
     * @param integer $in_menu
     * @return $this
     */
    public function setInMenu($in_menu)
    {
        $this->in_menu = $in_menu;

        return $this;
    }

    /**
     * Method to set the value of field in_footer
     *
     * @param integer $in_footer
     * @return $this
     */
    public function setInFooter($in_footer)
    {
        $this->in_footer = $in_footer;

        return $this;
    }

    /**
     * Method to set the value of field pos
     *
     * @param integer $pos
     * @return $this
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Method to set the value of field total_items
     *
     * @param integer $total_items
     * @return $this
     */
    public function setTotalItems($total_items)
    {
        $this->total_items = $total_items;

        return $this;
    }

    /**
     * Method to set the value of field image
     *
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Method to set the value of field fix_link
     *
     * @param string $fix_link
     * @return $this
     */
    public function setFixLink($fix_link)
    {
        $this->fix_link = $fix_link;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Returns the value of field parent_id
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Returns the value of field in_menu
     *
     * @return integer
     */
    public function getInMenu()
    {
        return $this->in_menu;
    }

    /**
     * Returns the value of field in_footer
     *
     * @return integer
     */
    public function getInFooter()
    {
        return $this->in_footer;
    }

    /**
     * Returns the value of field pos
     *
     * @return integer
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Returns the value of field total_items
     *
     * @return integer
     */
    public function getTotalItems()
    {
        return $this->total_items;
    }

    /**
     * Returns the value of field image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Returns the value of field fix_link
     *
     * @return string
     */
    public function getFixLink()
    {
        return $this->fix_link;
    }

    /**
     * Returns the value of field status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'name' => 'name', 
            'alias' => 'alias', 
            'parent_id' => 'parent_id', 
            'in_menu' => 'in_menu', 
            'in_footer' => 'in_footer', 
            'pos' => 'pos', 
            'total_items' => 'total_items', 
            'image' => 'image', 
            'fix_link' => 'fix_link', 
            'status' => 'status'
        );
    }

}
