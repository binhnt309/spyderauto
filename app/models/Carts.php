<?php

class Carts extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $customer_id;

    /**
     *
     * @var string
     */
    protected $customer_name;

    /**
     *
     * @var string
     */
    protected $customer_email;

    /**
     *
     * @var string
     */
    protected $customer_phone;

    /**
     *
     * @var integer
     */
    protected $customer_address;

    /**
     *
     * @var string
     */
    protected $customer_address_2;

    /**
     *
     * @var integer
     */
    protected $customer_district;

    /**
     *
     * @var string
     */
    protected $district_label;

    /**
     *
     * @var integer
     */
    protected $customer_province;

    /**
     *
     * @var string
     */
    protected $province_label;

    /**
     *
     * @var string
     */
    protected $customer_country;

    /**
     *
     * @var integer
     */
    protected $country_label;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field customer_id
     *
     * @param integer $customer_id
     * @return $this
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;

        return $this;
    }

    /**
     * Method to set the value of field customer_name
     *
     * @param string $customer_name
     * @return $this
     */
    public function setCustomerName($customer_name)
    {
        $this->customer_name = $customer_name;

        return $this;
    }

    /**
     * Method to set the value of field customer_email
     *
     * @param string $customer_email
     * @return $this
     */
    public function setCustomerEmail($customer_email)
    {
        $this->customer_email = $customer_email;

        return $this;
    }

    /**
     * Method to set the value of field customer_phone
     *
     * @param string $customer_phone
     * @return $this
     */
    public function setCustomerPhone($customer_phone)
    {
        $this->customer_phone = $customer_phone;

        return $this;
    }

    /**
     * Method to set the value of field customer_address
     *
     * @param integer $customer_address
     * @return $this
     */
    public function setCustomerAddress($customer_address)
    {
        $this->customer_address = $customer_address;

        return $this;
    }

    /**
     * Method to set the value of field customer_address_2
     *
     * @param string $customer_address_2
     * @return $this
     */
    public function setCustomerAddress2($customer_address_2)
    {
        $this->customer_address_2 = $customer_address_2;

        return $this;
    }

    /**
     * Method to set the value of field customer_district
     *
     * @param integer $customer_district
     * @return $this
     */
    public function setCustomerDistrict($customer_district)
    {
        $this->customer_district = $customer_district;

        return $this;
    }

    /**
     * Method to set the value of field district_label
     *
     * @param string $district_label
     * @return $this
     */
    public function setDistrictLabel($district_label)
    {
        $this->district_label = $district_label;

        return $this;
    }

    /**
     * Method to set the value of field customer_province
     *
     * @param integer $customer_province
     * @return $this
     */
    public function setCustomerProvince($customer_province)
    {
        $this->customer_province = $customer_province;

        return $this;
    }

    /**
     * Method to set the value of field province_label
     *
     * @param string $province_label
     * @return $this
     */
    public function setProvinceLabel($province_label)
    {
        $this->province_label = $province_label;

        return $this;
    }

    /**
     * Method to set the value of field customer_country
     *
     * @param string $customer_country
     * @return $this
     */
    public function setCustomerCountry($customer_country)
    {
        $this->customer_country = $customer_country;

        return $this;
    }

    /**
     * Method to set the value of field country_label
     *
     * @param integer $country_label
     * @return $this
     */
    public function setCountryLabel($country_label)
    {
        $this->country_label = $country_label;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field customer_id
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Returns the value of field customer_name
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customer_name;
    }

    /**
     * Returns the value of field customer_email
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    /**
     * Returns the value of field customer_phone
     *
     * @return string
     */
    public function getCustomerPhone()
    {
        return $this->customer_phone;
    }

    /**
     * Returns the value of field customer_address
     *
     * @return integer
     */
    public function getCustomerAddress()
    {
        return $this->customer_address;
    }

    /**
     * Returns the value of field customer_address_2
     *
     * @return string
     */
    public function getCustomerAddress2()
    {
        return $this->customer_address_2;
    }

    /**
     * Returns the value of field customer_district
     *
     * @return integer
     */
    public function getCustomerDistrict()
    {
        return $this->customer_district;
    }

    /**
     * Returns the value of field district_label
     *
     * @return string
     */
    public function getDistrictLabel()
    {
        return $this->district_label;
    }

    /**
     * Returns the value of field customer_province
     *
     * @return integer
     */
    public function getCustomerProvince()
    {
        return $this->customer_province;
    }

    /**
     * Returns the value of field province_label
     *
     * @return string
     */
    public function getProvinceLabel()
    {
        return $this->province_label;
    }

    /**
     * Returns the value of field customer_country
     *
     * @return string
     */
    public function getCustomerCountry()
    {
        return $this->customer_country;
    }

    /**
     * Returns the value of field country_label
     *
     * @return integer
     */
    public function getCountryLabel()
    {
        return $this->country_label;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'customer_id' => 'customer_id', 
            'customer_name' => 'customer_name', 
            'customer_email' => 'customer_email', 
            'customer_phone' => 'customer_phone', 
            'customer_address' => 'customer_address', 
            'customer_address_2' => 'customer_address_2', 
            'customer_district' => 'customer_district', 
            'district_label' => 'district_label', 
            'customer_province' => 'customer_province', 
            'province_label' => 'province_label', 
            'customer_country' => 'customer_country', 
            'country_label' => 'country_label'
        );
    }

}
