<?php

class ProductVehicle extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $product_id;

    /**
     *
     * @var integer
     */
    protected $brand;

    /**
     *
     * @var integer
     */
    protected $model;

    /**
     *
     * @var integer
     */
    protected $year;

    /**
     *
     * @var integer
     */
    protected $start_year;

    /**
     *
     * @var integer
     */
    protected $end_year;

    /**
     *
     * @var string
     */
    protected $label;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field product_id
     *
     * @param integer $product_id
     * @return $this
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;

        return $this;
    }

    /**
     * Method to set the value of field brand
     *
     * @param integer $brand
     * @return $this
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Method to set the value of field model
     *
     * @param integer $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Method to set the value of field year
     *
     * @param integer $year
     * @return $this
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Method to set the value of field start_year
     *
     * @param integer $start_year
     * @return $this
     */
    public function setStartYear($start_year)
    {
        $this->start_year = $start_year;

        return $this;
    }

    /**
     * Method to set the value of field end_year
     *
     * @param integer $end_year
     * @return $this
     */
    public function setEndYear($end_year)
    {
        $this->end_year = $end_year;

        return $this;
    }

    /**
     * Method to set the value of field label
     *
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field product_id
     *
     * @return integer
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * Returns the value of field brand
     *
     * @return integer
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Returns the value of field model
     *
     * @return integer
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Returns the value of field year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Returns the value of field start_year
     *
     * @return integer
     */
    public function getStartYear()
    {
        return $this->start_year;
    }

    /**
     * Returns the value of field end_year
     *
     * @return integer
     */
    public function getEndYear()
    {
        return $this->end_year;
    }

    /**
     * Returns the value of field label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'product_id' => 'product_id', 
            'brand' => 'brand', 
            'model' => 'model', 
            'year' => 'year', 
            'start_year' => 'start_year', 
            'end_year' => 'end_year', 
            'label' => 'label'
        );
    }

}
