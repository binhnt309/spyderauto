<?php

class ArtCategoriesExt extends ArtCategories
{
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED= 1;

    public function getSource() {
        return 'art_categories';
    }

    public static function statusArr() {
        return [
            self::STATUS_DISABLED => 'DISABLED',
            self::STATUS_ENABLED => 'ENABLED'
        ];
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_at', 'update_at'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'update_at',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
    }

    public function beforeValidation() {

    }
}
