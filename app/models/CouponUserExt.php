<?php

class CouponUserExt extends CouponUser
{
    public function getSource() {
        return 'coupon_user';
    }

    static function getTable() {
        $instance = new self;
        return $instance->getSource();
    }

    public function initialize() {
        /*$this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time'//, 'modified_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_time',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));*/
    }

    public function beforeValidationOnCreate() {
        $this->setValidate(1);
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation() {

        return $this->validationHasFailed() != true;
    }

}
