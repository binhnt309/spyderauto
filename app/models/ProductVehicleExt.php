<?php

/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */
class ProductVehicleExt extends ProductVehicle
{

    public function getSource()
    {
        return 'product_vehicle';
    }

    static function getTable() {
        $instance = new self;
        return $instance->getSource();
    }

}