<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */

class ProductModelsExt extends ProductModels{

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED= 1;

    public static function statusArr() {
        return [
            self::STATUS_DISABLED => 'DISABLED',
            self::STATUS_ENABLED => 'ENABLED'
        ];
    }

    public function getSource() {
        return 'product_models';
    }

    static function getTable() {
        $instance = new ProductModels();
        return $instance->getSource();
    }

    /**
     * @return bool
     */
    public function beforeValidation(){
        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => [
                'brand',
                'model_name'
            ],
            'message' => 'Model belong the brand must be unique!'
        ]));
        return $this->validationHasFailed() != true;
    }
}