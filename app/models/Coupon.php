<?php

class Coupon extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $code;

    /**
     *
     * @var integer
     */
    protected $amount;

    /**
     *
     * @var integer
     */
    protected $unit;

    /**
     *
     * @var double
     */
    protected $limit_amount;

    /**
     *
     * @var string
     */
    protected $start;

    /**
     *
     * @var string
     */
    protected $end;

    /**
     *
     * @var string
     */
    protected $mail_content;

    /**
     *
     * @var integer
     */
    protected $mail_send;

    /**
     *
     * @var integer
     */
    protected $active;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Method to set the value of field amount
     *
     * @param integer $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Method to set the value of field unit
     *
     * @param integer $unit
     * @return $this
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Method to set the value of field limit_amount
     *
     * @param double $limit_amount
     * @return $this
     */
    public function setLimitAmount($limit_amount)
    {
        $this->limit_amount = $limit_amount;

        return $this;
    }

    /**
     * Method to set the value of field start
     *
     * @param string $start
     * @return $this
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Method to set the value of field end
     *
     * @param string $end
     * @return $this
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Method to set the value of field mail_content
     *
     * @param string $mail_content
     * @return $this
     */
    public function setMailContent($mail_content)
    {
        $this->mail_content = $mail_content;

        return $this;
    }

    /**
     * Method to set the value of field mail_send
     *
     * @param integer $mail_send
     * @return $this
     */
    public function setMailSend($mail_send)
    {
        $this->mail_send = $mail_send;

        return $this;
    }

    /**
     * Method to set the value of field active
     *
     * @param integer $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Returns the value of field amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Returns the value of field unit
     *
     * @return integer
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Returns the value of field limit_amount
     *
     * @return double
     */
    public function getLimitAmount()
    {
        return $this->limit_amount;
    }

    /**
     * Returns the value of field start
     *
     * @return string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Returns the value of field end
     *
     * @return string
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Returns the value of field mail_content
     *
     * @return string
     */
    public function getMailContent()
    {
        return $this->mail_content;
    }

    /**
     * Returns the value of field mail_send
     *
     * @return integer
     */
    public function getMailSend()
    {
        return $this->mail_send;
    }

    /**
     * Returns the value of field active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'code' => 'code', 
            'amount' => 'amount', 
            'unit' => 'unit', 
            'limit_amount' => 'limit_amount', 
            'start' => 'start', 
            'end' => 'end', 
            'mail_content' => 'mail_content', 
            'mail_send' => 'mail_send', 
            'active' => 'active'
        );
    }

}
