<?php

class OrdersExt extends Orders
{
    const STATUS_WAIT = 0;
    const STATUS_PAID = 1;
    const STATUS_DELIVERED = 2;
    const STATUS_CANCEL = 3;

    public static function statusArr()
    {
        return [
            self::STATUS_WAIT => 'Waiting',
            self::STATUS_PAID => 'Paid',
            self::STATUS_DELIVERED => 'Delivered',
            self::STATUS_CANCEL => 'Cancel'
        ];
    }

    public function getSource()
    {
        return 'orders';
    }

    static function getTable()
    {
        $instance = new Orders();
        return $instance->getSource();
    }

    public function initialize()
    {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_at', 'modified_at'
                    ),
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));

        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\SoftDelete(
            [
                'field' => 'status',
                'value' => self::STATUS_CANCEL
            ]
        ));
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation()
    {

        return true;
    }

}
