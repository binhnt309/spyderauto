<?php

class CurrencyExt extends Currencies
{
    public function getSource() {
        return 'currencies';
    }

    static function getTable() {
        $instance = new Currencies();
        return $instance->getSource();
    }
    public function initialize() {
        /*$this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time', 'modified_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_time',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));*/
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation() {

        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => [
                'name',
                'country_id'
            ],
            'message' => 'Currency belong the country has been defined!'
        ]));

        return $this->validationHasFailed() != true;
    }

}
