<?php

class OrderItemsExt extends OrderItems
{

    public function getSource()
    {
        return 'order_items';
    }

    static function getTable()
    {
        $instance = new self;
        return $instance->getSource();
    }

    public function initialize()
    {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_at'
                    ),
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation()
    {
        return true;
    }

}
