<?php

class LocationsExt extends Locations
{
    const TYPE_COUNTRY = 1;
    const TYPE_PROVINCE = 2;
    const TYPE_DISTRICT = 3;

    public function getSource() {
        return 'locations';
    }

    public static function locationType() {
        return [
            self::TYPE_COUNTRY => 'COUNTRY',
            self::TYPE_PROVINCE => 'PROVINCE',
            self::TYPE_DISTRICT => 'DISTRICT'
        ];
    }

    public function initialize() {
        /*$this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time', 'modified_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_time',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));*/
    }

    public function beforeValidation() {
        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'name',
            'message' => 'Name is not be blank'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => [
                'name'
            ],
            'message' => 'Name has been exist!'
        ]));

        return $this->validationHasFailed() != true;
    }
}
