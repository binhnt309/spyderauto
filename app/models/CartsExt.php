<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */

class CartsExt extends \Carts {

    public function getSource() {
        return 'carts';
    }

    static function getTable() {
        $instance = new Carts();
        return $instance->getSource();
    }

    public function initialize() {
        $this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));
    }

    public function beforeValidation() {

        return $this->validationHasFailed() != true;
    }

}