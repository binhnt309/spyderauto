<?php

/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 2/3/15
 * Time: 3:10 PM
 */
class ShippingFeeExt extends ShippingFee
{

    public function getSource()
    {
        return 'shipping_fee';
    }

    static function getTable() {
        $instance = new ShippingFee();
        return $instance->getSource();
    }

    /**
     * @return bool
     */
    public function beforeValidation(){

    }
}