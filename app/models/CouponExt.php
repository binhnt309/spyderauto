<?php

class CouponExt extends Coupon
{

    const UNIT_PERCENT = 0;
    const UNIT_MONEY = 1;
    const UNIT_COLLECT = 2;

    const ACTIVE = 1;
    const DE_ACTIVE = 0;

    static function unitArr() {
        return [
            self::UNIT_PERCENT => '%',
            self::UNIT_MONEY => '$'
        ];
    }

    public function getSource() {
        return 'coupon';
    }

    static function getTable() {
        $instance = new self;
        return $instance->getSource();
    }

    public function initialize() {
        /*$this->addBehavior(new \Phalcon\Mvc\Model\Behavior\Timestampable(
            array(
                'beforeValidationOnCreate' => array(
                    'field' => array(
                        'created_time'//, 'modified_time'
                    ),
                    'format' => 'Y-m-d H:i:s'
                ),
                'beforeValidationOnUpdate' => array(
                    'field' => 'modified_time',
                    'format' => 'Y-m-d H:i:s'
                )
            )
        ));*/
    }

    /**
     * Validation data before save data
     * @return bool
     */
    public function beforeValidation() {

        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness([
            'field' => 'code',
            'message' => 'Coupon code has been existed'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'start',
            'message' => 'Time start is not be emptied'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'end',
            'message' => 'Time end is not be emptied'
        ]));

        $this->validate(new \Phalcon\Mvc\Model\Validator\PresenceOf([
            'field' => 'amount',
            'message' => 'Amount is not be emptied'
        ]));

        return $this->validationHasFailed() != true;
    }

}
