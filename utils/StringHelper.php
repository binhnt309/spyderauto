<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 4/11/15
 * Time: 6:57 PM
 */
namespace Phalcon\Utils;

class StringHelper
{
    /**
     * @param $str
     * @param int $length
     * @param string $space
     * @return mixed
     */
    public static function strip_word($str, $length = 10, $pattern = '/\s/')
    {
        $str = preg_split($pattern, $str);
        if (count($str) > $length) {
            $new_str = [];
            for ($i = 0; $i < $length; $i++) {
                $new_str[] = $str[$i];
            }
            $str = implode(' ', $new_str) . ' ...';
        } else {
            $str = implode(' ', $str);
        }
        return $str;
    }

    /**
     * @param $uri
     * @param $file_name
     * @param $created_time
     * @return string
     */
    public static function product_img($uri, $file_name, $created_time){
        if(strpos($file_name, 'http://') > -1) {
            return $file_name;
        }
        //$time_folder = date('Y-m-d', strtotime($created_time));
        return $uri . 'uploads/products/'. $file_name;
    }
}