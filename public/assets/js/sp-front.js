/**
 * Created by binhnt on 4/9/15.
 */
var utils = {
    'slug': function (str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        var to = "aaaaaeeeeeiiiiooooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    },
    'loadCart': function () {
        $.ajax({
            url: '/cart/totalItems/?_=' + Math.random(),
            dataType: 'JSON',
            success: function (d) {
                $('sup.total-item').html(d.total);
            }, error: function (err) {

            }
        });
    },
    'search': function () {

        // Home list product
        var product_home = $('.sp-products div.list').html(),
            query_string = 'keyword=' + $('.keyword').val() + '&brand=' + $('#brand').val()
                + '&model=' + $('#model').val() + '&year=' + $('#year').val() + '&cid='
                + ($('#cid').val() != undefined ? $('#cid').val() : '') + '&page=' + $('#page').val();
        $('#query_string').val(query_string);

        // Change url
        window.history.pushState(null, null, '/?' + query_string);
        $('.sp-products h2').html('Search result');

        $('body').append('<div class="loading"></div>');
        $.ajax({
            url: '/index/?option=loadProduct',
            data: {
                brand: $('#brand').val(),
                model: $('#model').val(),
                year: $('#year').val(),
                cid: $('#cid').val(),
                keyword: $('.keyword').val(),
                page: $('#page').val()
            },
            dataType: 'json',
            success: function (d) {
                var html = '';

                if (typeof d == 'object') {
                    var inc = 0;
                    for (var i in d['data']) {
                        if (inc == 0) {
                            html += '<div class="row">';
                        }

                        // HTML append here
                        html += '<div class="col-sm-3 sp-block text-center">';
                        html += '<a href="' + d['data'][i].link + '" title="' + d['data'][i].title + '">';
                        html += '<img src="' + d['data'][i].src + '"/>';
                        html += d['data'][i].title;
                        html += '</a>';
                        html += '<p class="item-price ' + (d['data'][i].sale_off != '0' ? "promotion" : "") + '">' +
                            (d['data'][i].sale_off == 0 ? "<span>Price:</span><span>" + d['data'][i].price + "</span>" :
                                "<span>Promotion:</span><span>" + d['data'][i].sale_off + "</span>");
                        html += '</div>';
                        // END html content

                        if (inc == 3 || (inc + 1 == d.length)) {
                            html += '</div>';
                        }
                        if (inc >= 4) {
                            inc = 0;
                        }
                        inc++;
                    }

                    if (!d['data'].length) {
                        $('.sp-products div.msg').html('<div class="col-sm-12 item-not-found"><i>No result found</i></div>');
                    } else {
                        $('.sp-products div.msg').html('');
                    } // ---------
                    $('.sp-products div.list').html(
                        d['data'].length ? html : product_home
                    );

                    if ($('.sp-products').hasClass('detail'))
                        $('.sp-products').removeClass('detail');

                    if (typeof d['pages'] == 'object') {
                        var _pageOption = [];
                        for (var i in d['pages']) {
                            _pageOption.push('<option value="' + i + '">' + i + '</option>');
                        }
                        $('#page').html(_pageOption);
                    }

                    // Highlight keyword search
                    $('.sp-products div.list').highlight($('.keyword').val());
                }

                $('div.loading').remove();
            }, error: function (err) {
                $('div.loading').remove();
            }
        });
    },
    'pagingChange': function (obj, baseUrl) {
        window.location.href = baseUrl + '?' + $("#query_string").val().replace(/page=[0-9]*|&page=[0-9]*/, "") + "&page=" + $(obj).val();
    }
};

$(document).ready(function () {
    utils.loadCart();
});