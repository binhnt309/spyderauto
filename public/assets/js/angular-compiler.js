/**
 * Created by binhnt on 12/15/14.
 */
var ngModule = angular.module('email-app', [])
    .config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    });