/**
 * Created by binhnt on 4/11/15.
 */
$(document).ready(function () {
    $('#brand').change(function () {
        //Load list model
        if ($(this).val())
            $.ajax({
                url: '/index/?option=loadModel',
                data: { brand: $(this).val() },
                dataType: 'json',
                success: function (d) {
                    $('#model').html('<option value="0">- Select model</option>');
                    console.log(typeof d);
                    if (typeof d == 'object') {
                        for (var i in d) {
                            $('#model').append('<option value="' + d[i]['id'] + '">' + d[i]['title'] + '</option>')
                        }
                    }
                }
            });
    });
    $('#model').change(function () {
        //Load list year
        if ($(this).val()) {
            $.ajax({
                url: '/index/?option=loadYear',
                data: { model: $(this).val() },
                dataType: 'json',
                success: function (d) {
                    $('#year').html('<option value="0">- Select year</option>');
                    console.log(typeof d);
                    if (typeof d == 'object') {
                        for (var i in d) {
                            $('#year').append('<option value="' + d[i]['id'] + '">' + d[i]['title'] + '</option>')
                        }
                    }
                }
            });
        }
    });

    $('button.sp-search').click(function () {
        utils.search();
    });

    $('input.keyword').keyup(function(e){
        if(e.keyCode == 13) {
            utils.search();
        }
    });
});